//
//  ShowImageViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 01/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol showImageDelegate {
    func btnCloseClick(_ sender:ShowImageViewController)
}
class ShowImageViewController: UIViewController,UIScrollViewDelegate {

    var delegate : showImageDelegate?
    var arrImg = [JSONDictionary]()
    var currentIndex : Int! = 0
    @IBOutlet var scrBanner : UIScrollView!
    @IBOutlet var pageControll : UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        pageControll.numberOfPages = arrImg.count
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let pagesScrollViewSize = CGSize(width: 300,height: scrBanner.frame.size.height)
        scrBanner.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(arrImg.count),height: pagesScrollViewSize.height)
        var cnt : CGFloat = 0
        for i in 0...arrImg.count - 1
        {
            cnt = CGFloat(i) * 300
            let imgView = UIImageView(frame: CGRect.init(x: cnt, y: 0, width: 300, height: scrBanner.contentSize.height))
            imgView.tag = i
            imgView.contentMode = .scaleAspectFit
            imgView.sd_setShowActivityIndicatorView(true)
            imgView.sd_showActivityIndicatorView()
            imgView.sd_setImage(with: URL.init(string: "\(arrImg[i]["image"]!)"), placeholderImage: nil, options: [], completed: nil)
            scrBanner.addSubview(imgView)
        }
        
        let x = CGFloat(currentIndex) * 300
        scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
    }
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @IBAction func changePage(sender : UIPageControl)
    {
        let x = CGFloat(pageControll.currentPage) * scrBanner.frame.size.width
        scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
        pageControll.currentPage = pageControll.currentPage
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pageNumber = round(scrBanner.contentOffset.x / scrBanner.frame.size.width)
        pageControll.currentPage = Int(pageNumber)
        currentIndex = pageControll.currentPage
    }
    
    @IBAction func btnClosePress(sender:UIButton)
    {
        self.delegate?.btnCloseClick(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
