//
//  PostAJobDatePickerViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 19/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol PostAJobDatePickerDelegate
{
    func DoneDatePickerClicked(_ secondDetailViewController: PostAJobDatePickerViewController)
}
class PostAJobDatePickerViewController: UIViewController
{
    var delegate: PostAJobDatePickerDelegate?
    @IBOutlet var btnFrom : UIButton!
    @IBOutlet var btnTo : UIButton!
    @IBOutlet var datePickerFrom : UIDatePicker!
    @IBOutlet var datePickerTo : UIDatePicker!
    @IBOutlet var lblError : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblError.isHidden = true
        btnFromPress(sender: UIButton())
    }
    @IBAction func btnFromPress(sender:UIButton)
    {
        btnFrom.setTitleColor(UIColor.customBlackColor(), for: .normal)
        btnTo.setTitleColor(UIColor.setgray_119_119_119(), for: .normal)
        datePickerFrom.isHidden = false
        datePickerTo.isHidden = true
    }
    @IBAction func btnToPress(sender:UIButton)
    {
        btnTo.setTitleColor(UIColor.customBlackColor(), for: .normal)
        btnFrom.setTitleColor(UIColor.setgray_119_119_119(), for: .normal)
        datePickerFrom.isHidden = true
        datePickerTo.isHidden = false
    }
    @IBAction func btnDonePress(sender:UIButton)
    {
        let date1 = UtilityClass.getFormattedDate(string: "\(datePickerFrom.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "yyyy-MM-dd")
        
        let date2 = UtilityClass.getFormattedDate(string: "\(datePickerTo.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "yyyy-MM-dd")

        switch  date1.compare(date2) {
        case .orderedAscending:
            self.delegate?.DoneDatePickerClicked(self)
            break
            
        case .orderedDescending:
            lblError.isHidden = false
            lblError.text = "From date should be less then To date."
            break
            
        case .orderedSame:
            self.delegate?.DoneDatePickerClicked(self)
            break
        }
//        if datePickerFrom.date <= datePickerTo.date
//        {
//            self.delegate?.DoneDatePickerClicked(self)
//        }
//        else
//        {
//            lblError.isHidden = false
//            lblError.text = "From date should be less then To date."
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
