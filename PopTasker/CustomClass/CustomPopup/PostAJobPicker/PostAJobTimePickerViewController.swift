//
//  PostAJobTimePickerViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 19/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol PostAJobTimePickerDelegate
{
    func DoneTimePickerClicked(_ secondDetailViewController: PostAJobTimePickerViewController)
}

class PostAJobTimePickerViewController: UIViewController
{
    var delegate: PostAJobTimePickerDelegate?
    @IBOutlet var btnFrom : UIButton!
    @IBOutlet var btnTo : UIButton!
    @IBOutlet var datePickerFrom : UIDatePicker!
    @IBOutlet var datePickerTo : UIDatePicker!
    @IBOutlet var lblError : UILabel!
    
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblError.isHidden = true

        btnFromPress(sender: UIButton())

    }

    @IBAction func btnFromPress(sender:UIButton)
    {
        btnFrom.setTitleColor(UIColor.customBlackColor(), for: .normal)
        btnTo.setTitleColor(UIColor.setgray_119_119_119(), for: .normal)
        datePickerFrom.isHidden = false
        datePickerTo.isHidden = true
    }
    @IBAction func btnToPress(sender:UIButton)
    {
        btnTo.setTitleColor(UIColor.customBlackColor(), for: .normal)
        btnFrom.setTitleColor(UIColor.setgray_119_119_119(), for: .normal)
        datePickerFrom.isHidden = true
        datePickerTo.isHidden = false
        
    }
    @IBAction func btnDonePress(sender:UIButton)
    {
        if datePickerFrom.date >= datePickerTo.date
        {
            lblError.isHidden = false
            lblError.text = "From date should be less then To date."
        }
        else
        {
            self.delegate?.DoneTimePickerClicked(self)
            
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
