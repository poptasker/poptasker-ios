//
//  CellPaymentTableview.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellPaymentTableview: UITableViewCell,UITextFieldDelegate {

    @IBOutlet var txtField: UITextField!
    var blockTableViewDidSelect:((Int,String)->Void)?

    @IBOutlet var LblTitle: LableNeueLightBlack0023!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(self.blockTableViewDidSelect != nil){
            self.blockTableViewDidSelect!(self.txtField.tag, txtField.text!)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
