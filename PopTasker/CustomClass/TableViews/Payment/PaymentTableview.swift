//
//  PaymentTableview.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

enum CellPaymentType {
    case FullName
    case BankName
    case BankRNumber
    case ACNumber
    case CardNumber
    case HolderName
    case CardType
}

class PaymentTableview: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var boolCheckTab : Bool = false
    enum custCellTtpe {
        case validity
        case cardDetail
    }

    var paymentArrayForValidity = [
        Payment.init(placeHolder: "Full Name", txtValue: "", cellType: CellPaymentType.FullName),
        Payment.init(placeHolder: "Bank Name", txtValue: "", cellType: CellPaymentType.BankName),
        Payment.init(placeHolder: "Bank Routing Number", txtValue: "", cellType: CellPaymentType.BankRNumber),
        Payment.init(placeHolder: "Account Number", txtValue: "", cellType: CellPaymentType.ACNumber),
        ]
    var paymentArrayForCardDetail = [
        Payment.init(placeHolder: "Card Number", txtValue: "", cellType: CellPaymentType.CardNumber),
        Payment.init(placeHolder: "Holder Name", txtValue: "", cellType: CellPaymentType.HolderName),
        Payment.init(placeHolder: "Card Type", txtValue: "", cellType: CellPaymentType.CardType),
        ]

    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellPaymentTableview", bundle: nil), forCellReuseIdentifier: "CellPaymentTableview")
        
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        //        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray() {
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!boolCheckTab){
            return paymentArrayForValidity.count

        }else{
            return paymentArrayForCardDetail.count

        }
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.dequeueReusableCell(withIdentifier: "CellPaymentTableview") as! CellPaymentTableview
        
        if(!boolCheckTab){
            let userData = self.paymentArrayForValidity[indexPath.row]
            cell.blockTableViewDidSelect = {(int,str) -> Void in
                self.paymentArrayForValidity[int].txtValue = str
                self.reloadData()
            }
            cell.txtField.placeholder = userData.placeHolder
            cell.LblTitle.text = userData.placeHolder
            cell.txtField.text = userData.txtValue
            cell.txtField.tag = indexPath.row
            if indexPath.row == 2 || indexPath.row == 3
            {
                cell.txtField.keyboardType = .numberPad
            }
            cell.txtField.isEnabled = true
        }
        else
        {
            let userData = self.paymentArrayForCardDetail[indexPath.row]
            cell.blockTableViewDidSelect = {(int,str) -> Void in
                self.paymentArrayForCardDetail[int].txtValue = str
                self.reloadData()
            }
            cell.txtField.placeholder = userData.placeHolder
            cell.LblTitle.text = userData.placeHolder
            cell.txtField.text = userData.txtValue
            cell.txtField.tag = indexPath.row

            cell.txtField.isEnabled = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getProportionalHeight(height: 98)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if(self.blockTableViewDidSelectAtIndexPath != nil){
        //            self.blockTableViewDidSelectAtIndexPath!(indexPath, arrSearchText[indexPath.row].image)
        //        }
        
    }
    
}
