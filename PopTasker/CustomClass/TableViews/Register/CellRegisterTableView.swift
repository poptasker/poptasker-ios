//
//  CellRegisterTableView.swift
//  PopTasker
//
//  Created by Rohan on 21/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

protocol CellRegisterDelegate {
    func getTextValue(text : String,cellType : CellType,Register: CellRegisterTableView)
    
}
class CellRegisterTableView: UITableViewCell,UITextFieldDelegate {

    @IBOutlet var txtField: RegisterTextField!
    @IBOutlet var bgImage: UIImageView!
    var cellType : CellType!
    var delegate : CellRegisterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.getTextValue(text: textField.text!,cellType : cellType, Register: self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
