//
//  CellRegisterFooter.swift
//  PopTasker
//
//  Created by Rohan on 21/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol CellRegisterFooterDelegate {
    func termsAndConditionSelected(Selected: Bool, RegisterFooter: CellRegisterFooter)
    func bntSignUpPress(RegisterFooter: CellRegisterFooter)

}
class CellRegisterFooter: UITableViewCell {

    //Variable Declaration
    @IBOutlet var btnTermsAndCondition: TOSButton!
    @IBOutlet var btnSignIn: UIButton!
    var delegate : CellRegisterFooterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnSignIn.addTarget(self, action: #selector(btnSignInpress), for: UIControl.Event.touchUpInside)
        btnTermsAndCondition.isSelected = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func btnTermsPress(sender:UIButton)
    {
        let isSelected = btnTermsAndCondition.isSelected
        
        btnTermsAndCondition.isSelected = !isSelected
        delegate?.termsAndConditionSelected(Selected: btnTermsAndCondition.isSelected, RegisterFooter: self)
    }
    
    @IBAction func btnSignInpress(sender:UIButton)
    {
        if let nvc = self.viewController?.navigationController
        {
            nvc.popViewController(animated: true)
        }

    }
    @IBAction func btnSignUppress(sender:UIButton)
    {
        delegate?.bntSignUpPress(RegisterFooter: self)
    }
}
