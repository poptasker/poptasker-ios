//
//  RegisterTableView.swift
//  PopTasker
//
//  Created by Rohan on 21/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

enum CellType {
    case FirstName
    case LastName
    case Email
    case Password
    case ConfirmPassword
}

class RegisterTableView: UITableView, UITableViewDataSource, UITableViewDelegate,CellRegisterFooterDelegate ,CellRegisterDelegate{
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var isTermsAggreed : Bool! = false
    var isFromFB : Bool! = false
    var fb_token = ""
    var params = JSONDICTIONARY()
    var userFieldArray = [RegisterUser]()
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellRegisterTableView", bundle: nil), forCellReuseIdentifier: "CellRegisterTableView")
        
        //Header
        self.register(UINib(nibName:"CellRgisterHeader", bundle: nil), forCellReuseIdentifier: "CellRgisterHeader")
        
        //Footer
        self.register(UINib(nibName:"CellRegisterFooter", bundle: nil), forCellReuseIdentifier: "CellRegisterFooter")
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
//        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()

        // self.setTableFooter()
    }
    
    func createUserArray(arr : [RegisterUser])
    {
        userFieldArray = arr
        self.reloadData()
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userFieldArray.count
    }
    
    //MARK: -
    //MARK: - Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.dequeueReusableCell(withIdentifier: "CellRgisterHeader")
    }
    //MARK: -
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = self.dequeueReusableCell(withIdentifier: "CellRegisterFooter") as! CellRegisterFooter
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "CellRegisterTableView") as! CellRegisterTableView
        cell.delegate = self
        let userData = self.userFieldArray[indexPath.row]
        cell.txtField.placeholder = userData.placeHolder
        cell.txtField.text = userData.txtValue
        cell.cellType = userData.cellType
        if cell.cellType == CellType.Password
        {
            cell.txtField.isSecureTextEntry = true
        }
        if cell.cellType == CellType.ConfirmPassword
        {
            cell.txtField.isSecureTextEntry = true
        }
        cell.bgImage.image = UIImage(named:  userData.imgBgName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getProportionalHeight(height: 68)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 173)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 203)
    }
    
    //MARK:- DID SELECT      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userdata = self.userFieldArray[indexPath.row]
        if(self.blockTableViewDidSelectAtIndexPath != nil)
        {
            self.blockTableViewDidSelectAtIndexPath!(indexPath, userdata.txtValue)
        }
        
    }
    
    //MARK:- Register textfield Delegate

    func getTextValue(text: String, cellType: CellType, Register: CellRegisterTableView)
    {
        if !isFromFB
        {
            if cellType == .FirstName
            {
                userFieldArray[0].txtValue = text
            }
            else if cellType == .LastName
            {
                userFieldArray[1].txtValue = text
            }
            else if cellType == .Email
            {
                userFieldArray[2].txtValue = text
            }
            else if cellType == .Password
            {
                userFieldArray[3].txtValue = text
            }
            else if cellType == .ConfirmPassword
            {
                userFieldArray[4].txtValue = text
            }
        }
        else
        {
            if cellType == .FirstName
            {
                userFieldArray[0].txtValue = text
            }
            else if cellType == .LastName
            {
                userFieldArray[1].txtValue = text
            }
            else if cellType == .Email
            {
                userFieldArray[2].txtValue = text
            }
        }
        
    }
    //MARK:- Register Footer Delegate
    func termsAndConditionSelected(Selected: Bool, RegisterFooter: CellRegisterFooter) {
        isTermsAggreed = Selected
    }
    
    func bntSignUpPress(RegisterFooter: CellRegisterFooter)
    {
        self.endEditing(true)
        
        if !isFromFB
        {
            if validate()
            {
                let param = ["firstname":"\(userFieldArray[0].txtValue!)","lastname":"\(userFieldArray[1].txtValue!)","email":"\(userFieldArray[2].txtValue!)","password":"\(userFieldArray[3].txtValue!)","confirm_password":"\(userFieldArray[4].txtValue!)","fcm_token":getUserDefaultsForKey(key: "fcmToken")!,"device_id":UIDevice.current.identifierForVendor!.uuidString,"device_type":"ios"] as JSONDICTIONARY
                print(param)
                
                AppDel.showHUD()
                ApiManager.shared.postData(url: kRegister, params: param) { (dict) in
                    print(dict)
                    AppDel.hideHUD()
                    if let nvc = self.viewController?.navigationController
                    {
                        nvc.popViewController(animated: true)
                    }
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                    
                }
            }
        }
        else
        {
            if FBValidation()
            {
                let param = ["email":"\(userFieldArray[2].txtValue!)","fcm_token":"\(getUserDefaultsForKey(key: "fcmToken")!)","device_id":"\(UIDevice.current.identifierForVendor!.uuidString)","fb_token":fb_token,"firstname":"\(userFieldArray[0].txtValue!)","lastname":"\(userFieldArray[1].txtValue!)","device_type":"ios"] as JSONDICTIONARY
                
                
                print(param)
                AppDel.showHUD()
                ApiManager.shared.postData(url: kLoginwithfb, params: param, completion: { (dict) in
                    print(dict)
                    AppDel.hideHUD()
                    setModelDataInUserDefaults(key: "userData", value:dict )
                    setUserDefaultsFor(object: true as AnyObject, with: "isLogin")
                    
                    AppDel.tabbarVC = STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                    AppDel.sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
                    AppDel.window?.rootViewController = SideMenuController(contentViewController: AppDel.tabbarVC,menuViewController: AppDel.sideMenuviewObj)
                    AppDel.window?.makeKeyAndVisible()
                })
            }
        }
    }
    
    func validate() -> Bool
    {
        if userFieldArray[0].txtValue.count != 0
        {
            if userFieldArray[1].txtValue.count != 0
            {
                if userFieldArray[2].txtValue.trim().count != 0 {
                    if UtilityClass.isValidEmail(testStr: userFieldArray[2].txtValue)
                    {
                        if userFieldArray[3].txtValue.count >= 6
                        {
                            if userFieldArray[4].txtValue.count >= 6
                            {
                                if userFieldArray[3].txtValue == userFieldArray[4].txtValue
                                {
                                    if isTermsAggreed
                                    {
                                        return true
                                    }
                                    else
                                    {
                                        UtilityClass.showAlert(str: "Please accept terms and condition to proceed..!!")
                                        return false
                                    }
                                }
                                else
                                {
                                    UtilityClass.showAlert(str: "Password and confirm password are not same.")
                                    return false
                                }
                            }
                            else
                            {
                                UtilityClass.showAlert(str: "Confirm password length should be minimum six chracters.")
                                return false
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "A password length should be minimum six chracters.")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please enter valid email id.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please enter email id.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter last name")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter first name")
            return false
        }
    }
    
    func FBValidation() -> Bool {
        if userFieldArray[0].txtValue.count != 0
        {
            if userFieldArray[1].txtValue.count != 0
            {
                if userFieldArray[2].txtValue.trim().count != 0 {
                    if UtilityClass.isValidEmail(testStr: userFieldArray[2].txtValue)
                    {
                        if isTermsAggreed
                        {
                            return true
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "Please accept terms and condition to proceed..!!")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please enter valid email id.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please enter email id.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter last name")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter first name")
            return false
        }
    }
}
