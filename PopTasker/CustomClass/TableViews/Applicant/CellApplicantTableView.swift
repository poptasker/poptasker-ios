//
//  CellApplicantTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellApplicantTableView: UITableViewCell
{

    @IBOutlet var imgDot : UIImageView!
    @IBOutlet var imgUser :UIImageView!
    @IBOutlet var lblCoachName : UILabel!
    @IBOutlet var lblCount : UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
