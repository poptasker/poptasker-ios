//
//  ApplicantTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantTableView: UITableView, UITableViewDataSource, UITableViewDelegate
{
    var blockTableViewDidSelectAtIndexPath:((IndexPath,JSONDICTIONARY)->Void)?
    var arrApplicant : [JSONDICTIONARY] = Array()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellApplicantTableView", bundle: nil), forCellReuseIdentifier: "CellApplicantTableView")
        
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        //        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.separatorStyle = .singleLine
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        self.tableFooterView = UIView()
        // self.setTableFooter()
    }
    
    func reloadTable()
    {
//        arrApplicant = arr
        self.reloadData()
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrApplicant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellApplicantTableView") as! CellApplicantTableView
        let dict = arrApplicant[indexPath.row]
        cell.lblCoachName.text = "\(dict["title"]!)"
        cell.lblCount.text = "\(dict["totalapplications"]!)"
        if Int("\(dict["totalapplications"]!)")! > 0
        {
            cell.imgDot.image = #imageLiteral(resourceName: "Applicant_dot_Active")
            cell.imgUser.image = #imageLiteral(resourceName: "Applicant_user_Active")
        }
        else
        {
            cell.imgDot.image = #imageLiteral(resourceName: "Applicant_dot_Inactive")
            cell.imgUser.image = #imageLiteral(resourceName: "Applicant_user_Inactive")

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getProportionalHeight(height: 69)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.blockTableViewDidSelectAtIndexPath != nil){
            self.blockTableViewDidSelectAtIndexPath!(indexPath, arrApplicant[indexPath.row])
        }
    }
}
