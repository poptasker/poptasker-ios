//
//  SearchDetailTableview.swift
//  PopTasker
//
//  Created by Admin on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

enum SearchDetailCellType
{
    case RankBy
    case Date
    case Wage
    case Distance
    
    func simpleDescription() -> String {
        switch self
        {
        case .RankBy:
            return "rankBy"
        case .Date:
            return "date"
        case .Wage:
            return "wage"
        case .Distance:
            return "distance"
        default:
            return ""
        }
    }
}

class SearchDetailTableview: UITableView,UITableViewDelegate,UITableViewDataSource {
    var blockTableViewDidSelectAtIndexPath:((IndexPath,JSONDICTIONARY)->Void)?

    var SearchDetailListArray = [SearchDetail]()
    var arrList = [JSONDICTIONARY]()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellSearchDetailTableview", bundle: nil), forCellReuseIdentifier: "CellSearchDetailTableview")
        
        self.estimatedRowHeight = 300
        self.rowHeight = UITableView.automaticDimension
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray(arr:[SearchDetail],arrList : [JSONDICTIONARY])
    {
        self.arrList = arrList
        self.SearchDetailListArray = arr
        self.reloadData()
        AppDel.hideHUD()
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SearchDetailListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellSearchDetailTableview") as! CellSearchDetailTableview
        let userData = self.SearchDetailListArray[indexPath.row]
        cell.imgView.image = userData.img
        cell.lblDateCity.text = "\(userData.date!)/\(userData.city!)"
        if userData.isApplied == 1
        {
            cell.lblIsApplied.isHidden = false
        }
        else
        {
            cell.lblIsApplied.isHidden = true
        }
        cell.lblJobName.text = userData.jobName
        cell.lblDescription.text = userData.description
        cell.lblTime.text = userData.time
        cell.lblDollar.text = userData.dollar
        cell.imgView.image = userData.img
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return getProportionalHeight(height: 175)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.blockTableViewDidSelectAtIndexPath != nil)
        {
            let userData = self.arrList[indexPath.row]
            self.blockTableViewDidSelectAtIndexPath!(indexPath, userData)
        }
    }
}
