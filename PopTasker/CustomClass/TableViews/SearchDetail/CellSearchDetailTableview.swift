//
//  CellSearchDetailTableview.swift
//  PopTasker
//
//  Created by Admin on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellSearchDetailTableview: UITableViewCell {

    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblJobName : LabelNeueLight!
    @IBOutlet var lblDateCity : LabelNeueLight!
    @IBOutlet var lblDescription : LabelNeueLight!
    @IBOutlet var lblDollar : LabelNeueLight!
    @IBOutlet var lblTime : LabelNeueLight!
    @IBOutlet var lblType : LabelNeueLight!
    @IBOutlet var lblIsApplied : LabelNeueLight!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
