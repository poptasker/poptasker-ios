//
//  ActivityTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

enum ActivityCellType
{
    case Posted
    case Pending
    case Accepted
    case Completed
    case Cancelled
    case Deleted
    case All
    func simpleDescription() -> String {
        switch self
        {
        case .Posted:
            return "posted"
        case .Pending:
            return "Pending"
        case .Accepted:
            return "accepted"
        case .Completed:
            return "completed"
        case .Cancelled:
            return "cancelled"
        case .Deleted:
            return "deleted"
        default:
            return ""
        }
    }
}
class ActivityTableView: UITableView,UITableViewDelegate,UITableViewDataSource {

    var blockTableViewDidSelectAtIndexPath:((IndexPath,JSONDICTIONARY,String)->Void)?
    var blockTableViewDidDeleteAtIndexPath:((IndexPath,JSONDICTIONARY,String)->Void)?

    var ActivityListArray = [Activity]()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellActivityTableView", bundle: nil), forCellReuseIdentifier: "CellActivityTableView")
        
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = 173
        self.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "HomeBg"))
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
   
    func createUserArray(arr:[Activity])
    {
        ActivityListArray = arr
        self.reloadData()
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ActivityListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellActivityTableView") as! CellActivityTableView
        let userData = self.ActivityListArray[indexPath.row]
        let first = userData.name.first
        var img  = LetterImageGenerator.imageWith(name:"\(first!)") as! UIImage
        if userData.imgUrl != ""
        {
            cell.imgView!.sd_setImage(with: URL.init(string: "\(userData.imgUrl!)"), completed: nil)
        }
        else
        {
            cell.imgView!.image = img

        }
        cell.lblName.text = userData.name
        cell.lblCoachName.text = userData.coachName
        cell.lblDescription.text = "\(userData.dict["start_date"]!) \(userData.dict["start_time"]!) / \(userData.description!)"
        cell.lblType.text = userData.cellType.simpleDescription().capitalized
        let time = Double("\(userData.time!)")!
        let intTime = Int(time)
        let tempDoubleTime = Double(intTime)
        if tempDoubleTime < time
        {
            cell.lblTime.text = String(format:"%.2f Hours", time)
        }
        else
        {
            cell.lblTime.text = String(format:"%d Hours", intTime)
        }
        cell.lblDollar.text = "\(userData.dollar!) budget"
        cell.lblDeveloper.text = userData.developer
        cell.btnDelete.addTarget(self, action: #selector(self.btnDeletePress(sender:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        let filter = "\(userData.activityFilter!)"
        let dict = userData.dict as JSONDICTIONARY
        let posted = dict["posted"] as! Bool
        let deleted = dict["is_deleted"] as! Int
        let cancelled = dict["is_cancelled"] as! Int
        
        if filter == "all"
        {
            if posted
            {
                cell.lblType.text = "Posted"
            }
            
            cell.lblType.isHidden = false
        }
        else
        {
            cell.lblType.isHidden = true
        }

        if posted
        {
            if deleted == 1
            {
                cell.lblType.text = "Deleted"
                cell.btnDelete.isHidden = true
            }
            else
            {
                if userData.cellType == ActivityCellType.Accepted
                {
                    cell.btnDelete.isHidden = true
                }
                else
                {
                    cell.btnDelete.isHidden = false
                }
            }
        }
        else
        {
            cell.btnDelete.isHidden = true
        }
        if (cancelled == 1) {
            cell.lblType.text = "Cancelled"
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension//getProportionalHeight(height: 175.7)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = self.cellForRow(at: indexPath) as! CellActivityTableView
        if(self.blockTableViewDidSelectAtIndexPath != nil){
            self.blockTableViewDidSelectAtIndexPath!(indexPath, ActivityListArray[indexPath.row].dict, cell.lblType.text!)
        }
    }
    @objc func btnDeletePress(sender:UIButton) {
        let cell = self.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! CellActivityTableView
        if(self.blockTableViewDidDeleteAtIndexPath != nil){
            self.blockTableViewDidDeleteAtIndexPath!(IndexPath.init(row: sender.tag, section: 0), ActivityListArray[sender.tag].dict, cell.lblType.text!)
        }
    }
}
