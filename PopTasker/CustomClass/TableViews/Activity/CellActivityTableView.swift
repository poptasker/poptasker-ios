//
//  CellActivityTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellActivityTableView: UITableViewCell
{
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblCoachName : LabelNeueLight!
    @IBOutlet var lblType : LabelNeueLight!
    @IBOutlet var lblName : LabelNeueLight!
    @IBOutlet var lblDescription : LabelNeueLight!
    @IBOutlet var lblDeveloper : LabelNeueLight!
    @IBOutlet var lblDollar : LabelNeueLight!
    @IBOutlet var lblTime : LabelNeueLight!
    @IBOutlet var btnDelete : UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        imgView.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        imgView.layer.borderWidth = 2.0
        imgView.layer.cornerRadius = imgView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
