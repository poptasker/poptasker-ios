//
//  ApplicantListFooter.swift
//  PopTasker
//
//  Created by Urja_Macbook on 16/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantListFooter: UITableViewCell
{
    @IBOutlet var btnAwardJob : UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
