//
//  RegisterTableView.swift
//  PopTasker
//
//  Created by Rohan on 21/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantListTable: UITableView, UITableViewDataSource, UITableViewDelegate {

    var blockTableViewDidSelectAtIndexPath:((IndexPath,JSONDICTIONARY,JSONDICTIONARY)->Void)?
    var arrApplicantList : [JSONDICTIONARY]! = Array()
    var blockTableViewFavorite:((Int)->Void)?
    var mainDict : JSONDICTIONARY! = JSONDICTIONARY()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.register(UINib(nibName:"ApplicantListCell", bundle: nil), forCellReuseIdentifier: "ApplicantListCell")

        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray() {
        self.reloadData()
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrApplicantList.count
    }

    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.dequeueReusableCell(withIdentifier: "CellRegisterFooter")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "ApplicantListCell") as! ApplicantListCell
        let dict = arrApplicantList[indexPath.row]
        cell.lblName.text = "\(dict["firstname"]!) \(dict["lastname"]!)"
        let first = "\(dict["firstname"]!)".first
        cell.lblDate.text = "\(dict["applicationdate"]!)"
        cell.lblSummary.text = "\(dict["summary"]!)"
        cell.lblSummary.text = "\(dict["summary"]!)"
        cell.lblEndor.text = "# endorsements \(dict["endorsements"]!)  #of job completed \(dict["completedtasks"]!)"
        cell.rateView.rating = Float("\(dict["ratings"]!)")!
        cell.imgFav.tag = indexPath.row
        cell.imgFav.addTarget(self, action: #selector(btnFavPress(sender:)), for: .touchUpInside)
        if Int("\(dict["is_favorite"]!)") == 1
        {
            cell.imgFav.setImage(#imageLiteral(resourceName: "ApplicantLike"), for: .normal)
        }
        else
        {
            cell.imgFav.setImage(#imageLiteral(resourceName: "ApplicantUnLike"), for: .normal)
        }
        let urlStr = "\(dict["profilepic"]!)"
        if urlStr == ""
        {
            cell.imgProfile.image = LetterImageGenerator.imageWith(name: "\(first!)")

        }
        else
        {
            cell.imgProfile.sd_setImage(with: URL.init(string: urlStr), completed: nil)
        }
        return cell
    }
    
    @objc func btnFavPress(sender:UIButton)
    {
        let dict = arrApplicantList[sender.tag]
        var fav = "\(dict["is_favorite"]!)"
        
        if fav == "1"
        {
            fav = "0"
        }
        else
        {
            fav = "1"
        }
        ApiManager.shared.postDataWithHeader(url: kFavorite, params: ["employee_id":"\(dict["userid"]!)","is_favorite":fav], completion: { (dict) in
            
            UtilityClass.showToast(view: self.superview!, message: "\(dict["message"]!)")
            if(self.blockTableViewFavorite != nil)
            {
                self.blockTableViewFavorite!(sender.tag)
            }
            print(dict)
        })
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension//getProportionalHeight(height: 68)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UITableView.automaticDimension
        return getProportionalHeight(height: 0)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 0)
    }
    
    //MARK:- DID SELECT      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.blockTableViewDidSelectAtIndexPath != nil){
            self.blockTableViewDidSelectAtIndexPath!(indexPath, arrApplicantList[indexPath.row],mainDict)
        }
    }
}
