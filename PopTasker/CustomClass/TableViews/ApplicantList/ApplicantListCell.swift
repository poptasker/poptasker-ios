//
//  ApplicantListCell.swift
//  PopTasker
//
//  Created by Rohan on 31/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantListCell: UITableViewCell, RateViewDelegate {

    @IBOutlet var rateView: RateView!
    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblSummary : UILabel!
    @IBOutlet var lblEndor : UILabel!
//    @IBOutlet var lblCompletedJob : UILabel!
    @IBOutlet var imgFav : UIButton!
    var isFav : Bool! = false
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.rateView.fullSelectedImage = #imageLiteral(resourceName: "RateFill")
        self.rateView.halfSelectedImage = self.rateView.fullSelectedImage
        self.rateView.notSelectedImage = #imageLiteral(resourceName: "RateBlank")
        self.rateView.editable = false
        self.rateView.maxRating = 5
        self.rateView.leftMargin = 0
        self.rateView.midMargin = 1
        self.rateView.rating = 3
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func rateView(_ rateView: RateView!, ratingDidChange rating: Float) {
        
    }
}
class LetterImageGenerator: NSObject {
    class func imageWith(name: String?) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let nameLabel = UILabel(frame: frame)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = .lightGray
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 35)
        var initials = ""
        if let initialsArray = name?.components(separatedBy: " ") {
            if let firstWord = initialsArray.first {
                if let firstLetter = firstWord.first {
                    initials += String(firstLetter).capitalized
                }
            }
            if initialsArray.count > 1, let lastWord = initialsArray.last {
                if let lastLetter = lastWord.first {
                    initials += String(lastLetter).capitalized
                }
            }
        } else {
            return nil
        }
        nameLabel.text = initials
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
}
