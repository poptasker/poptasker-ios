//
//  FilterTableview.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit
import WARangeSlider
import WARangeSlider
enum CellFilterType {
    case Distance
    case Wage
    case WorkingHour
}
class FilterTableview: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var blockTableViewSetFilter:((JSONDICTIONARY)->Void)?

    var arrTags = [JSONDICTIONARY]()
    var selectedIndex = [Int]()
    var dictFilter = JSONDICTIONARY()
   
    var userFieldArray = [
        FilterModel.init(cellType:CellFilterType.Distance,sliderMinValue: "0 mile", sliderMaxValue: "50 mile", sliderValue: "0",sliderValueType:"mile", sliderTintColor: "plumPurple", titleLabel: "Distance Limitation",isSliderValUpldated: false),
        FilterModel.init(cellType:CellFilterType.Wage,sliderMinValue: "0 $/Hr", sliderMaxValue: "1000$/Hr", sliderValue: "1000",sliderValueType : "$/Hr", sliderTintColor: "brightLightBlue", titleLabel: "Required Minimum Wage",isSliderValUpldated: false),
        FilterModel.init(cellType:CellFilterType.WorkingHour, sliderMinValue: "0h", sliderMaxValue: "20h", sliderValue: "0",sliderValueType: "20h", sliderTintColor: "plumPurple", titleLabel: "Desired Working Hour",isSliderValUpldated: false),
         FilterModel.init(cellType:CellFilterType.WorkingHour, sliderMinValue: "0h", sliderMaxValue: "20h", sliderValue: "0",sliderValueType: "20h", sliderTintColor: "plumPurple", titleLabel: "Desired Working Hour",isSliderValUpldated: false),
        ]
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"FilterTableviewCell", bundle: nil), forCellReuseIdentifier: "FilterTableviewCell")
        
        //Header
        self.register(UINib(nibName:"FilterTableHeaderCell", bundle: nil), forCellReuseIdentifier: "FilterTableHeaderCell")
        
        //Footer
        self.register(UINib(nibName:"FilterTableFooterCell", bundle: nil), forCellReuseIdentifier: "FilterTableFooterCell")
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        //        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        
      //  initTagView()

        // self.setTableFooter()
    }


    func createUserArray(arrTags : [JSONDICTIONARY])
    {
        self.arrTags = arrTags
        self.arrTags.reverse()
//        print(self.arrTags)
        self.reloadData()

    }
    
    func setFilters(dict: JSONDICTIONARY?) {
        if dict!.keys.count != 0
        {
            print(dict!)
            let cell = self.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! FilterTableviewCell
            cell.slider.upperValue = Double.init("\(dict!["distance"]!)")!
            userFieldArray[0].isSliderValUpldated = true
            userFieldArray[0].sliderValue = "0"
            userFieldArray[0].sliderValueType = "\(dict!["distance"]!) mile"
            self.dictFilter["distance"] = "\(Int(cell.slider.upperValue))"
            
            let cell1 = self.cellForRow(at: IndexPath.init(row: 0, section: 1)) as! FilterTableviewCell
            let wages = "\(dict!["wage"]!)".components(separatedBy: "-")
            cell1.slider.lowerValue = Double.init("\(wages[0])")!
            cell1.slider.upperValue = Double.init("\(wages[1])")!
            userFieldArray[1].isSliderValUpldated = true
            userFieldArray[1].sliderValue = "\(wages[0])"
            userFieldArray[1].sliderValueType = "\(wages[1]) $/Hr"
            self.dictFilter["wages"] = "\(Int(cell1.slider.lowerValue))-\(Int(cell1.slider.upperValue))"
            
            let cell2 = self.cellForRow(at: IndexPath.init(row: 0, section: 2)) as! FilterTableviewCell
            let hours = "\(dict!["working_hours"]!)".components(separatedBy: "-")
            cell2.slider.lowerValue = Double.init("\(hours[0])")!
            cell2.slider.upperValue = Double.init("\(hours[1])")!
            userFieldArray[2].isSliderValUpldated = true
            userFieldArray[2].sliderValue = "\(hours[0])"
            userFieldArray[2].sliderValueType = "\(hours[1]) h"
            self.dictFilter["hour"] = "\(Int(cell2.slider.lowerValue))-\(Int(cell2.slider.upperValue))"
            
            let keywords = dict!["keywords"] as! [String]
            if keywords.count == 1
            {
                if keywords[0] == ""
                {
                    self.selectedIndex = Array()
                }
                else
                {
                    self.selectedIndex = keywords.map { Int($0)!}
                    self.selectedIndex.reverse()
                }
            }
            else
            {
                self.selectedIndex = Array()
                for i in keywords
                {
                    self.selectedIndex.append(Int("\(i)")!)
                }
                self.selectedIndex.reverse()

//                self.selectedIndex = keywords.map { Int($0)!}
            }
        }
        else
        {
            if let cell = self.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? FilterTableviewCell
            {
                cell.slider.upperValue = 50
                userFieldArray[0].isSliderValUpldated = true
                userFieldArray[0].sliderValue = "0"
                userFieldArray[0].sliderValueType = "50 mile"
                self.dictFilter["distance"] = "\(Int(cell.slider.upperValue))"
            }
            if let cell1 = self.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? FilterTableviewCell
            {
                cell1.slider.lowerValue = Double.init("0")!
                cell1.slider.upperValue = Double.init("1000")!
                userFieldArray[1].isSliderValUpldated = true
                userFieldArray[1].sliderValue = "0"
                userFieldArray[1].sliderValueType = "1000 $/Hr"
                self.dictFilter["wages"] = "\(Int(cell1.slider.lowerValue))-\(Int(cell1.slider.upperValue))"
            }
            if let cell2 = self.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? FilterTableviewCell
            {
                cell2.slider.lowerValue = Double.init("0")!
                cell2.slider.upperValue = Double.init("20")!
                userFieldArray[2].isSliderValUpldated = true
                userFieldArray[2].sliderValue = "0"
                userFieldArray[2].sliderValueType = "20 h"
                self.dictFilter["hour"] = "\(Int(cell2.slider.lowerValue))-\(Int(cell2.slider.upperValue))"
            }
            let keywords = [String]()
            self.selectedIndex = keywords.map { Int($0)!}
        }
       
        reloadData()
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return userFieldArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    //MARK: -
    //MARK: - Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.dequeueReusableCell(withIdentifier: "FilterTableHeaderCell") as! FilterTableHeaderCell
        let userData = userFieldArray[section]
        cell.lblHeader.text = "\(userData.cellType!)"
        if(section == 3){
            cell.lblHeader.textAlignment = .center
        }else
        {
            cell.lblHeader.textAlignment = .left
        }
        return cell
    }
    //MARK: -
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if(section == 3)
        {
            let view = self.dequeueReusableCell(withIdentifier: "FilterTableFooterCell") as! FilterTableFooterCell
            view.arrTag = self.arrTags
            view.tagView.reloadData()
            for tag in self.selectedIndex
            {
                view.tagView.selectTagAtIndex(tag-1)
                view.arrSelectedTag.append(self.arrTags[tag-1])
            }
            view.blockTableViewSaveFilterPress = {(arrSelectedTag) -> Void in
                
                var param = JSONDICTIONARY()
                if let wages = self.dictFilter["wages"] as? String
                {
                    param["wage"] = wages
                }
                else
                {
                    param["wage"] = "0-1000"
                }
                if let distance = self.dictFilter["distance"] as? String
                {
                    param["distance"] = distance
                }
                else
                {
                    param["distance"] = "50"
                }
                if let hour = self.dictFilter["hour"] as? String
                {
                    param["working_hours"] = hour
                }
                else
                {
                    param["working_hours"] = "0-20"
                }
                var stringArray = [String]()

                for dic in arrSelectedTag
                {
                    stringArray.append("\(dic["id"]!)")
                }
                if stringArray.count > 0
                {
                    param["keywords"] = stringArray.joined(separator: ",")
                }
                else
                {
                    param["keywords"] = ""
                }
                param["latitude"] = "\(AppDel.locationManager.location!.coordinate.latitude)"
                param["longitude"] = "\(AppDel.locationManager.location!.coordinate.longitude)"
                param["page"] = 1
                
                print(param)
                if self.blockTableViewSetFilter != nil
                {
                    self.blockTableViewSetFilter!(param)
                }
            }
            return view
        }
        else
        {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section
        {
        case 0:
            let cell = self.dequeueReusableCell(withIdentifier: "FilterTableviewCell") as! FilterTableviewCell
            let userData = self.userFieldArray[indexPath.section]
            cell.lblDetailTitle.text = userData.titleLabel
            cell.minSliderValue.text = userData.sliderMinValue
            cell.maxSliderValue.text = userData.sliderMaxValue
            cell.lblSlidervalue.text = userData.sliderValue
            cell.lblSliderValueType.text = userData.sliderValueType
            cell.slider.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            cell.slider.addTarget(self, action: #selector(sliderValueChange(slider:)), for: UIControl.Event.valueChanged)
            
            cell.slider.tag = indexPath.section
            cell.slider.minimumValue = 0
            cell.slider.maximumValue = 50
            if userData.isSliderValUpldated == false
            {
                cell.slider.upperValue = 50
                cell.slider.lowerValue = 0
            }
            cell.slider.thumbTintColor = UIColor.setPurpalSliderValueColor()
            cell.slider.tintColor = UIColor.setPurpalSliderValueColor()
            cell.slider.trackTintColor = UIColor.customGrayColor()
            cell.slider.trackHighlightTintColor = UIColor.setPurpalSliderValueColor()
            return cell
            
        case 1 :
            let cell = self.dequeueReusableCell(withIdentifier: "FilterTableviewCell") as! FilterTableviewCell
            let userData = self.userFieldArray[indexPath.section]
            cell.lblDetailTitle.text = userData.titleLabel
            cell.minSliderValue.text = userData.sliderMinValue
            cell.maxSliderValue.text = userData.sliderMaxValue
            cell.lblSlidervalue.text = userData.sliderValue
            cell.lblSliderValueType.text = userData.sliderValueType
            cell.slider.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            cell.lblSlidervalue.textColor = UIColor.setBlueSliderValuecolor()
            cell.lblSliderValueType.textColor = UIColor.setBlueSliderValuecolor()

            cell.maxSliderValue.textColor = UIColor.setBlueSliderValuecolor()
            cell.slider.addTarget(self, action: #selector(sliderValueChange(slider:)), for: UIControl.Event.valueChanged)
            cell.slider.tag = indexPath.section
            cell.slider.minimumValue = 0
            cell.slider.maximumValue = 1000
            if userData.isSliderValUpldated == false
            {
                cell.slider.upperValue = 1000
                cell.slider.lowerValue = 0
            }
            cell.slider.thumbTintColor = UIColor.setBlueSliderValuecolor()
            cell.slider.trackTintColor = UIColor.customGrayColor()
            cell.slider.trackHighlightTintColor = UIColor.setBlueSliderValuecolor()

            return cell

        case 2:
            let cell = self.dequeueReusableCell(withIdentifier: "FilterTableviewCell") as! FilterTableviewCell
            cell.slider.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
            let userData = self.userFieldArray[indexPath.section]
            cell.lblDetailTitle.text = userData.titleLabel
            cell.minSliderValue.text = userData.sliderMinValue
            cell.maxSliderValue.text = userData.sliderMaxValue
            cell.lblSlidervalue.text = userData.sliderValue
            cell.lblSliderValueType.text = userData.sliderValueType
            cell.lblSlidervalue.textColor = UIColor.setPurpalSliderValueColor()
            cell.lblSliderValueType.textColor = UIColor.setPurpalSliderValueColor()
            cell.slider.addTarget(self, action: #selector(sliderValueChange(slider:)), for: UIControl.Event.valueChanged)
            cell.slider.tag = indexPath.section
            cell.slider.minimumValue = 0
            cell.slider.maximumValue = 20
            if userData.isSliderValUpldated == false
            {
                cell.slider.upperValue = 20
                cell.slider.lowerValue = 0
            }
            cell.slider.thumbTintColor = UIColor.setPurpalSliderValueColor()
            cell.slider.trackTintColor = UIColor.customGrayColor()
            cell.slider.trackHighlightTintColor = UIColor.setPurpalSliderValueColor()
            return cell

        case 3:
            return UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    @objc func sliderValueChange(slider:RangeSlider)
    {
        let cell = self.cellForRow(at: IndexPath.init(row: 0, section:slider.tag)) as! FilterTableviewCell
        let userData = self.userFieldArray[slider.tag]

        if slider.tag == 0
        {
            cell.slider.lowerValue = cell.slider.minimumValue
            userData.sliderValueType = "\(Int(slider.upperValue)) mile"
            self.dictFilter["distance"] = "\(Int(slider.upperValue))"
        }
        else if slider.tag == 1
        {
            userData.sliderValueType = "\(Int(slider.upperValue)) $/Hr"
            self.dictFilter["wages"] = "\(Int(cell.slider.lowerValue))-\(Int(cell.slider.upperValue))"

        }
        else if slider.tag == 2
        {
            userData.sliderValueType = "\(Int(slider.upperValue)) h"
            self.dictFilter["hour"] = "\(Int(cell.slider.lowerValue))-\(Int(cell.slider.upperValue))"
        }
        
        cell.lblSlidervalue.text = "\(Int(slider.lowerValue))"
        cell.lblSliderValueType.text = userData.sliderValueType
        userData.isSliderValUpldated = true
        userData.sliderMinValue = "\(Int(slider.lowerValue))"
        userData.sliderMaxValue = "\(Int(slider.upperValue))"
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 1
        }else{
            return getProportionalHeight(height: 150)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 54.7)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if section == 3
        {
            return getProportionalHeight(height: 600)
        }
        else
        {
            return 1
        }
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
