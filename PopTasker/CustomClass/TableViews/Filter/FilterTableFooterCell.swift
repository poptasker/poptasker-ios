//
//  FilterTableFooterCell.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol FilterkeywordDelegate {
    func selectedTags(arrIndex:[Int],arrTags:[JSONDICTIONARY])
}
class FilterTableFooterCell: UITableViewCell,HTagViewDelegate,HTagViewDataSource
{
    var blockTableViewSaveFilterPress:(([JSONDICTIONARY])->Void)?
    @IBOutlet var tagView : HTagView!
    var arrTag = [JSONDICTIONARY]()
    var arrSelectedTag = [JSONDICTIONARY]()
    var deleg : keywordDelegate?
//    var isSelected : Bool! = false
    var arrSelectedStr = [String]()
    var selectedIndex = [Int]()
    override func awakeFromNib() {
        super.awakeFromNib()
//        initTagView()
        tagView.delegate = self
        tagView.dataSource = self
        tagView.multiselect = true
        tagView.marg = 3
        tagView.btwTags = 16
        tagView.btwLines = 20
        tagView.tagFont = UIFont.init(name: "HelveticaNeueLTPro-Lt", size: 16.7)!
        tagView.tagSecondBackColor = UIColor.white
        tagView.tagSecondTextColor = UIColor.black
        tagView.tagMainBackColor = UIColor.init(patternImage: GradientImage.gradientTheamBackgroundFrame(view: self.contentView))
        tagView.tagMainTextColor = UIColor.white
        tagView.tagBorderColor = UIColor(red: 61.0/255.0, green: 196.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
        // Initialization code
    }
    @IBAction func btnSavePress(sender:UIButton)
    {
        if(self.blockTableViewSaveFilterPress != nil)
        {
            self.blockTableViewSaveFilterPress!(arrSelectedTag)
        }
    }
    func initTagView()
    {
        self.tagView.reloadData()
    }
    
    // MARK: - HTagViewDataSource
    func numberOfTags(_ tagView: HTagView) -> Int {
        
        return arrTag.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String{
        return "\(arrTag[index]["keyword"]!)"
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType{
        return .select
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    // MARK: - HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
        arrSelectedTag = Array()
        for index in selectedIndices
        {
            arrSelectedTag.append(arrTag[index])
        }
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
