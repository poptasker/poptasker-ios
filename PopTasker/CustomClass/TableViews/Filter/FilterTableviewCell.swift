//
//  FilterTableviewCell.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import WARangeSlider
class FilterTableviewCell: UITableViewCell {

    @IBOutlet var slider: RangeSlider!
    @IBOutlet var minSliderValue: LabelNeueLightBlack!
    @IBOutlet var maxSliderValue: LabelNeueLightBlack!
    @IBOutlet var lblDetailTitle: LabelNeueLightBlack!
    @IBOutlet var lblSlidervalue: LabelNeueLightBlack!
    @IBOutlet var lblSliderValueType: LabelNeueLightBlack!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
