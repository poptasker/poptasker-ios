//
//  FilterTableHeaderCell.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class FilterTableHeaderCell: UITableViewCell {

    @IBOutlet var lblHeader: LabelNeueBold!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
