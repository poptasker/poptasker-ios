//
//  SettingsTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class SettingsTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var arrList = ["Help","Contact Us","About Us","Privacy"]
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
    super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.register(UINib(nibName:"SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .singleLine
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.tableFooterView = UIView()
        self.reloadData()
    }
    
    func createUserArray() {
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
    return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrList.count
    }
    
    //MARK: -
    //MARK: - Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return nil
    }
    //MARK: -
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        cell.lblName.text = arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return getProportionalHeight(height: 64)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0 //getProportionalHeight(height: 125)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return getProportionalHeight(height: 0)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
            if(self.blockTableViewDidSelectAtIndexPath != nil){
                self.blockTableViewDidSelectAtIndexPath!(indexPath, "")
            }
    
    }

}
