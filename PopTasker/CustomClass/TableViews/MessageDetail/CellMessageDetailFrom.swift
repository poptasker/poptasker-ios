//
//  CellMessageDetailFrom.swift
//  PopTasker
//
//  Created by Admin on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellMessageDetailFrom: UITableViewCell {

    @IBOutlet var imgFrom: UIImageView!
    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var viewBG: UIView!
    
    override func awakeFromNib(){
        super.awakeFromNib()
     imgFrom.setCornerRadius()
     imgFrom.setBorder(width: 1.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
