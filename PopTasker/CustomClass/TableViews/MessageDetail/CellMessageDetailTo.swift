//
//  CellMessageDetailTo.swift
//  PopTasker
//
//  Created by Admin on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellMessageDetailTo: UITableViewCell {

    @IBOutlet var viewBG: UIView!
    @IBOutlet var textMessage: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
