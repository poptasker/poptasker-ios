//
//  MessageDetail.swift
//  PopTasker
//
//  Created by Admin on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit
enum CellMessageType {
    case From
    case To
}
class DateTextItem: NSObject {
    var text: String = ""
    var insertDate: NSDate = NSDate()
}

class MessageDetail: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var txtTemp : UITextView?
    var minChatBGViewWidth : CGFloat = 30.0
    let maxChatBGViewWidth = UIScreen.main.bounds.size.width - 0.28*UIScreen.main.bounds.size.width
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var sectionsInTable = [String]()
    var sectionDataMessage = [String:[Message]]()
    var sortedKeysMessage = [String]()
    var UserId:String = ""
    var senderProfile:String!
    
    @IBAction func saveButton(sender: AnyObject) {

    }
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
       
        self.register(UINib(nibName:"CellMessageDetailFrom", bundle: nil), forCellReuseIdentifier: "CellMessageDetailFrom")
       
        self.register(UINib(nibName:"CellMessageDetailTo", bundle: nil), forCellReuseIdentifier: "CellMessageDetailTo")
        
        self.register(UINib(nibName:"CellMessageDetailHeader", bundle: nil), forCellReuseIdentifier: "CellMessageDetailHeader")

        // Swift 4.1 and below
//        self.rowHeight = UITableView.automaticDimension
//        self.estimatedRowHeight = UITableView.automaticDimension

        self.rowHeight = UITableView.automaticDimension
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .none
        self.separatorInset = .zero
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.saveButton(sender: self)
        self.reloadData()
        self.tableFooterView = UIView()
    }
    
    func displayDataInTextCellTO(cell : CellMessageDetailTo,data : Message){
        cell.textMessage.attributedText = getConfiguredAttributedString(text: data.messageText)
    }
    func displayDataInTextCellFrom(cell : CellMessageDetailFrom,data : Message){
        cell.txtMessage.attributedText = getConfiguredAttributedString(text: data.messageText)
        cell.imgFrom.sd_setImage(with: URL.init(string: senderProfile), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)
        cell.imgFrom.setCornerRadius()
        cell.imgFrom.contentMode = .scaleAspectFill
        let heightOfRow = self.calculateHeight(inString: data.messageText)
        self.setPropertiesForBG(sender: cell.viewBG, blueColor: false, height: heightOfRow > 40.0 ? heightOfRow+(heightOfRow*0.60):heightOfRow+(heightOfRow*1.3))
    }

    func getConfiguredAttributedString(text : String) -> NSAttributedString{
        let myAttribute = [ NSAttributedString.Key.font: Font.setHelvaticaNeueLTPro(font: .NeueLight, size: 20)!, NSAttributedString.Key.foregroundColor:UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)]
        let attributedString = NSAttributedString(string: text, attributes: myAttribute)
        return attributedString
    }
    
//    func configureCellSizeTo(cell : CellMessageDetailTo){
//        if txtTemp == nil{
//            txtTemp = UITextView()
//        }
//        txtTemp?.attributedText = cell.textMessage.attributedText
////        let tSize = txtTemp?.sizeThatFits(CGSize(width: maxChatBGViewWidth, height: CGFloat.greatestFiniteMagnitude))
////        cell.textMessage.frame.size.width = tSize!.width
////            < minChatBGViewWidth ? minChatBGViewWidth : tSize!.width
////        cell.textMessage.frame.size.height = tSize!.height
////        cell.textMessage.translatesAutoresizingMaskIntoConstraints = true
//        cell.contentView.reloadInputViews()
//    }
//    func configureCellSizeFrom(cell : CellMessageDetailFrom){
//        if txtTemp == nil{
//            txtTemp = UITextView()
//        }
//        txtTemp?.attributedText = cell.txtMessage.attributedText
////        let tSize = txtTemp?.sizeThatFits(CGSize(width: maxChatBGViewWidth, height: CGFloat.greatestFiniteMagnitude))
////        cell.txtMessage.frame.size.width = tSize!.width
////            < minChatBGViewWidth ? minChatBGViewWidth : tSize!.width
////        cell.txtMessage.frame.size.height = tSize!.height
////        cell.txtMessage.textContainerInset = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 15)
////        cell.txtMessage.translatesAutoresizingMaskIntoConstraints = true
//        cell.contentView.reloadInputViews()
//    }

    func setPropertiesForBG(sender:UIView,blueColor:Bool, height:CGFloat = 0.0){
        sender.layer.borderColor = UIColor.white.cgColor
        sender.layer.borderWidth = 1
        if(blueColor){
//            sender.layer.roundCorners(corners: [.topLeft, .bottomRight,.bottomLeft], radius: 20)
            sender.setCornerRadius()
            sender.backgroundColor = UIColor.init(patternImage: GradientImage.gradientTheamBackgroundFrame(view: sender))
        }else{
            sender.setCornerRadius()
            sender.backgroundColor = UIColor(patternImage: GradientImage.gradientTheamBackgroundFrameforPurple(view: sender,isMessageDetail:true, height:height))
        }
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDataMessage.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let msgs = self.sectionDataMessage[self.sortedKeysMessage[section]]
        {
            return msgs.count
        }
        
        return 0
    }
    
    //MARK: -
    //Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.dequeueReusableCell(withIdentifier: "CellMessageDetailHeader") as! CellMessageDetailHeader
        let userData = sortedKeysMessage[section]
        cell.lblHeader.text = userData
        return cell
    }

    //MARK: -
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tdata = self.sectionDataMessage[self.sortedKeysMessage[indexPath.section]]
        let data = tdata![indexPath.row]
        if data.user == UserId{
            let cell = self.dequeueReusableCell(withIdentifier: "CellMessageDetailTo") as! CellMessageDetailTo
            displayDataInTextCellTO(cell:cell,data: data)
            self.setPropertiesForBG(sender: cell.viewBG, blueColor: true)
            return cell
        }else{
            let cell = self.dequeueReusableCell(withIdentifier: "CellMessageDetailFrom") as! CellMessageDetailFrom
            displayDataInTextCellFrom(cell:cell,data: data)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tdata = self.sectionDataMessage[self.sortedKeysMessage[indexPath.section]]
        let data = tdata![indexPath.row]
        let heightOfRow = self.calculateHeight(inString: data.messageText)
        return (heightOfRow + 60.0)
    }
    
    func calculateHeight(inString:String) -> CGFloat{
        let txt = getConfiguredAttributedString(text: inString)
        let rect : CGRect = txt.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        let requredSize:CGRect = rect
        return requredSize.height
    }
    
}
extension CALayer {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        mask = shape
    }
}
