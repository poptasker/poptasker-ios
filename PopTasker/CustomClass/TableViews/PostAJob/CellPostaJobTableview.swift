//
//  CellPostaJobTableview.swift
//  PopTasker
//
//  Created by Admin on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import GrowingTextView
class CellPostaJobTableview: UITableViewCell,UITextViewDelegate{

    @IBOutlet var lblTitle: LabelNeueLightBlack!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var Cellbutton: UIButton!
    @IBOutlet var txtValue: GrowingTextView!
    let datePickerView:UIDatePicker = UIDatePicker()
    var timePickerView:UIDatePicker = UIDatePicker()
    var blockTableViewDidSelect:((Int,String)->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        timePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        // Initialization code
    }
//    override func prepareForReuse() {
//        super.prepareForReuse()
////        txtValue.text = nil
////        txtValue.keyboardType = UIKeyboardType.default
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
        if(self.blockTableViewDidSelect != nil){
            self.blockTableViewDidSelect!(self.Cellbutton.tag, textView.text!)
        }
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        //dateTextField.text = dateFormatter.stringFromDate(sender.date)
        
    }
}
