//
//  CellImageTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellImageTableView: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource
{
    @IBOutlet var lblTitle: LabelNeueLightBlack!
    @IBOutlet var Cellbutton: UIButton!
    @IBOutlet weak var btnValue: UIButton!
    @IBOutlet var imgCollection : UICollectionView!
    var arrImg = [UIImage]()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        imgCollection.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.imgView.image = arrImg[indexPath.row]
        cell.btnClose.addTarget(self, action: #selector(btnClosePress(sender:)), for: .touchUpInside)
        cell.btnClose.tag = indexPath.row
        return cell
    }
    
    @IBAction func btnClosePress(sender:UIButton)
    {
        print(sender.tag)
        arrImg.remove(at: sender.tag)
        imgCollection.reloadData()
        if arrImg.count == 0
        {
            btnValue.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
