//
//  PostAjobTableview.swift
//  PopTasker
//
//  Created by Admin on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

enum PostAjobCellType {
    case JobTitle
    case JobDesc
    case Keywords
    case Address
    case Date
    case When
    case UploadPic
    case Wages
    case PostButton
}
class PostAjobTableview: UITableView, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {
    var arrImgs = [UIImage]()
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var blockTableViewDidSelecth:((Int,String)->Void)?
    var blockTableViewDidSelecLocation:((Int,String)->Void)?
    var userFieldArray = [PostAjob]()
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: -1, left: 0, bottom: 0, right: 0)
        
        userFieldArray = [
            PostAjob.init(title: "Job Title", value: "Type in",valueColor: "Gray", righButtonimgName: "No Image", cellType: PostAjobCellType.JobTitle, arr:self.arrImgs),
            PostAjob.init(title: "Job Description", value: "Type in",valueColor: "Gray", righButtonimgName: "No Image", cellType: PostAjobCellType.JobDesc, arr: self.arrImgs),
            PostAjob.init(title: "Keywords", value: "Select Keywords",valueColor: "Blue", righButtonimgName: "No Image", cellType: PostAjobCellType.Keywords, arr:self.arrImgs),
            PostAjob.init(title: "Address", value: "Type in",valueColor: "Blue", righButtonimgName: "locate", cellType: PostAjobCellType.Address, arr:self.arrImgs),
            PostAjob.init(title: "Date", value: "Select Date",valueColor: "Blue", righButtonimgName: "calander", cellType: PostAjobCellType.Date, arr:self.arrImgs),
            PostAjob.init(title: "When", value: "Select Time",valueColor: "Blue", righButtonimgName: "Locate", cellType: PostAjobCellType.When, arr:self.arrImgs),
            PostAjob.init(title: "Upload Picture", value: "Select images", valueColor: "Blue", righButtonimgName: "camara", cellType: PostAjobCellType.UploadPic, arr: self.arrImgs),
            PostAjob.init(title: "Wages($)", value: "Type in",valueColor: "Gray", righButtonimgName: "No Image", cellType: PostAjobCellType.Wages, arr:self.arrImgs)
        ]
        
        // REUSE CELL
        self.register(UINib(nibName:"CellPostaJobTableview", bundle: nil), forCellReuseIdentifier: "CellPostaJobTableview")
        // REUSE CELL BLUE
        self.register(UINib(nibName:"CellPostaJobTableview2", bundle: nil), forCellReuseIdentifier: "CellPostaJobTableview2")

        // REUSE CELL IMAGE
        
        self.register(UINib(nibName:"CellImageTableView", bundle: nil), forCellReuseIdentifier: "CellImageTableView")
        
        //Footer
        self.register(UINib(nibName:"CellPostAjobFooter", bundle: nil), forCellReuseIdentifier: "CellPostAjobFooter")
        
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        //        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.bounces = false
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
        
    }
    
    func createUserArray(arrImg : [UIImage]) {
        self.arrImgs = arrImg
        DispatchQueue.main.async
            {
            self.reloadData()
        }
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userFieldArray.count
    }
    
    //MARK: -
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellPostAjobFooter") as! CellPostAjobFooter
        cell.btnPost.tag = 8
        cell.btnPost.addTarget(self, action: #selector(self.pressed(sender:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let userData = self.userFieldArray[indexPath.row]
        let valColor = userData.valueColor

        if(valColor == "Gray")
        {
            let cell = self.dequeueReusableCell(withIdentifier: "CellPostaJobTableview") as! CellPostaJobTableview
            
            cell.blockTableViewDidSelect = {(int,str) -> Void in
                if str == ""
                {
                    self.userFieldArray[int].value = "Type in"
                }
                else
                {
                    self.userFieldArray[int].value = str
                }
//                cell.txtValue.text = userData.value
                if userData.title == "Type in"
                {
                    cell.txtValue.placeholder = userData.value
                    
                }
                else
                {
                    cell.lblTitle.text = userData.title
                    
                }
                self.reloadData()
            }
            
            cell.lblTitle.text = userData.title
            cell.txtValue.placeholder = userData.value

            cell.txtValue.placeholderColor = UIColor(white: 0.7, alpha: 1)
//            cell.txtValue.isPlaceholderScrollEnabled = true
//            cell.txtValue.leftViewOrigin = CGPoint(x: 8, y: 8)
            if(userData.righButtonimgName == "No Image"){
                cell.Cellbutton.isHidden = true
            }else{
                cell.Cellbutton.isHidden = false
            }
            let img = UIImage(named: userData.righButtonimgName)
            cell.Cellbutton.setImage(img, for: .normal)
            cell.Cellbutton.tag = indexPath.row
            cell.Cellbutton.addTarget(self, action: #selector(btnRightPress(sender:)), for: UIControl.Event.touchUpInside)
            if(userData.cellType == .Date){
               cell.txtValue.inputView = cell.datePickerView
            }
            else if(userData.cellType == .When){
                cell.txtValue.inputView = cell.timePickerView
            }else if(userData.cellType == .Wages){
                cell.txtValue.keyboardType = .numberPad
            }
            else
            {
                cell.txtValue.keyboardType = .default
            }
            return cell

        }else{
            if userData.cellType == PostAjobCellType.UploadPic
            {
                let cell = self.dequeueReusableCell(withIdentifier: "CellImageTableView") as! CellImageTableView
                cell.lblTitle.text = userData.title
                cell.btnValue.tag = indexPath.row
                cell.btnValue.setTitle(userData.value, for: .normal)
                //  cell.btnValue.addTarget(self, action: , for: )
                cell.btnValue.addTarget(self, action: #selector(self.pressed(sender:)), for: .touchUpInside)
                cell.arrImg = self.arrImgs
                if self.arrImgs.count > 0
                {
                    cell.btnValue.isHidden = true
                }
                cell.imgCollection.reloadData()
                if(userData.righButtonimgName == "No Image"){
                    cell.Cellbutton.isHidden = true
                }
                else
                {
                    cell.Cellbutton.isHidden = false
                }
                
                let img = UIImage(named: userData.righButtonimgName)
                cell.Cellbutton.setImage(img, for: .normal)
                cell.Cellbutton.tag = indexPath.row
                cell.Cellbutton.addTarget(self, action: #selector(btnRightPress(sender:)), for: UIControl.Event.touchUpInside)
                return cell
            }
            else
            {
                let cell = self.dequeueReusableCell(withIdentifier: "CellPostaJobTableview2") as! CellPostaJobTableview2
                cell.lblTitle.text = userData.title
                cell.btnValue.tag = indexPath.row
                cell.txtView.toolbarPlaceholder = userData.value
                cell.txtView.text = userData.value
                cell.txtView.sizeToFit()

                cell.btnValue.addTarget(self, action: #selector(self.pressed(sender:)), for: .touchUpInside)
                
                if(userData.righButtonimgName == "No Image"){
                    cell.Cellbutton.isHidden = true
                }else{
                    cell.Cellbutton.isHidden = false
                }
              
                let img = UIImage(named: userData.righButtonimgName)
                cell.Cellbutton.setImage(img, for: .normal)
                cell.Cellbutton.tag = indexPath.row
                cell.Cellbutton.addTarget(self, action: #selector(btnRightPress(sender:)), for: UIControl.Event.touchUpInside)
                return cell
            }
        }
    }
    @objc func btnRightPress(sender: UIButton!)
    {
        print("pressed")
        if sender.tag == 3
        {
            if(self.blockTableViewDidSelecLocation != nil){
                self.blockTableViewDidSelecLocation!(sender.tag,"")
            }
        }
        if sender.tag == 6
        {
            if(self.blockTableViewDidSelecLocation != nil){
                self.blockTableViewDidSelecLocation!(sender.tag,"")
            }
        }
    }
    @objc func pressed(sender: UIButton!) {
        if(self.blockTableViewDidSelecth != nil){
            self.blockTableViewDidSelecth!(sender!.tag,"")
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 6
        {
            return 100
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if(self.blockTableViewDidSelectAtIndexPath != nil){
        //            self.blockTableViewDidSelectAtIndexPath!(indexPath, arrSearchText[indexPath.row].image)
        //        }
        
    }
    
}


