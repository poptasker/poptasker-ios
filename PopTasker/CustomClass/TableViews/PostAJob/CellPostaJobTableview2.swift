//
//  CellPostaJobTableview2.swift
//  PopTasker
//
//  Created by Admin on 17/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import GrowingTextView
class CellPostaJobTableview2: UITableViewCell,GrowingTextViewDelegate {
  
    @IBOutlet var lblTitle: LabelNeueLightBlack!
    @IBOutlet var txtView : GrowingTextView!
    @IBOutlet var Cellbutton: UIButton!
    @IBOutlet weak var btnValue: UIButton!
    var blockTableViewDidSelect:((Int,String)->Void)?
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(self.blockTableViewDidSelect != nil){
            self.blockTableViewDidSelect!(self.Cellbutton.tag, textView.text!)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        txtView.sizeToFit()
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
