//
//  TransactionsTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
enum CellTransactionType
{
    case Spent
    case Earn
}
class TransactionsTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
    
    var arrTransaction : [TransactionModel]! = [TransactionModel]()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()

        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        self.register(UINib(nibName:"CellTransactionTableView", bundle: nil), forCellReuseIdentifier: "CellTransactionTableView")
        
        self.estimatedRowHeight = 300
        self.rowHeight = UITableView.automaticDimension
        //        self.backgroundColor = UIColor.appTheameBackgroundColor()
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.separatorStyle = .singleLine
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        self.tableFooterView = UIView()
        // self.setTableFooter()
    }

    func createUserArray(transaction:[TransactionModel])
    {
        arrTransaction = transaction
        self.reloadData()
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrTransaction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellTransactionTableView") as! CellTransactionTableView
        let model = arrTransaction[indexPath.row]
        cell.lblTitle.text = model.titleLable!
        cell.lblCost.text = model.cost!
        cell.lblTime.text = model.date!
        cell.lblAddress.text = model.address!
        cell.lblAgo.text = model.timeAgo!
        cell.lblThanks.text = model.description!

        if model.cellType == CellTransactionType.Earn
        {
            cell.lblCost.textColor = UIColor.setGreenColor()
        }
        else
        {
            cell.lblCost.textColor = UIColor.setCustomRedColor()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
