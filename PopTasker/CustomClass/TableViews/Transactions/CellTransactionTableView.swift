//
//  CellTransactionTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellTransactionTableView: UITableViewCell {
    
    @IBOutlet var lblTitle : LabelNeueBoldCustomBlack!
    @IBOutlet var lblCost : LabelNeueLight!
    @IBOutlet var lblTime : LableNeueLightGray119!
    @IBOutlet var lblAddress : LableNeueLightGray119!
    @IBOutlet var lblThanks : LabelNeueLight!
    @IBOutlet var lblAgo : LabelNeueLight!
    @IBOutlet var thanksUpConstraint : NSLayoutConstraint!
    @IBOutlet var AgoUpConstraint : NSLayoutConstraint!
    var isEarn : Bool! = true
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        if IS_iPHONE_5 || IS_iPHONE_4
        {
            thanksUpConstraint.constant = 10
            AgoUpConstraint.constant = 8
        }
        if isEarn
        {
            lblCost.textColor = UIColor.setGreenColor()
        }
        else
        {
            lblCost.textColor = UIColor.setCustomRedColor()
        }
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
