//
//  MessagesTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 04/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class MessagesTableView: UITableView,UITableViewDelegate,UITableViewDataSource{
    
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var list = [Conversation]()
    let obj = Conversation()
    var conversationList = [JSONDICTIONARY]()
    var KuserId:Int!
    var isNextPage : Bool! = false
    var page : Int! = 1
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        // REUSE CELL
        self.register(UINib(nibName:"CellMessagesTableView", bundle: nil), forCellReuseIdentifier: "CellMessagesTableView")
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.separatorStyle = .singleLine
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.tableFooterView = UIView()
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        if let userId = dataDict["user_id"]{
            KuserId = (userId as! Int)
        }
    }
    
    // MARK: - Fucntions
    
    func sortArray(){
        
//        print(list)
//        self.list.sort(by:{ (obj2, obj1) -> Bool in
//            if obj2.lastMessage.timestamp.count > 0 && obj1.lastMessage.timestamp.count > 0{
//                if Int64(obj2.lastMessage.timestamp)! > Int64(obj1.lastMessage.timestamp)!{
//                    print(list)
//                    return true
//                }
//            }else{
//                if obj2.lastMessage.timestamp.count > 0{
//                    print(list)
//                    return true
//                }
//                if obj1.lastMessage.timestamp.count > 0{
//                    print(list)
//                    return true
//                }
//                print(list)
//            }
//            return false
//        })
        
        self.list = self.list.sorted{
            $0.lastMessage.timestamp > $1.lastMessage.timestamp
        }
    }
    
    func getMessagesList()
    {
        AppDel.showHUD()
        ApiManager.shared.getData(url: kMessagelist, completion: { (dict) in
            
            if let dictData = dict["data"] as? JSONDICTIONARY {
                
                if let listing  = dictData["listing"] as? [JSONDICTIONARY] {
                    if listing.count > 0{
                        for chat in listing {
                            self.obj.updateUserData(data: chat)
                            self.conversationList.append(chat)
                            self.isHidden = false
                        }
                    }else{
                        UtilityClass.showToast(view: self, message: "No message found")
                        AppDel.hideHUD()
                    }
                }
                else
                {
                    UtilityClass.showToast(view: self, message: "No message found")
                    self.isHidden = true
                    AppDel.hideHUD()
                }
            }else{
                UtilityClass.showToast(view: self, message: "No message found")
                self.isHidden = true
                AppDel.hideHUD()
            }
        })
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count > 0 ? self.list.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "CellMessagesTableView") as! CellMessagesTableView
        if list.count > 0{
            let data = list[indexPath.row]
            cell.cellData = data
            if let dict = data.userDictionary{
                for(key,_) in dict{
                    if (key as! String) != "user_\(String(describing: KuserId))"{
                        cell.displayCellData(userid: "\(key as! String)")
                    }
                }
            }
            data.isValueChanged = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getProportionalHeight(height: 97.4)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.blockTableViewDidSelectAtIndexPath != nil){
            self.blockTableViewDidSelectAtIndexPath!(indexPath, "")
        }
    }
}

