//
//  CellMessagesTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 04/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class CellMessagesTableView: UITableViewCell
{

    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : LableNeueLightBlack0023!
    @IBOutlet var lastMessage : LableNeueLightGray119!
    @IBOutlet var lblDate : LableNeueLightGray119!
    @IBOutlet weak var lblMessageCount: UILabel!
    
    var cellData = Conversation()
    
    override func awakeFromNib(){
        super.awakeFromNib()
        imgView.setCornerRadius()
        imgView.setBorder(width: 2.5)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func displayCellData(userid:String){
        if cellData.unReadMessageCountDict != nil{
            let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
            let dataDict = userdata["data"] as! JSONDICTIONARY
            if let KuserId = dataDict["user_id"]{
                if ((cellData.unReadMessageCountDict.value(forKey: "user_\(String(describing: KuserId))") as? String) != nil && (cellData.unReadMessageCountDict.value(forKey: "user_\(String(describing: KuserId))") as? String) != "0"){
                    lblMessageCount.isHidden = false
                    self.lblMessageCount.text = cellData.unReadMessageCountDict.value(forKey: "user_\(String(describing: KuserId))") as? String
                    self.lblMessageCount.layer.cornerRadius = self.lblMessageCount.frame.width/2
                    self.lblMessageCount.clipsToBounds = true
                }else{
                    self.lblMessageCount.isHidden = true
                }
            }
        }else{
            self.lblMessageCount.isHidden = true
        }
            lastMessage.text = cellData.lastMessage.messageText
            lblName.text = cellData.title
        if cellData.ProfilePic != nil {
          imgView.sd_setImage(with: URL.init(string: "\(cellData.ProfilePic!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)
        }
            if cellData.lastMessage.timestamp.count > 0{
            let dateString =  UtilityClass.getFormattedDateTime(secondss: Double(cellData.lastMessage.timestamp)!, isChatDetailScreen: false)
            self.lblDate.text = dateString
        }
        else{
            self.lblDate.text = "--:--"
        }
    }
}
