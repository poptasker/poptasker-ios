//
//  MenuTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 06/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class MenuTableView: UITableView, UITableViewDataSource, UITableViewDelegate{
    
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var blockTableViewDidSelectHeader:((String)->Void)?

    var arrMenu = [["title":"Transactions", "icon":"menu_transaction"],["title":"Favorites", "icon":"menu_favorite"],["title":"Applicants", "icon":"menu_applicant"],(["title":"Settings", "icon":"menu_settings"]),(["title":"Logout", "icon":"menu_logout"])]
    
    //MARK:- INIT
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.register(UINib(nibName:"CellMenuTableView", bundle: nil), forCellReuseIdentifier: "CellMenuTableView")
        
        //Header
        self.register(UINib(nibName:"CellMenuHeader", bundle: nil), forCellReuseIdentifier: "CellMenuHeader")
        
        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        self.isScrollEnabled = true
        self.bounces = false
//        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        self.tableFooterView = UIView()
    }
    
    func createUserArray() {
    }
    
    
    //MARK:- TableView DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMenu.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return getProportionalHeight(height: 60)
    }
    
    //MARK: -
    //MARK: - Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellMenuHeader") as! CellMenuHeader
        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = UIColor.white.cgColor
        bottomBorder.frame = CGRect.init(x: 23, y: cell.frame.size.height - 5, width: cell.frame.size.width - 46, height: 1)
        cell.layer.addSublayer(bottomBorder)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.delegate = self as? UIGestureRecognizerDelegate
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        cell.contentView.addGestureRecognizer(tapRecognizer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 155
    }
    
    //MARK:- cellForRowAt
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.dequeueReusableCell(withIdentifier: "CellMenuTableView") as! CellMenuTableView
        cell.imgMenu.image = UIImage(named: arrMenu[indexPath.row]["icon"]!)
        cell.lblName.text = arrMenu[indexPath.row]["title"]!
        return cell
    }

    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.blockTableViewDidSelectAtIndexPath != nil){
            self.blockTableViewDidSelectAtIndexPath!(indexPath, "")
        }
    }
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        self.blockTableViewDidSelectHeader!("")
    }
}
