//
//  CellMenuHeader.swift
//  PopTasker
//
//  Created by Urja_Macbook on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import SDWebImage
class CellMenuHeader: UITableViewCell
{
    @IBOutlet var lblUserName : LabelNeueLight!
    @IBOutlet var imgUserProfile : UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        
        lblUserName.text = "\(dataDict["firstname"]!) \(dataDict["lastname"]!)"
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.height/2
       
        if let userProfile = getModelDataFromUserDefaults(key: "userProfile") as? JSONDICTIONARY
        {
            if userProfile["profilepic"] as? String != ""
            {

                imgUserProfile.sd_setImage(with: URL.init(string: "\( userProfile["profilepic"]!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)
            }
            else
            {
                let first = lblUserName.text?.first
                imgUserProfile.image = LetterImageGenerator.imageWith(name: "\(first!)")
            }
        }
        else
        {
            let first = lblUserName.text?.first
            imgUserProfile.image = LetterImageGenerator.imageWith(name: "\(first!)")
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
