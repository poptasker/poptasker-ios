//
//  RegisterTableView.swift
//  PopTasker
//
//  Created by Rohan on 21/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class FavoriteListTable: UITableView, UITableViewDataSource, UITableViewDelegate {

    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var blockTableViewFavorite:((Int)->Void)?

    var arrFav : [JSONDICTIONARY]! = [JSONDICTIONARY]()
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.register(UINib(nibName:"FavoriteListCell", bundle: nil), forCellReuseIdentifier: "FavoriteListCell")

        self.estimatedRowHeight = 200
        self.rowHeight = UITableView.automaticDimension
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .none
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray()
    {
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrFav.count
    }
    
    //MARK: -
    //MARK: - Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    //MARK: -
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "FavoriteListCell") as! FavoriteListCell
        let dict = arrFav[indexPath.row]
        cell.lblName.text = "\(dict["firstname"]!) \(dict["lastname"]!)"
        cell.lblSummary.text = "\(dict["summary"]!)"
        cell.lblEndor.text = "# endorsements \(dict["endorsements"]!)  #of job completed \(dict["completedtasks"]!)"
        cell.rateView.rating = Float("\(dict["ratings"]!)")!
        
        let img = LetterImageGenerator.imageWith(name: "\(dict["firstname"]!)")
        if dict["profilepic"] as? String != ""
        {
            let url = dict["profilepic"] as? String
            cell.imgView!.sd_setImage(with: URL.init(string: url!), completed: nil)
        }
        else
        {
            cell.imgView.image = img
        }
        cell.imgFav.tag = indexPath.row
        cell.imgFav.addTarget(self, action: #selector(btnFavPress(sender:)), for: .touchUpInside)
        if Int("\(dict["is_favorite"]!)") == 1
        {
            cell.imgFav.setImage(#imageLiteral(resourceName: "ApplicantLike"), for: .normal)
        }
        else
        {
            cell.imgFav.setImage(#imageLiteral(resourceName: "ApplicantUnLike"), for: .normal)
        }
        return cell
    }
    @objc func btnFavPress(sender:UIButton)
    {
        let dict = arrFav[sender.tag]
        var fav = "\(dict["is_favorite"]!)"
        
        if fav == "1"
        {
            fav = "0"
        }
        else
        {
            fav = "1"
        }
        
        ApiManager.shared.postDataWithHeader(url: kFavorite, params: ["employee_id":"\(dict["user_id"]!)","is_favorite":fav], completion: { (dict) in
            UtilityClass.showToast(view: self.superview!, message: "\(dict["message"]!)")
            if(self.blockTableViewFavorite != nil)
            {
                self.blockTableViewFavorite!(sender.tag)
            }
            print(dict)
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension //getProportionalHeight(height: 68)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0 //getProportionalHeight(height: 125)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 0)
    }
    
    //MARK:- DID SELECT      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.blockTableViewDidSelectAtIndexPath != nil)
        {
            self.blockTableViewDidSelectAtIndexPath!(indexPath, "")
        }
    }
}
