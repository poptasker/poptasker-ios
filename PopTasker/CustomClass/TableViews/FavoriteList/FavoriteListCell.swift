//
//  FavoriteListCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 13/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class FavoriteListCell: UITableViewCell, RateViewDelegate {
    
    @IBOutlet var rateView: RateView!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblSummary : UILabel!
    @IBOutlet var lblEndor : UILabel!
    //    @IBOutlet var lblCompletedJob : UILabel!
    @IBOutlet var imgFav : UIButton!
    var isFav : Bool! = false
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.rateView.fullSelectedImage = #imageLiteral(resourceName: "RateFill")
        self.rateView.halfSelectedImage = self.rateView.fullSelectedImage
        self.rateView.notSelectedImage = #imageLiteral(resourceName: "RateBlank")
        self.rateView.editable = false
        self.rateView.maxRating = 5
        self.rateView.leftMargin = 0
        self.rateView.midMargin = 1
        self.rateView.rating = 3
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    func rateView(_ rateView: RateView!, ratingDidChange rating: Float)
    {
        
    }
}
