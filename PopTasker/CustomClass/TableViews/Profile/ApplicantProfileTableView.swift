//
//  ApplicantTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantProfileTableView: UITableView , UITableViewDataSource, UITableViewDelegate
{
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var blockTableViewAwardJob:((Int,JSONDICTIONARY)->Void)?

    var dict = JSONDICTIONARY()
    var arrReviews = [JSONDictionary]()
    var isAwarded : Int! = 0
    var isFromApplicant : Bool = false
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        //Header
        self.register(UINib(nibName:"MyProfileReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileReviewsTableViewCell")
        
        self.register(UINib(nibName:"ApplicantProfileHeaderCell", bundle: nil), forCellReuseIdentifier: "ApplicantProfileHeaderCell")
        
        //Footer
        self.register(UINib(nibName:"ApplicantListFooter", bundle: nil), forCellReuseIdentifier: "ApplicantListFooter")
        self.estimatedRowHeight = 200
        self.showsVerticalScrollIndicator = false
        self.rowHeight = UITableView.automaticDimension
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .singleLine
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray(dict:JSONDictionary,arr : [JSONDictionary]) {
        self.dict = dict
        self.arrReviews = arr
        if self.arrReviews.count == 0
        {
            UtilityClass.showAlert(str: "No reviews yet.")
        }
        self.reloadData()
    }
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFromApplicant
        {
            if isAwarded == 0
            {
                return 3
            }
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return arrReviews.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = self.dequeueReusableCell(withIdentifier: "ApplicantProfileHeaderCell") as! ApplicantProfileHeaderCell
            if self.dict.keys.count > 0
            {
                let img = LetterImageGenerator.imageWith(name: "\(dict["firstname"]!)")
                if dict["profilepic"] as? String != ""
                {
                    cell.imgView.sd_setImage(with: URL.init(string: "\(self.dict["profilepic"]!)"), placeholderImage: img, options: [], completed: nil)
                }
                else
                {
                    cell.imgView.image = img
                }
                cell.lblName.text = "\(self.dict["firstname"]!) \(self.dict["lastname"]!)"
                cell.lblSummary.text = "\(self.dict["summary"]!)"
                cell.lblEducation.text = "\(self.dict["education"]!)"
                cell.lblPassion.text = "\(self.dict["passions"]!)"
                cell.lblSkill.text = "\(self.dict["skills"]!)"
                if cell.lblSummary.text == ""
                {
                    cell.lblSummary.text = "--"
                }
                if cell.lblEducation.text == ""
                {
                    cell.lblEducation.text = "--"
                }
                if cell.lblPassion.text == ""
                {
                    cell.lblPassion.text = "--"
                }
                if cell.lblSkill.text == ""
                {
                    cell.lblSkill.text = "--"
                }
                cell.lblJobCompleted.text = "\(self.dict["completedtasks"]!)\n# of jobs completed"
                cell.lblEndorsmentCompleted.text = "\(self.dict["endorsements"]!)\n# of endorsements"
            }
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = self.dequeueReusableCell(withIdentifier: "MyProfileReviewsTableViewCell") as! MyProfileReviewsTableViewCell
            let dict = arrReviews[indexPath.row] as JSONDictionary
            cell.lblName.text = "\(dict["firstname"]!) \(dict["lastname"]!)"
            cell.lblDate.text = "\(dict["review_date"]!)"
            cell.lblComment.text = "\(dict["comments"]!)"
            cell.rateView.rating = Float("\(dict["rating"]!)")!
            cell.imgView.sd_setImage(with: URL.init(string: "\(dict["profilepic"]!)"), placeholderImage:LetterImageGenerator.imageWith(name: "\(dict["firstname"]!)"), options: [], completed: nil)

            return cell
        }
        else
        {
            let cell = self.dequeueReusableCell(withIdentifier: "ApplicantListFooter") as! ApplicantListFooter
            cell.btnAwardJob.addTarget(self, action: #selector(awardJob(sender:)), for: .touchUpInside)
            cell.btnAwardJob.tag = indexPath.row
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 660
        }
        else if indexPath.section == 1
        {
            return getProportionalHeight(height: 133.7)
        }
        else
        {
            return getProportionalHeight(height: 112.3)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return getProportionalHeight(height: 0)
    }

    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if(self.blockTableViewDidSelectAtIndexPath != nil){
        //            self.blockTableViewDidSelectAtIndexPath!(indexPath, arrSearchText[indexPath.row].image)
        //        }
        
    }
    
    @objc func awardJob(sender:UIButton)
    {
        if(self.blockTableViewAwardJob != nil){
            self.blockTableViewAwardJob!(sender.tag, dict)
        }
    }
}
