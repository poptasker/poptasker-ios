//
//  MyProfileTableView.swift
//  PopTasker
//
//  Created by Urja_Macbook on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
enum CellProfileType {
    case Summary
    case Education
    case Passion
    case Skill
    case PhoneNumber
    case Address
    case LinkedIn
    case Image
    case Name

}
class MyProfileTableView: UITableView, UITableViewDataSource, UITableViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate
{
    var imagePicker = UIImagePickerController()
    var arrReviews = [JSONDictionary]()
    var blockTableViewDidSelectAtIndexPath:((IndexPath,String)->Void)?
    var userProfileArray = [
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.Summary),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.Education),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.Passion),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.Skill),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.PhoneNumber),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.Address),
        MyProfile.init(placeHolder: "Type In", txtValue: "", cellType: CellProfileType.LinkedIn),
        MyProfile.init(placeHolder: "", txtValue: "", cellType: CellProfileType.Image,img:UIImage()),
        MyProfile.init(placeHolder: "", txtValue: "", cellType: CellProfileType.Name)
        ]
    
    //MARK:- INIT
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        //Header
        self.register(UINib(nibName:"MyProfileHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileHeaderTableViewCell")
        
        self.register(UINib(nibName:"MyProfileReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileReviewsTableViewCell")
        
        self.register(UINib(nibName:"MyProfileSaveTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileSaveTableViewCell")
        
        self.estimatedRowHeight = 200
        self.showsVerticalScrollIndicator = false
        self.rowHeight = UITableView.automaticDimension
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = true
        self.bounces = true
        self.separatorStyle = .singleLine

        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        imagePicker.delegate = self
        // self.setTableFooter()
    }
    
    func createUserArray(dict:JSONDictionary,arr : [JSONDictionary]) {
        self.arrReviews = arr
        if self.arrReviews.count == 0
        {
            UtilityClass.showAlert(str: "No reviews yet.")
        }
        self.reloadData()
    }
    
    
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return arrReviews.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = self.dequeueReusableCell(withIdentifier: "MyProfileHeaderTableViewCell") as! MyProfileHeaderTableViewCell
            cell.selectionStyle = .none
            cell.blockTableViewDidSelect = {(int,str) -> Void in
                self.userProfileArray[int].txtValue = str
                self.reloadData()
            }
            cell.blockTableViewSelectImage = {(str) -> Void in
                self.selectImage()
            }

            cell.txtSummary.text = userProfileArray[0].txtValue!
            cell.txtEducation.text = userProfileArray[1].txtValue!
            cell.txtPssions.text = userProfileArray[2].txtValue
            cell.txtSkills.text = userProfileArray[3].txtValue!
            cell.txtPhoneNumber.text = userProfileArray[4].txtValue!
            cell.txtHomeAddress.text = userProfileArray[5].txtValue!
            cell.txtLinkedin.text = userProfileArray[6].txtValue
            cell.imgView.image = userProfileArray[7].img
          
            cell.lblName.text = userProfileArray[8].txtValue!
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = self.dequeueReusableCell(withIdentifier: "MyProfileReviewsTableViewCell") as! MyProfileReviewsTableViewCell
            let dict = arrReviews[indexPath.row] as JSONDictionary
            cell.lblName.text = "\(dict["firstname"]!) \(dict["lastname"]!)"
            cell.lblDate.text = "\(dict["review_date"]!)"
            cell.lblComment.text = "\(dict["comments"]!)"
            cell.rateView.rating = Float("\(dict["rating"]!)")!
            return cell
        }
        else
        {
            let cell = self.dequeueReusableCell(withIdentifier: "MyProfileSaveTableViewCell") as! MyProfileSaveTableViewCell
            cell.btnSave.addTarget(self, action: #selector(btnSavePress), for: .touchUpInside)
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 1
        {
            return getProportionalHeight(height: 133.7)
        }
        else
        {
            return getProportionalHeight(height: 112.7)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 0)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return getProportionalHeight(height: 0)
    }
    
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func selectImage() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
//        switch UIDevice.current.userInterfaceIdiom {
//        case .pad:
//            alert.popoverPresentationController?.sourceView = self.view
//            alert.popoverPresentationController?.sourceRect = sender.bounds
//            alert.popoverPresentationController?.permittedArrowDirections = .up
//        default:
//            break
//        }
        
        AppDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    @objc func btnSavePress()
    {
        if validation()
        {
            let str = "\(self.userProfileArray[8].txtValue!)"
            let arr = str.components(separatedBy: " ")
            
            let param = ["firstname":"\(arr[0])",
                "lastname":"\(arr[1])",
                "summary":"\(self.userProfileArray[0].txtValue!)",
                "education":"\(self.userProfileArray[1].txtValue!)",
                "skills":"\(self.userProfileArray[3].txtValue!)",
                "passions":"\(self.userProfileArray[2].txtValue!)",
                "phone":"\(self.userProfileArray[4].txtValue!)",
                "address":"\(self.userProfileArray[5].txtValue!)",
                "linkedin":"\(self.userProfileArray[6].txtValue!)"]
            
            print(param)
            AppDel.showHUD()
            
            ApiManager.shared.PostWithImage(url: kUserEdit, imageData: [self.userProfileArray[7].img.pngData()!], parameters: param)
            { (dict) in
                print(dict)
                AppDel.hideHUD()
                UtilityClass.showAlert(str: "\(dict["message"]!)")
            }
//            ApiManager.shared.postDataWithHeader(url: kUserEdit, params: param) { (dict) in
//                print(dict)
//                AppDel.hideHUD()
//                UtilityClass.showAlert(str: "\(dict["message"]!)")
//            }
        }
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            AppDel.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            AppDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        AppDel.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage) != nil {
            // imageViewPic.contentMode = .scaleToFill
            self.userProfileArray[7].img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
            self.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func validation() -> Bool {
        if self.userProfileArray[0].txtValue.count != 0
        {
            if self.userProfileArray[1].txtValue.count != 0
            {
                if self.userProfileArray[2].txtValue.count != 0
                {
                    if self.userProfileArray[3].txtValue.count != 0
                    {
                        if self.userProfileArray[4].txtValue.count != 0
                        {
                            if self.userProfileArray[5].txtValue.count != 0
                            {
                                if self.userProfileArray[6].txtValue.count != 0
                                {
                                    return true
                                }
                                else
                                {
                                    UtilityClass.showAlert(str: "Please provide linkedIn profile url.")
                                    return false
                                }
                            }
                            else
                            {
                                UtilityClass.showAlert(str: "Please provide home address.")
                                return false
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "Please provide phone number.")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please provide skills.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please provide passion.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please provide education.")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please provide personal summary.")
            return false
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
