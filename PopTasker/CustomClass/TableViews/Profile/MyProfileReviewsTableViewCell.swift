//
//  MyProfileReviewsTableViewCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class MyProfileReviewsTableViewCell: UITableViewCell, RateViewDelegate {
    
    @IBOutlet var rateView: RateView!
    @IBOutlet var lblComment : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var imgView : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        self.rateView.fullSelectedImage = #imageLiteral(resourceName: "RateFill")
        self.rateView.halfSelectedImage = self.rateView.fullSelectedImage
        self.rateView.notSelectedImage = #imageLiteral(resourceName: "RateBlank")
        self.rateView.editable = false
        self.rateView.maxRating = 5
        self.rateView.leftMargin = 0
        self.rateView.midMargin = 1
//        self.rateView.rating = 3
        
        self.imgView.cornerRadius = self.imgView.frame.width/2
        self.imgView.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        self.imgView.layer.borderWidth = 2.0
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

    }
    
    func rateView(_ rateView: RateView!, ratingDidChange rating: Float)
    {
        
    }
}
