//
//  ApplicantProfileHeaderCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantProfileHeaderCell: UITableViewCell
{
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblSummary : UILabel!
    @IBOutlet var lblEducation : UILabel!
    @IBOutlet var lblPassion : UILabel!
    @IBOutlet var lblSkill : UILabel!
    @IBOutlet var lblJobCompleted : UILabel!
    @IBOutlet var lblEndorsmentCompleted : UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        self.imgView.cornerRadius = self.imgView.frame.width/2
        self.imgView.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        self.imgView.layer.borderWidth = 2.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
