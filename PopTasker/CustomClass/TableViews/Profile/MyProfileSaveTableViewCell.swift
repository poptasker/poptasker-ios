//
//  MyProfileSaveTableViewCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class MyProfileSaveTableViewCell: UITableViewCell {
    @IBOutlet var btnSave : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
