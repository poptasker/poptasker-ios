//
//  MyProfileHeaderTableViewCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class MyProfileHeaderTableViewCell: UITableViewCell,UITextFieldDelegate
{
    @IBOutlet var txtSummary: TextFieldProfileTitle!
    @IBOutlet var txtEducation: TextFieldProfileTitle!
    @IBOutlet var txtPssions: TextFieldProfileTitle!
    @IBOutlet var txtSkills: TextFieldProfileTitle!
    @IBOutlet var txtPhoneNumber: TextFieldProfileTitle!
    @IBOutlet var txtHomeAddress: TextFieldProfileTitle!
    @IBOutlet var txtLinkedin: TextFieldProfileTitle!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    var blockTableViewDidSelect:((Int,String)->Void)?
//    var blockTableViewDidSelectImage:((UIImage)->Void)?
    var blockTableViewSelectImage:((String)->Void)?

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.imgView.cornerRadius = self.imgView.frame.width/2
        self.imgView.setBorder(width: 2.0)
        self.imgView.layer.borderColor = UIColor.setPurpalColor().cgColor
        self.selectionStyle = .none
        txtSummary.setLeftTitle(title: "Personal Summary")
        txtEducation.setLeftTitle(title: "Education")
        txtPssions.setLeftTitle(title: "Passions")
        txtSkills.setLeftTitle(title: "Skills")
        txtPhoneNumber.setLeftTitle(title: "Phone Number")
        txtHomeAddress.setLeftTitle(title: "Home Address")
        txtLinkedin.setLeftTitle(title: "Linkedin")
        self.selectionStyle = .none
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(self.blockTableViewDidSelect != nil){
            self.blockTableViewDidSelect!(textField.tag, textField.text!)
        }
    }
    @IBAction func btnSelectImage(sender:UIButton)
    {
        if(self.blockTableViewSelectImage != nil){
            self.blockTableViewSelectImage!("")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
