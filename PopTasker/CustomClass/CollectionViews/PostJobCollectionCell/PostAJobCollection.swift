//
//  PostAJobCollection.swift
//  PopTasker
//
//  Created by Devendra on 17/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation


class PostAJobCollection: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var postAJobList = [JSONDICTIONARY]()
    var blockCollectionViewDidSelectAtIndexPath:((IndexPath,String)->Void)?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
        
        self.register(UINib(nibName: "PostAJobCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PostAJobCollectionViewCell")
        
        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.bounces = false
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func createUserArray(arr:[JSONDICTIONARY])
    {
        postAJobList = arr
        self.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postAJobList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 160, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostAJobCollectionViewCell", for: indexPath as IndexPath) as! PostAJobCollectionViewCell
        let strImgName = "\(self.postAJobList[indexPath.row]["image"]!)"
        cell.imgView.sd_setImage(with: URL.init(string: strImgName), placeholderImage:
            UIImage.init(named: "job_placeholder"), options: [], completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("You selected cell #\(postAJobList[indexPath.item])!")
        if(self.blockCollectionViewDidSelectAtIndexPath != nil)
        {
            self.blockCollectionViewDidSelectAtIndexPath!(indexPath, "")
        }
    }
    
}
