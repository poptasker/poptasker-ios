//
//  SearchDetailColViewCell.swift
//  PopTasker
//
//  Created by Admin on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

class SearchDetailColViewCell: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    var SearchDetailColListArray = ["Date","Wage","Distance"]
    var blockCollectionViewDidSelectAtIndexPath:((IndexPath,String,String)->Void)?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        // REUSE CELL
       
        self.register(UINib(nibName: "SearchDetailFilterColViewCell", bundle: nil), forCellWithReuseIdentifier: "SearchDetailFilterColViewCell")

        self.isScrollEnabled = true
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.bounces = false
        self.allowsSelection = true
        self.dataSource = self
        self.delegate = self
        self.reloadData()
        // self.setTableFooter()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return SearchDetailColListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let str = self.SearchDetailColListArray[indexPath.row]
        let width = str.width(withConstrainedHeight: 40, font: Font.setFont(name: "HelveticaNeueLTPro-Lt", size: 16)!)
        return CGSize.init(width: width + 40, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchDetailFilterColViewCell", for: indexPath as IndexPath) as! SearchDetailFilterColViewCell
        cell.isSelected = false
        cell.lblName.text = self.SearchDetailColListArray[indexPath.item]
        cell.imgView.isHidden = true

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! SearchDetailFilterColViewCell
        cell.imgView.isHidden = true
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("You selected cell #\(SearchDetailColListArray[indexPath.item])!")
        let cell = collectionView.cellForItem(at: indexPath) as! SearchDetailFilterColViewCell
        var order = ""
        cell.imgView.isHidden = false
        if cell.isDes
        {
            order = "dec"
            cell.isDes = false
            cell.imgView.image = UIImage.init(named: "upline")
        }
        else
        {
            order = "asc"
            cell.isDes = true
            cell.imgView.image = UIImage.init(named: "downline")
        }
        if(self.blockCollectionViewDidSelectAtIndexPath != nil)
        {
            self.blockCollectionViewDidSelectAtIndexPath!(indexPath, "\(SearchDetailColListArray[indexPath.item])", order)
        }
    }
}
