//
//  ApplicantListCollectionCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantListCollectionCell: UICollectionViewCell {
    @IBOutlet var lblName : LableNeueLightGray119!
    var isDes : Bool! = true
    @IBOutlet var imgView : UIImageView!
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                super.isSelected = true
                self.lblName.textColor = UIColor.setBlack_0_2_48()
            }
            else
            {
                super.isSelected = false
                self.lblName.textColor = UIColor.setgray_119_119_119()
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.isHidden = true
    }

}
