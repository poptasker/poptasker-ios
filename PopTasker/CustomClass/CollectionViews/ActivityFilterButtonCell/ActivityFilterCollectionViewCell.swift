//
//  ActivityFilterCollectionViewCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ActivityFilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lblName : LableNeueLightGray119!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                super.isSelected = true
                self.lblName.textColor = UIColor.setBlack_0_2_48()
            }
            else
            {
                super.isSelected = false
                self.lblName.textColor = UIColor.setgray_119_119_119()
            }
        }
    } 
}
