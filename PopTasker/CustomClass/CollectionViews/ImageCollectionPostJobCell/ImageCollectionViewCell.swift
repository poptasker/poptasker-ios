//
//  ImageCollectionViewCell.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgView : UIImageView!
    @IBOutlet var btnClose : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
