//
//  AppDelegate.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import UserNotifications
import Reachability
import FacebookLogin
import FacebookCore
import GoogleSignIn
import GoogleMaps
import GooglePlaces


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    //    var SWRevalViewC : SWRevealViewController!
    var tabbarVC : UITabBarController!
    var sideMenuviewObj : MenuViewController!
    let reachability = Reachability()!
    var locationManager = CLLocationManager()
    var isChatDetailsScreen:Bool = false
    var senderId:String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        //        Fabric.with([Crashlytics.self])
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        configureForPushNotification(application: application)
        IQKeyboardManager.shared().disabledToolbarClasses.addObjects(from: [messageDetailVC.self])
        IQKeyboardManager.shared().disabledTouchResignedClasses.add(messageDetailVC.self)
        IQKeyboardManager.shared().disabledDistanceHandlingClasses.add(messageDetailVC.self)
        
        SideMenuController.preferences.basic.menuWidth = 283
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = false
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        //        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = kGoogleClientID
        GMSServices.provideAPIKey(kGoogleAPIKey)
        GMSPlacesClient.provideAPIKey(kGoogleAPIKey)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        setRootView()
        application.applicationIconBadgeNumber = 0
        return FacebookCore.SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func setRootView()
    {
        if UserDefaults.standard.object(forKey: "isLogin") == nil
        {
            setUserDefaultsFor(object: false as AnyObject, with: "isLogin")
        }
        
        let isLogin = getUserDefaultsForKey(key: "isLogin") as! Bool
        
        if isLogin
        {
            sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
            tabbarVC = (STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController)
            
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = SideMenuController(contentViewController: tabbarVC,menuViewController: sideMenuviewObj)
            
            window?.makeKeyAndVisible()
        }
        else
        {
            let LoginVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = UINavigationController.init(rootViewController: LoginVC)
            nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 24))!, NSAttributedString.Key.foregroundColor: UIColor.white]
            nav.navigationBar.shadowImage = UIImage()
            nav.navigationBar.setBackgroundImage(GradientImage.gradientTheamBackgroundFrame(view: nav.navigationBar), for: .default)
            
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = nav
            
            window?.makeKeyAndVisible()
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification)
    {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none
        {
            if reachability.connection == .wifi
            {
                print("Reachable via WiFi")
            }
            else
            {
                print("Reachable via Cellular")
            }
        }
        else
        {
            print("Network not reachable")
        }
    }
    func showHUD()
    {
        hideHUD()
        MBProgressHUD.showAdded(to: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
    }
    
    func hideHUD()
    {
        MBProgressHUD.hide(for: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return FacebookCore.SDKApplicationDelegate.shared.application(app, open: url, options: options) || GIDSignIn.sharedInstance().handle(url as URL?,sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }
    
    private func application(application: UIApplication,openURL url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        var options: [String: AnyObject] = [UIApplication.OpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,UIApplication.OpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        return GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - Push Notification code -
    //this will show popup @ the start of app for sending notifiations
    func configureForPushNotification(application : UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UtilityClass.showAlert(str: "\(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping ( _ options:   UNNotificationPresentationOptions) -> Void){
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        if let userId = dataDict["user_id"]
        {
            var userInfo = notification.request.content.userInfo as? JSONDictionary
            print(userInfo!)
            if userInfo?["gcm.notification.senderId"] as? String == "user_\(userId)"{
                return
            }
            if let conversationId = userInfo?["gcm.notification.id"] as? String{
                print(conversationId)
                if isChatDetailsScreen{
                    if let friendId = userInfo?["gcm.notification.senderId"] as? String{
                        if self.senderId == friendId{
                            return
                        }
                    }
                }
            }
            completionHandler([.alert,.sound])
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        let userInfo = response.notification.request.content.userInfo as? JSONDictionary
        print(userInfo!)
        let data = response.notification.request.content.userInfo
        if let details = data[AnyHashable("details")] as? JSONDICTIONARY {
            let taskId = details["task_id"] as! Int
            let UserId = details["user_id"] as! Int
            let status = details["type"] as! String
            navigateTo(isNavigate: true, isChatNotification: false, taskId: taskId, userId: UserId, status: status)
            return
        }
        if let ConversationId = userInfo?["gcm.notification.id"] as? String{
            print(ConversationId)
            if let senderId = userInfo?["gcm.notification.senderId"] as? String{
                print(senderId)
                navigateTo(isNavigate: true, isChatNotification: true, conversationId:ConversationId, senderId:senderId)
            }
        }
    }
    
    @objc func tokenRefreshNotification(){
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.sendTokenToServer(currentToken: result.token)
                self.connectToFcm()
            }
        }
    }
    
    func sendTokenToServer(currentToken: String)
    {
        print("FCM Token: \(currentToken)")
        UserDefaults.standard.set(currentToken, forKey: "fcmToken")
        UserDefaults.standard.synchronize()
        // Send token to server ONLY IF NECESSARY
    }
    
    func connectToFcm(){
        Messaging.messaging().connect { (error) in
            
            if (error != nil){
                print("Unable to connect with FCM. \(String(describing: error?.localizedDescription))")
                //                    self.tokenRefreshNotification()
            }
            else{
                print("Connected to FCM.")
                if let token = Messaging.messaging().fcmToken
                {
                    print("Firebase registration token: \(token)")
                    UserDefaults.standard.set(token, forKey: "fcmToken")
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    
    func navigateTo(isNavigate : Bool = false, isChatNotification:Bool = false, conversationId:String = "", senderId:String = "", taskId:Int = 0, userId:Int = 0, title:String = "", status:String = "") {
        
        let rootVC = self.window?.rootViewController
        var navVc : UINavigationController?
        print(rootVC!)
        if rootVC!.isKind(of: SideMenuController.self){
            navVc =  (rootVC as! SideMenuController).contentViewController as? UINavigationController
        }else if rootVC!.isKind(of: UINavigationController.self){
            navVc = rootVC as? UINavigationController
        }
        if isChatNotification{
            
            if navVc != nil && navVc!.isKind(of: UINavigationController.self) && navVc!.topViewController!.isKind(of: messageDetailVC.self){
                let vc = navVc!.topViewController! as! messageDetailVC
                if vc.conversationObj.conversationID == conversationId{
                    return
                }else{
                   navVc?.popViewController(animated: true)
                   return
                }
            }
            self.tabbarVC.selectedIndex = 3
        }else{
            RedirectToView(Status:status ,taskId:taskId, userId:userId, title:title, navVc:navVc!)
        }
    }
    
    func RedirectToView(Status:String = "",taskId:Int = 0, userId:Int = 0, title:String = "", status:String = "",navVc:UINavigationController){
        
        if Status == NotificatonKey.NOTI_NAME_AWARD_TASK || Status == NotificatonKey.NOTI_NAME_TASK_COMPLETED || Status == NotificatonKey.NOTI_NAME_TASK_CANCELED{
            self.tabbarVC.selectedIndex = 1
            let JobDetails = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
            JobDetails.taskId = taskId
            JobDetails.JobTitle = title
            JobDetails.strComeFrom = "frompushnotification"
            
            switch status{
            case NotificatonKey.NOTI_NAME_AWARD_TASK :
                JobDetails.needApply = false
                break
            case NotificatonKey.NOTI_NAME_TASK_COMPLETED :
                JobDetails.status = "Completed"
                break
            case NotificatonKey.NOTI_NAME_TASK_CANCELED :
                JobDetails.status = "Cancelled"
                break
            default:
                break
            }
            navVc.pushViewController(JobDetails, animated: true)
        }else{
            let applicantsListVC = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantsListVC") as! ApplicantsListVC
            applicantsListVC.taskId = String(describing: taskId)
            navVc.navigationController?.pushViewController(applicantsListVC, animated: true)
        }
    }
    
}
extension AppDelegate : MessagingDelegate{
    // Receive data message on iOS 10 devices while app is in the foreground.
    
    //    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    //        print("Firebase registration token: \(fcmToken)")
    //        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
    //        UserDefaults.standard.synchronize()
    //    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        UserDefaults.standard.synchronize()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print(remoteMessage.appData)
        
        if let userInfo = remoteMessage.appData as? [String: Any]
        {
            print(userInfo)
        }
    }
}
