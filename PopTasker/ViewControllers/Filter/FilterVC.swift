//
//  FilterVC.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class FilterVC: BaseVC
{
    @IBOutlet var filterTable: FilterTableview!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        AppDel.tabbarVC.tabBar.isHidden = true
        self.navigationItem.title = "Filters"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        let menu = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 44, height: 44))
        menu.setTitle("Reset", for: .normal)
        
        let barButtonItem = UIBarButtonItem.init(customView: menu)
       self.navigationItem.rightBarButtonItem = barButtonItem
            menu.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: -10)
        menu.addTarget(self, action: #selector(self.buttonResetPress), for: .touchUpInside)
        self.filterTable.blockTableViewSetFilter = { (param) -> Void in
            AppDel.showHUD()
            ApiManager.shared.postDataWithHeader(url: kSaveSearch, params: param, completion: { (dict) in
                AppDel.hideHUD()
                print(dict)
                let data = dict["data"] as! JSONDICTIONARY
                let arrListing = data["listing"] as! [JSONDICTIONARY]
                self.navigationController?.popViewController(animated: true)

                if arrListing.count == 0
                {
                    UtilityClass.showToast(view: (AppDel.window?.currentViewController?.view)!, message: "No jobs found.")
                }
            })
        }
        getKeywords()
    }
    @objc func buttonResetPress()
    {
        self.filterTable.setFilters(dict: JSONDICTIONARY())
    }
    func getKeywords()
    {
        AppDel.showHUD()
        ApiManager.shared.getData(url: kKeywords) { (dict) in
            print(dict)
            let arrTag = dict["data"] as! [JSONDICTIONARY]
            self.filterTable.createUserArray(arrTags: arrTag)
            self.getFilter()

        }
    }
    
    func getFilter()
    {
        ApiManager.shared.getData(url: kGetFilters) { (dict) in
            AppDel.hideHUD()
            print(dict)
            if let data = dict["data"] as? JSONDICTIONARY
            {
                 self.filterTable.setFilters(dict: data)
            }
            else
            {
                self.filterTable.setFilters(dict: JSONDICTIONARY())
            }
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
