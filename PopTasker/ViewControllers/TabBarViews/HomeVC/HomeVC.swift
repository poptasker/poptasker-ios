//
//  HomeVC.swift
//  PopTasker
//
//  Created by Rohan on 29/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import CoreLocation
class HomeVC: BaseVC,CLLocationManagerDelegate {
    
    var imgBackgroud: UIImageView {
        let imageView = UIImageView.init(frame: self.view.bounds)
        imageView.image = #imageLiteral(resourceName: "HomeBg")
        return imageView
    }

    override func viewDidLoad()
    {
        self.setNavigationMenu(menuButtons: .Menu, type: .Menu)
        super.viewDidLoad()
//        self.setBotomAndTopBar(hide: true)
        self.navigationItem.title = ""
        self.view.insertSubview(imgBackgroud, at: 0)
        getNoReview()

        print(getModelDataFromUserDefaults(key: "userData"))
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.setBotomAndTopBar(hide: false)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        AppDel.tabbarVC.tabBar.isHidden = false
        
        AppDel.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        AppDel.locationManager.requestWhenInUseAuthorization()
        AppDel.locationManager.delegate = self;
        AppDel.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        AppDel.locationManager.distanceFilter = 10
        AppDel.locationManager.allowsBackgroundLocationUpdates = true
        AppDel.locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                
                break
                
            case .notDetermined,.restricted,.denied:
                
                break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        AppDel.locationManager = manager
       
        //        self.locationManager.stopUpdatingLocation()
    }

    

    func getNoReview()
    {
        AppDel.showHUD()
        ApiManager.shared.getData(url: kNotreview, completion: { (dict) in
            print(dict)
            AppDel.hideHUD()
            if let data = dict["data"] as? JSONDICTIONARY
            {
                let reviewObj = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
                reviewObj.dictData = data
                reviewObj.isFromHome = true
                self.pushViewController(viewcontroller: reviewObj)
            }
        })
    }
    @IBAction func btnPostataskPressed(_ sender: Any) {
        let PostAJobVC = POSTTASK_STORYBOARD.instantiateViewController(withIdentifier: "PostAJobVC") as! PostAJobVC
        self.pushViewController(viewcontroller: PostAJobVC)
    }
    @IBAction func btnFindaTaskPressd(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
        
    }
    func setBotomAndTopBar(hide: Bool)
    {
        self.navigationController!.navigationBar.setBackgroundImage(hide == true ? UIImage() : GradientImage
            
            .gradientTheamBackgroundFrame(view: self.navigationController!.navigationBar), for: .default)
        AppDel.tabbarVC.tabBar.backgroundImage = hide == true ? UIImage() : GradientImage.gradientTheamBackgroundFrame(view: AppDel.tabbarVC.tabBar)
    }
   
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
