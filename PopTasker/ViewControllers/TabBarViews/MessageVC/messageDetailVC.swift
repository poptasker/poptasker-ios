//
//  messageDetailVC.swift
//  PopTasker
//
//  Created by Admin on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class messageDetailVC: BaseChatViewController, searchTypeDelegate {
    
    @IBOutlet var chatBottomConst: NSLayoutConstraint!
    @IBOutlet var messageDetailtblView: MessageDetail!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblJobTitle: LabelNeueLight!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var txtView: TextView!
    @IBOutlet weak var viewEnterText: UIView!
    @IBOutlet weak var txtViewHeight: NSLayoutConstraint!
    
    
    
    var textViewPlaceholder = ""
    var valueChangeRef : DatabaseReference?
    var conversationObj = Conversation()
    var messages = [Message]()
    var sectionData = [String:[Message]]()
    var sortedKeys = [String]()
    var tableReloadTimer: Timer?
    var txtTemp : UITextView?
    let minChatBGViewWidth : CGFloat = 75.0
    let maxChatBGViewWidth = UIScreen.main.bounds.size.width - 56.5
    var chathistoryTime = "true"
    var ChatListDelegate : MessageVC?
    
    var Jobtitle:String!
    var address:String!
    var picture:String!
    var fromId:Int!
    var toId:Int!
    var conversationId:String = ""
    var index:Int!
    var JobId:Int!
    var timeStamp:String!
    var senderId:String!
    
    var arrFilter = [String:[Message]]()
    var arrTmp = [String:[Message]]()
    var tmpSortedKeys = [String]()
    
    var imgview: UIImageView {
        let imageView = UIImageView.init(frame: self.view.bounds)
        imageView.alpha = 0.4
        imageView.image = #imageLiteral(resourceName: "chat details")
        return imageView
    }
    override func viewDidLoad() {
        isMessageDetailsScreen = true
        super.viewDidLoad()
        AppDel.isChatDetailsScreen = true
        self.navigationItem.title = "Chat Details"
        self.setNavigationMenu(menuButtons: .Back, type: .BackAndSearch)
        self.setSearchType(type: .Message)
        self.delegate = self
        txtView.delegate = self
        self.viewEnterText.setCornerRadius()
        viewEnterText.setBorder(width: 1.5)
        imgViewProfile.setCornerRadius()
        imgViewProfile.setBorder(width: 2.0)
        self.imgViewProfile.setCornerRadius()
        self.imgViewProfile.contentMode = .scaleAspectFill
        
        self.conversationObj.conversationID = self.conversationId
        self.conversationObj.lastMessage.timestamp = timeStamp
        lblJobTitle.text = Jobtitle
        lblAddress.text = address
        imgViewProfile.sd_setImage(with: URL.init(string: "\(picture!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo") , options: .highPriority, completed: nil)
        if timeStamp.count > 0{

            let toDate = Date()
            let dateNow = toDate.addingTimeInterval(330.0 * 60.0)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
            let date = dateFormatter.date(from: UtilityClass.getDateFromTimeStamp(timeStamp: conversationObj.lastMessage.timestamp))
            lblDate.text = dateNow.offset(from: date!)
        }else{
            lblDate.isHidden = true
        }
        
        AppDel.senderId = senderId
        txtView.text = textViewPlaceholder
        if txtView.text.trim() == textViewPlaceholder{
            btnSendMessage.isEnabled = false
        }
        displayUserDetail(userID: senderId)
        initializeMessageRef()
        fetchMessages()
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardTap))
        tapGest.cancelsTouchesInView = false
        self.messageDetailtblView.addGestureRecognizer(tapGest)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow(_:)),name: UIResponder.keyboardWillShowNotification,object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillHide(_:)),name: UIResponder.keyboardWillHideNotification,object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AppDel.isChatDetailsScreen = true
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        isMessageDetailsScreen = true
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared().isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isMessageDetailsScreen = false
        AppDel.isChatDetailsScreen = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Functions -
    
    @objc func hideKeyboardTap()
    {
        self.view.endEditing(true)
    }
    
    func initializeMessageRef()
    {
        messagesRef = Database.database().reference().child("messages")
        messagesRef.keepSynced(true)
    }
    
    func getAssignItems(){
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        self.messageDetailtblView.sectionDataMessage = sectionData
        self.messageDetailtblView.sortedKeysMessage = sortedKeys
        self.messageDetailtblView.UserId = "user_\(dataDict["user_id"]!)"
    }
    
    //Fetched all messages from messages table of perticular conversation Ex : (A - B chat)
    func fetchMessages(){
            let dbRef = messagesRef.child(self.conversationObj.conversationID).queryOrdered(byChild: "time")//.queryStarting(atValue: chatHistoryTime)
            var tObj = [Message]()
            dbRef.observe(DataEventType.childAdded, with: { (snapshot) in
                
                //if the reference have some values
                if snapshot.childrenCount > 0
                {
                    let obj = Message()
                    obj.update(data: snapshot.value as! Dictionary<String,Any>)
                    obj.id = snapshot.key
                    
                    let dateStr = UtilityClass.getFormattedDateTime(secondss: Double(obj.timestamp)!, isChatDetailScreen: true)
                    
                    if self.sectionData[dateStr] == nil
                    {
                        self.sortedKeys.append(dateStr)
                        tObj = [Message]()
                    }
                    tObj.append(obj)
                    self.sectionData[dateStr] = tObj
                    self.messages.append(obj)
                }
                
                if self.tableReloadTimer != nil && self.tableReloadTimer?.isValid == true
                {
                    self.tableReloadTimer?.invalidate()
                }
                if self.isMessageDetailsScreen{
                self.tableReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableAndScrollToBottom), userInfo: nil, repeats: false)
                }
                if self.valueChangeRef == nil{
                    self.addObserverForValueChange()
                }
            })
    }
    
    func addObserverForValueChange(){
        
        self.valueChangeRef = Database.database().reference().child("messages").child(self.conversationObj.conversationID)
        let dbRef = self.valueChangeRef?.queryOrdered(byChild: "time")//.queryStarting(atValue: chatHistoryTime)
        
        dbRef?.observe(.childChanged, with: { (snapshot) in
            let dict = snapshot.value as! Dictionary<String,Any>
            let obj = Message()
            obj.update(data: dict)
            obj.id = snapshot.key
            
            if let index = self.messages.index(where: {$0.id == obj.id}) {
                self.messages.remove(at: index)
                self.messages.insert(obj, at: index)
            }
            self.messageDetailtblView.reloadData()
        })
    }
    
    func addMessageToDB(isMedia : Bool,messageID : String)
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        if let userId = dataDict["user_id"]
        {
            let key = messageID.count > 0 ? messageID : self.messagesRef!.childByAutoId().key!
            
            let msgObj = Message()
            msgObj.messageText = self.txtView.text.trim()
            msgObj.senderName = "\(dataDict["firstname"]!)"
            msgObj.timestamp =  Int64(Date().timeIntervalSince1970 * 1000).description
            
            let msgDict = NSMutableDictionary()
            msgDict.setValue(msgObj.messageText, forKey: "message")
            msgDict.setValue(msgObj.senderName, forKey: "senderName")
            if let userProfile = getModelDataFromUserDefaults(key: "userProfile") as? JSONDICTIONARY
            {
                if let pic = userProfile["profilepic"] as? String
                {
                    msgDict.setValue(pic, forKey: "senderProfile")
                }else{
                    msgDict.setValue("", forKey: "senderProfile")
                }
            }else{
                msgDict.setValue("", forKey: "senderProfile")
            }
            
            msgDict.setValue(msgObj.timestamp, forKey: "time")
            msgDict.setValue("text", forKey: "type")
            msgDict.setValue("user_\(userId)", forKey: "user")
            
            messagesRef.child(self.conversationObj.conversationID).child(key).setValue(msgDict)
            
            let dbConvRef = Database.database().reference()
            let dbConversationRef = dbConvRef.child("conversations").child(self.conversationObj.conversationID)
            
            let conversationDict = NSMutableDictionary()
            conversationDict.setValue(conversationObj.conversationID, forKey: "conversationID")
            conversationDict.setValue(msgObj.messageText, forKey: "text")
            conversationDict.setValue("text", forKey: "type")
            conversationDict.setValue(msgObj.timestamp, forKey: "timestamp")
            conversationDict.setValue(key, forKey: "msgID")
            conversationDict.setValue(msgObj.senderName, forKey: "senderName")
            conversationDict.setValue("user_\(userId)", forKey: "senderId")
            dbConversationRef.child("last_message").setValue(conversationDict)
            
            if timeStamp.count > 0{
                dbConversationRef.child("unread").queryOrderedByKey().observeSingleEvent(of: .value, with: {snapshot in
                    guard let dict = snapshot.value as? [String:Any] else{
                        print("Error")
                        return
                    }
                    let ownUnReadCount = dict["user_\(String(describing: userId))"]! as! String
                    let count = dict[self.senderId!]! as! String
                    var UnReadcount = Int(count)
                    UnReadcount = UnReadcount!+1
                    let UnReadCountDict = NSMutableDictionary()
                    UnReadCountDict.setValue(self.conversationObj.conversationID, forKey: "conversationID")
                    UnReadCountDict.setValue(String(UnReadcount!), forKey: self.senderId!)
                    UnReadCountDict.setValue(ownUnReadCount != "0" ? ownUnReadCount : "0", forKey: "user_\(String(describing: userId))")
                    print(UnReadCountDict)
                    dbConversationRef.child("unread").setValue(UnReadCountDict)
                })
            }else{
                let UnReadCountDict = NSMutableDictionary()
                UnReadCountDict.setValue(self.conversationObj.conversationID, forKey: "conversationID")
                UnReadCountDict.setValue("1", forKey: self.senderId!)
                UnReadCountDict.setValue("0", forKey: "user_\(String(describing: userId))")
                print(UnReadCountDict)
                dbConversationRef.child("unread").setValue(UnReadCountDict)
            }
            
            DispatchQueue.main.async {
                if self.chatBottomConst.constant == 0{
                    self.txtView.text = self.textViewPlaceholder
                }else{
                    self.txtView.text = ""
                }
                UIView.animate(withDuration: 0.5, animations:{
                    self.txtViewHeight.constant = 36
                    self.viewEnterText.setCornerRadius()
                })
                self.btnSendMessage.isEnabled = false
            }
        }
        self.tableReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableAndScrollToBottom), userInfo: nil, repeats: false)
    }
    
    @objc func reloadTableAndScrollToBottom()
    {
        self.getAssignItems()
        self.messageDetailtblView.reloadData()
        self.scrollToBottom()
    }
    
    func scrollToBottom()
    {
        let lastSection = self.sectionData.keys.count - 1
        
        if lastSection >= 0
        {
            let tdata =  self.sectionData[self.sortedKeys[lastSection]]
            
            if tdata!.count > 0
            {
                self.messageDetailtblView.scrollToRow(at: IndexPath(row: tdata!.count - 1, section: lastSection), at: .bottom, animated: true)
                 if let del = self.ChatListDelegate{
//                    if index != nil{
                     del.setMessageAsRead(index: index!)
//                    }
                }
            }
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification){
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.chatBottomConst.constant = -(keyboardHeight)
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification){
        self.chatBottomConst.constant = 0
        self.view.layoutIfNeeded()
    }
    
    //MARK:- search delegate
    func search(text: String, type: searchType) {
        if type == .Message
        {
            self.arrTmp = self.sectionData
            self.arrFilter = self.sectionData
            self.tmpSortedKeys = sortedKeys
            self.sortedKeys.removeAll()
            if text != ""{
                let filter = self.arrFilter.filter { (dict) -> Bool in
                    
                    let key = dict.key
                    let filter1 = dict.value.filter{ (dict1) -> Bool in
                        if let name = dict1.messageText as? String {
                           return name.lowercased().range(of:text.lowercased()) != nil
                        }
                        return false
                    }
                    self.arrFilter[key] = filter1
                    return false
                }
            }
            var localArr = [String:[Message]]()
            for(key,value) in arrFilter{
                if value.count > 0 {
                    localArr[key] = value
                    self.sortedKeys.append(key)
                }
            }
            self.sectionData = localArr
            getAssignItems()
            self.messageDetailtblView.reloadData()
        }
    }
    func close(text: String, type: searchType) {
        self.arrFilter = self.arrTmp
        self.sortedKeys = tmpSortedKeys
        self.sectionData = self.arrFilter
        self.tableReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableAndScrollToBottom), userInfo: nil, repeats: false)
    }
    
    func displayUserDetail(userID : String?){
        Chatdelegate?.getUserNameForUserID(userID, completion: { (userObj) in
         self.messageDetailtblView.senderProfile = userObj.imageURL
        })
    }
    
    //MARK: - IBAction -
    
    @IBAction func btnSendMessagePressed(_ sender: UIButton) {
        
        if txtView.text!.trim() != textViewPlaceholder && txtView.text!.trim() != ""{
            self.addMessageToDB(isMedia: false, messageID: "")
        }else{
            UtilityClass.showAlert(str: VALIDATION_BLANK_TEXT_CHAT)
        }
    }
    
    override func buttonBackPress(){
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            AppDel.tabbarVC.tabBar.isHidden = false
            nvc.popViewController(animated: true)
        }
    }
    @IBAction func btnJobDetailPressed(_ sender: UIButton) {
        
        let PostAJobVC = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
        PostAJobVC.taskId = JobId
        PostAJobVC.JobTitle = Jobtitle
        PostAJobVC.strComeFrom = "messagedetailvc"
        PostAJobVC.status = "Accepted"
        self.pushViewController(viewcontroller: PostAJobVC)
    }
}
extension messageDetailVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView.text.count > 0{
            self.btnSendMessage.isEnabled = true
        }else{
            self.btnSendMessage.isEnabled = false
        }
        
        if (textView.contentSize.height < (UIScreen.main.bounds.size.height * 0.20))
        {
            UIView.animate(withDuration: 0.0, animations:{
                self.txtViewHeight.constant = textView.contentSize.height
                textView.textContainerInset = UIEdgeInsets(top: 8,left: 8,bottom: 8,right: 8)
            })
        }
        
        if textView.contentSize.height < 27
        {
            self.txtViewHeight.constant = 36
            viewEnterText.setCornerRadius()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView){
        if textView.text.trim() == textViewPlaceholder{
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView){
        if textView.text.trim() == ""
        {
            if self.chatBottomConst.constant == 0{
                textView.text = textViewPlaceholder
                textView.textColor = GREY_COLOR_THEME_TEXT
                viewEnterText.setCornerRadius()
            }
        }
    }
}
