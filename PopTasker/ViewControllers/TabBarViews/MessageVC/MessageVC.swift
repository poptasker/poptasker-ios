//
//  MessageVC.swift
//  PopTasker
//
//  Created by Rohan on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class MessageVC: BaseChatViewController, searchTypeDelegate {
    
    @IBOutlet var tblMessage : MessagesTableView!
    
    var arrFilter = [Conversation]()
    var arrTmp = [Conversation]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        //        if AppDel.reachability.connection != .none{
        //            UtilityClass.showAlert(str: "NO newtwork")
        //        }else{
        //        }
        self.setSearchType(type: .Message)
        self.delegate = self
        initializeMessageRef()
        addValueChangedObserver()
        self.tblMessage.tableFooterView = UIView()
        tblMessage.blockTableViewDidSelectAtIndexPath = { (indexpath, str) -> Void in
            let messageDetail = MESSAGEDETAIL_STORYBOARD.instantiateViewController(withIdentifier: "messageDetailVC") as! messageDetailVC
            let data = self.tblMessage.list[indexpath.row]
            if data.unReadMessageCountDict != nil{
                if ((data.unReadMessageCountDict.value(forKey: "user_\(String(describing: data.toId!))") as? String) != nil && (data.unReadMessageCountDict.value(forKey: "user_\(String(describing: data.toId!))") as? String) != "0"){
                    self.setMessageAsRead(index: indexpath.row)
                }
            }
            if let dict = data.userDictionary{
                for(key,_) in dict{
                    if (key as! String) != "user_\(String(describing: self.tblMessage.KuserId!))"{
                        messageDetail.senderId = (key as! String)
                    }
                }
            }
            messageDetail.index = indexpath.row
            messageDetail.Jobtitle = data.title
            messageDetail.conversationId = data.conversationId
            messageDetail.fromId = data.FromId
            messageDetail.toId = data.toId
            messageDetail.picture = data.ProfilePic
            messageDetail.address = data.address
            messageDetail.JobId = data.taskId
            messageDetail.timeStamp = data.lastMessage.timestamp
            messageDetail.ChatListDelegate = self
            self.pushViewController(viewcontroller: messageDetail)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblMessage.list.removeAll()
        self.tblMessage.conversationList.removeAll()
        self.tblMessage.getMessagesList()
        DispatchQueue.main.asyncAfter(deadline: .now()+2.5, execute: {
            self.fetchConversations()
        })
    }
    
    //MARK:- functions
    
    //Getting list of users
    @objc func fetchConversations(){
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        if let userId = dataDict["user_id"]
        {
            self.conversationRef.queryOrdered(byChild: "users/user_\(userId)").queryEqual(toValue: "true").observe(.childAdded, with: { (snapshot) in
                //if the reference have some values
                if snapshot.childrenCount > 0
                {
                    let dict = snapshot.value as! JSONDICTIONARY
                    self.tblMessage.obj.conversationID = snapshot.key
                    for i in (0..<self.tblMessage.conversationList.count){
                        var convId = self.tblMessage.conversationList[i]
                        if convId["conversation_id"]! as! String == self.tblMessage.obj.conversationID{
                            let obj = Conversation()
                            obj.update(data: dict)
                            obj.updateUserData(data: self.tblMessage.conversationList[i])
                            self.tblMessage.list.append(obj)
                        }
                    }
                    self.tblMessage.sortArray()
                    DispatchQueue.main.async{
                        self.arrTmp = self.tblMessage.list
                        self.tblMessage.reloadData()
                        AppDel.hideHUD()
                    }
                }
            })
        }
    }
    
    func setMessageAsRead(index:Int){
        
        if self.tblMessage.list.count > 0{
          
            let data = self.tblMessage.list[index]
            let dbConvRef = Database.database().reference()
            let dbConversationRef = dbConvRef.child("conversations").child(data.conversationId)
            dbConversationRef.child("unread").queryOrderedByKey().observeSingleEvent(of: .value, with: {snapshot in
                guard let dict = snapshot.value as? [String:Any] else{
                    print("Error")
                    return
                }
                let UnReadCountDict = NSMutableDictionary()
                for(key,_) in dict{
                    if (key) == "user_\(String(describing:self.tblMessage.KuserId!))"{
                        UnReadCountDict.setValue(data.conversationId, forKey: "conversationID")
                        UnReadCountDict.setValue("0", forKey: "user_\(String(describing: self.tblMessage.KuserId!))")
                    }else if (key) != "user_\(String(describing:self.tblMessage.KuserId!))" && (key) != "conversationID"{
                        let ownUnReadCount = dict[key]! as! String
                        UnReadCountDict.setValue(ownUnReadCount != "0" ? ownUnReadCount : "0", forKey: key)
                    }
                }
                dbConversationRef.child("unread").setValue(UnReadCountDict)
            })
        }
        
    }
    
    func initializeMessageRef(){
        var messageRef = Database.database().reference()
        messageRef = messageRef.child("messages")
        messageRef.keepSynced(true)
    }
    
    func addValueChangedObserver(){
        
        conversationRef!.observe(.childChanged, with: { (snapshot) in
            
            if snapshot.childrenCount > 0
            {
                let dict = snapshot.value as! Dictionary<String,Any>
                if let index = self.tblMessage.list.index(where: {$0.conversationId == snapshot.key})
                {
                    let tObj = self.tblMessage.list[index]
                    tObj.isValueChanged = true
                    tObj.update(data: dict)
                    self.tblMessage.sortArray()
                    DispatchQueue.main.async{
                        self.tblMessage.reloadData()
                    }
                }
            }
        })
    }
    
    //MARK:- search delegate
    func search(text: String, type: searchType) {
        if type == .Message
        {
            self.arrFilter = self.arrTmp
            if text != ""
            {
                let filter = self.arrFilter.filter { (dict) -> Bool in
                    if let name = dict.title //dict["title"] as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                arrFilter = filter
            }
            self.tblMessage.list = self.arrFilter
            self.tblMessage.reloadData()
        }
    }
    func close(text: String, type: searchType) {
        self.arrFilter = self.arrTmp
        self.tblMessage.list = self.arrFilter
        self.tblMessage.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
