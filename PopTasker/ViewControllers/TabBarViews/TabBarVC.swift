//
//  TabBarVC.swift
//  PopTasker
//
//  Created by Rohan on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    /*
     color of the indicator
     */
    @IBInspectable var indicatorColor: UIColor = UIColor.setColor(red: 17, green: 24, blue: 66)
    
    /*
     determine if the indicator
     will be drawn on top of bar items or not
     */
    @IBInspectable var onTopIndicator: Bool = false

    var tabBarItemArray: [TabBarItems] {
        return [TabBarItems.init(title: "Home", viewController: "HomeVC", unSelectedImg: #imageLiteral(resourceName: "Tab_Home_UnSelected"), selectedImg: #imageLiteral(resourceName: "Tab_Home_Selected")),
                TabBarItems.init(title: "Activities", viewController: "ActivityVC", unSelectedImg: #imageLiteral(resourceName: "Tab_Activity_UnSelected"), selectedImg: #imageLiteral(resourceName: "Tab_Activity_Selected")),
                TabBarItems.init(title: "Search", viewController: "SearchVC", unSelectedImg: #imageLiteral(resourceName: "Tab_Search_UnSelected"), selectedImg: #imageLiteral(resourceName: "Tab_Search_Selected")),
                TabBarItems.init(title: "Message", viewController: "MessageVC", unSelectedImg: #imageLiteral(resourceName: "Tab_Message_UnSelected"), selectedImg: #imageLiteral(resourceName: "Tab_Message_Selected")),
               ]
    }
    
    //MARK:- View Controller Life Cycle
    override func viewDidLoad() {
        StatusBarStyle.setStatusBar(style: .lightContent)
        super.viewDidLoad()

//        print(self.navigationController?.viewControllers)
//        if (self.navigationController?.viewControllers.count)! > 1
//        {
//            self.navigationController?.viewControllers.removeFirst()
//        }
//        print(self.navigationController?.viewControllers)

        var vcAray = [UIViewController]()
        for tabItem in self.tabBarItemArray {
            let vc = UIStoryboard.init(name: tabItem.title, bundle: nil).instantiateViewController(withIdentifier: tabItem.viewController)
            vc.title = tabItem.title
            let navigationVC = UINavigationController.init(rootViewController: vc)

            vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 24))!, NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationVC.navigationBar.shadowImage = UIImage()
            let tab = UITabBarItem.init(title:tabItem.title, image:tabItem.unSelectedImg.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage:tabItem.selectedImg.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
            let tabFont = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 12.0))!
            let tabAttribute = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : tabFont]
            tab.setTitleTextAttributes(tabAttribute, for: .normal)
            tab.setTitleTextAttributes(tabAttribute, for: .selected)
            tab.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -7)
            tab.imageInsets = UIEdgeInsets.init(top: -3, left: 0, bottom: 3, right: 0)
            navigationVC.navigationBar.setBackgroundImage(GradientImage.gradientTheamBackgroundFrame(view: navigationVC.navigationBar), for: .default)
            navigationVC.tabBarItem = tab
            vcAray.append(navigationVC)
        }
        self.tabBar.tintColor = UIColor.white

        self.viewControllers = vcAray
        // Do any additional setup after loading the view.
      
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        var tabFrame = self.tabBar.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        let tabbarHeight: CGFloat = 49
        tabFrame.size.height = Devices.deviceType == .iPhoneX ? tabbarHeight + 40 : tabbarHeight
        tabFrame.origin.y = self.view.frame.size.height - tabFrame.size.height
        self.tabBar.frame = tabFrame
        
        // Draw Indicator above the tab bar items
        guard let numberOfTabs = tabBar.items?.count else {
            return
        }
        
        let numberOfTabsFloat = CGFloat(numberOfTabs)
        let imageSize = CGSize(width: tabBar.frame.width / numberOfTabsFloat,height: tabBar.frame.height)
        
        
        let indicatorImage = UIImage.drawTabBarIndicator(color: indicatorColor,size: imageSize,onTop: onTopIndicator)
        self.tabBar.selectionIndicatorImage = indicatorImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
