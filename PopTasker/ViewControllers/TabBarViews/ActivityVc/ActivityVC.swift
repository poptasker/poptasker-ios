//
//  ActivityVC.swift
//  PopTasker
//
//  Created by Rohan on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ActivityVC: BaseVC,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,searchTypeDelegate
{
    var arrFilter : [String]!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var activityTblObj : ActivityTableView!
    var arr = [Activity]()
    var arrFilterActivity = [Activity]()
    var isNextPage : Bool! = false
    var page : Int! = 1
    var imgBackgroud: UIImageView {
        let imageView = UIImageView.init(frame: self.view.bounds)
        imageView.image = #imageLiteral(resourceName: "activities_bg")
        return imageView
    }
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setNavigationMenu(menuButtons: .Menu, type: .MenuAndSearch)
        self.setSearchType(type: .Activity)
        self.delegate = self
        self.view.insertSubview(imgBackgroud, at: 0)
        self.collectionView.register(UINib(nibName: "ActivityFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ActivityFilterCollectionViewCell")

        arrFilter = ["All","Posted","Pending","Accepted","Completed","Cancelled","Deleted"]
        
        activityTblObj.blockTableViewDidDeleteAtIndexPath = { (indexpath, dict,status) -> Void in
            print(status)
            let alert = UIAlertController.init(title: kAppName, message: "Are you sure you want to delete job?", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "YES", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kCanceljob, params: ["task_id":"\(dict["task_id"]!)"]) { (dict) in
//                    AppDel.hideHUD()
                    print(dict)
                    self.page = 1
                    let index = self.collectionView.indexPathsForSelectedItems?.first as! IndexPath
                    
                    print("\(self.arrFilter[(index.row)].lowercased())")
                    self.arr = Array()
                    self.arrFilterActivity = Array()
                    self.getActivity(status: "\(self.arrFilter[(index.row)].lowercased())")
                }
            }))
           alert.addAction(UIAlertAction.init(title: "NO", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        activityTblObj.blockTableViewDidSelectAtIndexPath = { (indexpath, dict,status) -> Void in
            print(status)
            let PostAJobVC = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
            PostAJobVC.Datadict = dict
            PostAJobVC.status = status
            self.pushViewController(viewcontroller: PostAJobVC)
            
        }
//        self.activityTblObj.addPullToRefresh(actionHandler: { () -> Void in
//
//        })
        
        self.activityTblObj.addInfiniteScrolling {
            if self.isNextPage
            {
                self.page = self.page + 1
                let index = self.collectionView.indexPathsForSelectedItems?.first as! IndexPath

                print("\(self.arrFilter[(index.row)].lowercased())")
        
                self.getActivity(status: "\(self.arrFilter[(index.row)].lowercased())")
            }
            else
            {
                self.hideInfiniteScroll()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.contentOffset.x = 0
        arr = Array()
        arrFilterActivity = Array()
        getActivity(status: "all")
        collectionView.selectItem(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .left)
    }
//    func hidepullToRefresh()
//    {
//        self.activityTblObj.pullToRefreshView.stopAnimating()
//    }
    func hideInfiniteScroll() {
        self.activityTblObj.infiniteScrollingView.stopAnimating()
    }
    func getActivity(status:String) {
        
        let param = ["status":status,"page":page] as JSONDICTIONARY
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kActivities, params: param) { (dict) in
            print(dict)
            AppDel.hideHUD()
            let data = dict["data"] as! JSONDICTIONARY
            if Int("\(data["has_page"]!)") == 1
            {
                self.isNextPage = true
            }
            else
            
            {
                self.isNextPage = false
            }
            let arrListing = data["listing"] as! Array<JSONDICTIONARY>
            print(arrListing)
            
            for list in arrListing
            {
                let pictures = list["pictures"] as! [JSONDICTIONARY]
                var imgUrl = ""
                if pictures.count != 0
                {
                    imgUrl = "\(pictures[0]["thumb"]!)"
                }
                let type : ActivityCellType!
                
                if "\(list["status"]!)" == "completed"
                {
                    type = ActivityCellType.Completed
                }
                else if "\(list["status"]!)" == "pending"
                {
                    type = ActivityCellType.Pending
                }
                else if "\(list["status"]!)" == "cancelled"
                {
                    type = ActivityCellType.Cancelled
                }
                else if "\(list["status"]!)" == "posted"
                {
                    type = ActivityCellType.Posted
                }
                else if "\(list["status"]!)" == "accepted"
                {
                    type = ActivityCellType.Accepted
                }
                else if "\(list["status"]!)" == "deleted"
                {
                    type = ActivityCellType.Deleted
                }
                else
                {
                    type = ActivityCellType.All
                }
                let activity = Activity.init( imgUrl: imgUrl, coachName: "\(list["title"]!)", name: "\(list["employer"]!)", description: "\(list["address"]!)", developer: "\(list["description"]!)", dollar: "\(list["wage"]!)", time: "\(list["working_hours"]!)", cellType: type, dict: list, filter: status)
                self.arr.append(activity)
            }
            self.hideInfiniteScroll()
            self.activityTblObj.createUserArray(arr: self.arr)
            
            
        }
    }
   
    //MARK:- search delegate
    func search(text: String, type: searchType) {
        if type == .Activity
        {
            self.arrFilterActivity = self.arr

            if text != ""
            {
                let filter = arrFilterActivity.filter { (dict) -> Bool in
                    if let name = dict.coachName as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                arrFilterActivity = filter
                
            }
            self.activityTblObj.createUserArray(arr: self.arrFilterActivity)

        }
    }
    func close(text: String, type: searchType) {
        self.arrFilterActivity = self.arr
        self.activityTblObj.createUserArray(arr: self.arrFilterActivity)
    }
    // MARK: - UICollectionView Methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = self.arrFilter[indexPath.row]
        let width = str.width(withConstrainedHeight: 40, font: Font.setFont(name: "HelveticaNeueLTPro-Lt", size: 16)!)
        return CGSize.init(width: width + 10, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivityFilterCollectionViewCell", for: indexPath as IndexPath) as! ActivityFilterCollectionViewCell
        cell.isSelected = false
        cell.lblName.text = self.arrFilter[indexPath.item]

        if indexPath.row == 0
        {
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right) //Add this line
            cell.isSelected = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print("You selected cell #\(arrFilter[indexPath.item])!")
        arr = Array()
        arrFilterActivity = Array()
        page = 1
        self.closeButtonPress()
//        self.setButton(type: .Search, isLeft: false)
        close(text: "", type: .Activity)
        if indexPath.row == 0
        {
            getActivity(status: "all")
        }
        else if indexPath.row == 1
        {
            getActivity(status: "posted")
        }
        else if indexPath.row == 2
        {
            getActivity(status: "pending")
        }
        else if indexPath.row == 3
        {
            getActivity(status: "accepted")
        }
        else if indexPath.row == 4
        {
            getActivity(status: "completed")
        }
        else if indexPath.row == 5
        {
            getActivity(status: "cancelled")
        }
        else if indexPath.row == 6
        {
            getActivity(status: "deleted")
        }
    }
    
    // MARK: -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
