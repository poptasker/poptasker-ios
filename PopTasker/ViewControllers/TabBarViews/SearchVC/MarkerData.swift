
//
//  MarkerData.swift
//  iOSComponents
//
//  Created by Rohan on 27/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

class MarkerData {
    
    var name: String?
    var address: String?
    var lat: String?
    var long: String?
    
    func populateWithJson(data:[String: Any]) {
        
        if let name = data["name"] as? String {
            self.name = name
        }
        
        if let address = data["address"] as? String {
            self.address = address
        }
        
        if let lat = data["lat"] as? String {
            self.lat = lat
        }
        
        if let long = data["long"] as? String {
            self.long = long
        }
        
    }
    
}
