//
//  SearchDetailVC.swift
//  PopTasker
//
//  Created by Admin on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class SearchDetailVC: BaseVC,searchTypeDelegate {

    var arrFilter : [String]!
    @IBOutlet var collectionView : SearchDetailColViewCell!
    @IBOutlet var activityTblObj : SearchDetailTableview!
    var arrList = [JSONDICTIONARY]()
    var arrFilterList = [JSONDICTIONARY]()
    var page : Int! = 1
    var isNextPage : Bool! = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Search"
        self.setNavigationMenu(menuButtons: .MapSearch, type:.MapSearch)
        self.setSearchType(type: .JobSearchList)
        self.delegate = self
        self.activityTblObj.addInfiniteScrolling {
            if self.isNextPage
            {
                self.page = self.page + 1
                
                self.getNearbyJobs(lat: "\(AppDel.locationManager.location!.coordinate.latitude)", long: "\(AppDel.locationManager.location!.coordinate.longitude)")
            }
            else
            {
                self.hideInfiniteScroll()
            }
        }
        self.activityTblObj.blockTableViewDidSelectAtIndexPath = {(indexpath,data) -> Void in
            let PostAJobVC = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
            PostAJobVC.Datadict = data
            PostAJobVC.status = "\(data["status"]!)"
            PostAJobVC.needApply = true
            self.pushViewController(viewcontroller: PostAJobVC)

        }
        self.collectionView.blockCollectionViewDidSelectAtIndexPath = {(indexpath,sortType,order) -> Void in
            
            if sortType == "Date"
            {
                var array2 = [JSONDICTIONARY]()
               
                if order == "dec"
                {
                    array2 = self.arrList.sorted(by:{let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        return formatter.date(from:$0["start_date"] as! String)! > formatter.date(from:$1["start_date"] as! String)!})
                }
                else
                {
                    array2 = self.arrList.sorted(by:{let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        return formatter.date(from:$0["start_date"] as! String)! < formatter.date(from:$1["start_date"] as! String)!})
                }
               
                self.arrList = array2
                self.arrFilterList = array2
            }
            else if sortType == "Wage"
            {
                var array2 = [JSONDICTIONARY]()
                
                if order == "dec"
                {
                    array2 = self.arrList.sorted{($1["wage"] as? Int ?? 0) < ($0["wage"] as? Int ?? 0)}
                }
                else
                {
                    array2 = self.arrList.sorted{($1["wage"] as? Int ?? 0) > ($0["wage"] as? Int ?? 0)}
                }
                
                self.arrList = array2
                self.arrFilterList = array2
            }
            else if sortType == "Distance"
            {
                var array2 = [JSONDICTIONARY]()
                
                if order == "dec"
                {
                    array2 = self.arrList.sorted{($1["distance"] as? Double ?? 0) > ($0["distance"] as? Double ?? 0)}
                }
                else
                {
                    array2 = self.arrList.sorted{($1["distance"] as? Double ?? 0) < ($0["distance"] as? Double ?? 0)}
                }
                print(array2)
                self.arrList = array2
                self.arrFilterList = array2
            }
            self.setDatatoTable(arrData: self.arrFilterList)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = false
        getNearbyJobs(lat: "\(AppDel.locationManager.location!.coordinate.latitude)", long: "\(AppDel.locationManager.location!.coordinate.longitude)")
        

    }
    func hideInfiniteScroll() {
        self.activityTblObj.infiniteScrollingView.stopAnimating()
    }
    func getNearbyJobs(lat:String,long:String)
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kNearbyJobs, params: ["latitude":lat,"longitude":long,"page":page]) { (dict) in
            print(dict)

            let dictData = dict["data"] as! JSONDICTIONARY
            if Int("\(dictData["has_page"]!)") == 1
            {
                self.isNextPage = true
            }
            else
                
            {
                self.isNextPage = false
            }
            let arrListing = dictData["listing"] as! [JSONDICTIONARY]
            self.arrList = arrListing
            self.arrFilterList = arrListing
            self.setDatatoTable(arrData: self.arrList)
            AppDel.hideHUD()
        }
    }
    
    func setDatatoTable(arrData:[JSONDICTIONARY])
    {
        var arr = [SearchDetail]()
        AppDel.showHUD()
        for dict in arrData
        {
            var img = UIImage()
            let arrPictures = dict["pictures"] as! [JSONDICTIONARY]
            if arrPictures.count > 0
            {
                let dictPic = arrPictures[0] as JSONDICTIONARY
                img = try! UIImage.sd_image(with: Data.init(contentsOf: URL.init(string: "\(dictPic["thumb"]!)")!, options: Data.ReadingOptions.mappedRead))!
            }
            else
            {
                img = UIImage.init(named: "AppLogo")!
            }
            let seardata = SearchDetail.init(img: img, jobName: "\(dict["title"]!)", date: "\(dict["start_date"]!) \(dict["start_time"]!)", city: "\(dict["address"]!))", description: "\(dict["description"]!)", dollar: "\(dict["wage"]!)/Hour", time: "\(dict["working_hours"]!) Hours", isApplied: Int("\(dict["is_applied"]!)")!, cellType: SearchDetailCellType.RankBy)
            arr.append(seardata)
        }
        self.activityTblObj.createUserArray(arr: arr, arrList: arrData)
    }
    //MARK:- search delegate
    func search(text: String, type: searchType) {
        if type == .JobSearchList
        {
            self.arrFilterList = self.arrList
            
            if text != ""
            {
                let filter = arrFilterList.filter { (dict) -> Bool in
                    if let name = dict["title"] as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                print(filter) // ["b": 42]
                arrFilterList = filter
            }
            
            setDatatoTable(arrData: arrFilterList)
            
        }
    }
    func close(text: String, type: searchType) {
        self.arrFilterList = self.arrList
        setDatatoTable(arrData: arrFilterList)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
