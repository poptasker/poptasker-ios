//
//  SearchVC.swift
//  PopTasker
//
//  Created by Rohan on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
class SearchVC: BaseVC,CLLocationManagerDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate,CustomInfoWindowDelegate
{
    @IBOutlet var serchBar : TextFieldGray!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var imgMenu : UIImageView!
    @IBOutlet var mapView : GMSMapView!
    private var infoWindow = CustomInfoWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    var arrMarkers = [JSONDICTIONARY]()
    var arrFilterMarkers = [JSONDICTIONARY]()

    override func viewDidLoad() {
        StatusBarStyle.setStatusBar(style: .default)
        super.viewDidLoad()
        serchBar.layer.cornerRadius = serchBar.frame.height/2
        serchBar.layer.borderColor = UIColor.setGrayLignColor().cgColor
        serchBar.layer.borderWidth = 0.7
        
        mapView.settings.scrollGestures = true
        mapView.delegate = self
        mapView.mapType = .normal
        mapView.isMyLocationEnabled = true
        let mapDragRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        mapDragRecognizer.delegate = self
        mapView.addGestureRecognizer(mapDragRecognizer)
        
        infoWindow.blockMarkerInfoPress = {(dict) -> Void in
            
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        AppDel.tabbarVC.tabBar.isHidden = false
        
        imgMenu.cornerRadius = imgMenu.frame.width/2
        imgMenu.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        imgMenu.layer.borderWidth = 1.0
        
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        let first = "\(dataDict["firstname"]!)".first
        let img = LetterImageGenerator.imageWith(name: "\(first!)")
        imgMenu.image = img
        if let userProfile = getModelDataFromUserDefaults(key: "userProfile") as? JSONDICTIONARY
        {
            if userProfile["profilepic"] as? String != ""
            {
                imgMenu.sd_setImage(with: URL.init(string: "\(userProfile["profilepic"]!)"), completed: nil)
            }
        }
        
        AppDel.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        AppDel.locationManager.requestWhenInUseAuthorization()
        AppDel.locationManager.delegate = self;
        AppDel.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        AppDel.locationManager.distanceFilter = 10
        AppDel.locationManager.allowsBackgroundLocationUpdates = true
        AppDel.locationManager.startUpdatingLocation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
       
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                
                guard let locValue: CLLocationCoordinate2D =  AppDel.locationManager.location?.coordinate else { return }
                print("locations = \(locValue.latitude) \(locValue.longitude)")
                let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 15)
                self.mapView.camera = camera
                self.mapView.clear()
                arrMarkers = Array()
                arrFilterMarkers = Array()
                getNearbyJobs(lat: "\(locValue.latitude)", long: "\(locValue.longitude)")
                break
                
            case .notDetermined,.restricted,.denied:
                
                print("Not determined.")
                let alertController = UIAlertController(title: kAppName, message: "GPS permission allows you to access location data .Please allow it to App settings for aditional functionality", preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl)
                    {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                }
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                break
            }
            
            //            self.locationManager.startUpdatingLocation()
        }
       
    }
    @IBAction func btnMenuClick(sender:UIButton)
    {
        self.buttonMenuPress()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        AppDel.locationManager = manager
        let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 15)
        self.mapView.camera = camera
        //        self.locationManager.stopUpdatingLocation()
    }
    func getNearbyJobs(lat:String,long:String)
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kNearbyJobs, params: ["latitude":lat,"longitude":long,"page":"1"]) { (dict) in
            print(dict)
            let dictData = dict["data"] as! JSONDICTIONARY
            let arrListing = dictData["listing"] as! [JSONDICTIONARY]
            self.arrMarkers = arrListing
            self.arrFilterMarkers = arrListing
            AppDel.hideHUD()
            self.addMarkersOnMap()
        }
    }
    func addMarkersOnMap() {
        for data in arrFilterMarkers
        {
            let position = CLLocationCoordinate2D.init(latitude: CLLocationDegrees.init("\(data["task_lat"]!)")!, longitude: CLLocationDegrees.init("\(data["task_long"]!)")!)
            let marker = GMSMarker(position: position)
            let v = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 60, height: 30))
            v.cornerRadius = 15
            v.backgroundColor = UIColor.clear
            let imgview = UIImageView.init(frame: v.frame)
            imgview.image = UIImage.init(named: "pin")
            v.addSubview(imgview)
            let lbl = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: v.frame.width, height: 25))
            lbl.text = "$\(data["wage"]!)"
            lbl.textColor = UIColor.white
            lbl.textAlignment = NSTextAlignment.center
            v.addSubview(lbl)
            marker.iconView = v
            marker.userData = data
            marker.map = self.mapView
        }
    }
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        self.arrFilterMarkers = self.arrMarkers
        
        if serchBar.text != ""
        {
            let filter = arrFilterMarkers.filter { (dict) -> Bool in
                if let name = dict["title"] as? String
                {
                    return name.lowercased().range(of:serchBar.text!.lowercased()) != nil
                }
                return false
            }
            print(filter) // ["b": 42]
            arrFilterMarkers = filter
            self.mapView.clear()
            self.addMarkersOnMap()
        }
        else
        {
            arrFilterMarkers = arrMarkers
            self.mapView.clear()
            self.addMarkersOnMap()
        }
        return true
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        infoWindow.delegate = self
        infoWindow.setData(markerData: marker.userData as! JSONDICTIONARY)
        infoWindow.alpha = 0.9
        infoWindow.center = mapView.projection.point(for: marker.position)
        infoWindow.center.y = infoWindow.center.y - 130
        self.view.addSubview(infoWindow)
        return true
    }
    func DoneClicked(_ secondDetailViewController: CustomInfoWindow)
    {
        serchBar.text = ""
        let PostAJobVC = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "JobDetailVC") as! JobDetailVC
        PostAJobVC.Datadict = secondDetailViewController.dict
        PostAJobVC.status = "\(secondDetailViewController.dict["status"]!)"
        PostAJobVC.needApply = true
        self.pushViewController(viewcontroller: PostAJobVC)
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 130
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        infoWindow.removeFromSuperview()
    }
    
    func loadNiB() -> CustomInfoWindow
    {
        let infoWindow = CustomInfoWindow.instanceFromNib() as! CustomInfoWindow
        return infoWindow
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    @objc func didDragMap(_ gestureRecognizer: UIGestureRecognizer)
    {
        switch gestureRecognizer.state {
        case .began: break
//            print("Map drag began")
        case .ended: break
//            print("Map drag ended")
        //           self.getAddressFromLatLog()
        default:
            break
        }
    }
    
    @IBAction func btnPostJobTaskPressed(_ sender: UIButton)
    {
        let PostAJobVC = POSTTASK_STORYBOARD.instantiateViewController(withIdentifier: "PostAJobVC") as! PostAJobVC
        self.pushViewController(viewcontroller: PostAJobVC)
    }
    
    @IBAction func btnFilterPressed(_ sender: UIButton)
    {
        let filterVC = FILTER_STORYBOARD.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.pushViewController(viewcontroller: filterVC)
    }
    
    @IBAction func btnCurrentLocationPressed(_ sender: Any)
    {
        let camera = GMSCameraPosition.camera(withLatitude: (AppDel.locationManager.location?.coordinate.latitude)!, longitude: (AppDel.locationManager.location?.coordinate.longitude)!, zoom: 16)
        self.mapView.camera = camera
    }
    
    @IBAction func btnSearchDetailPressed(_ sender: UIButton)
    {
        let SearchDetailVC = SEARCHDETAIL_STORYBOARD.instantiateViewController(withIdentifier: "SearchDetailVC") as! SearchDetailVC
            self.navigationController?.pushViewController(SearchDetailVC, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        StatusBarStyle.setStatusBar(style: .lightContent)
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
