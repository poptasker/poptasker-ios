//
//  CustomInfoWindow.swift
//  iOSComponents
//
//  Created by Rohan on 27/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol CustomInfoWindowDelegate
{
    func DoneClicked(_ secondDetailViewController: CustomInfoWindow)
}
class CustomInfoWindow: UIView
{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblDescription : UILabel!
    @IBOutlet var lblJobDate : UILabel!
    @IBOutlet var lblHours : UILabel!
    @IBOutlet var lblWages : UILabel!
    var dict = JSONDICTIONARY()
    var blockMarkerInfoPress:((JSONDICTIONARY)->Void)?
    var delegate : CustomInfoWindowDelegate?
    
    //MARK: Load NIB
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomInfoWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func setData(markerData: JSONDICTIONARY)
    {
        self.dict = markerData
        self.lblName!.text = "\(markerData["title"]!)"
        self.lblAddress!.text = "\(markerData["address"]!)"
        self.lblDescription!.text = "\(markerData["description"]!)"
        self.lblDate!.text = "\(markerData["start_date"]!) \(markerData["start_time"]!)"
        self.lblJobDate!.text = "\(UtilityClass.getFormattedDate(string: "\(markerData["start_date"]!)", formatter1: "yyyy-MM-dd", formatter2: "MM-dd")) - \(UtilityClass.getFormattedDate(string: "\(markerData["end_date"]!)", formatter1: "yyyy-MM-dd", formatter2: "MM-dd"))"
        self.lblHours!.text = "\(markerData["working_hours"]!) Hours"
        self.lblWages!.text = "\(markerData["wage"]!) budget"
    }
    
    @IBAction func btnPress(sender:UIButton)
    {
        self.delegate?.DoneClicked(self)
    }
}
