//
//  ConfirmationVC.swift
//  PopTasker
//
//  Created by Admin on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import FacebookShare
class ConfirmationVC: BaseVC
{
    @IBOutlet var lblMessage : UILabel!
    var isComplete : Bool! = false
    var strMessage : String!
    @IBOutlet var viewFB : UIView!
    @IBOutlet var viewActivity : UIStackView!
    var dict : JSONDICTIONARY!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Confirmation"
        self.setNavigationMenu(menuButtons: .BackToRoot, type: .BackToRoot)
        lblMessage.text = strMessage
        if isComplete
        {
            viewFB.isHidden = false
            viewActivity.isHidden = false
        }
        else
        {
            viewFB.isHidden = true
            viewActivity.isHidden = true
        }
    }
    @IBAction func btnActivityPress(sender:UIButton)
    {
        self.navigationController?.popToRootViewController(animated: false)
        AppDel.tabbarVC.tabBar.isHidden = false
        AppDel.tabbarVC.selectedIndex = 1
    }
    @IBAction func btnFBPress(sender:UIButton)
    {
        var content = LinkShareContent.init(url: URL.init(string: "https://poptasker.com/")!)
        content.quote = "Job Title \(dict["title"]!)\nJob Description : \(dict["description"]!)\nAddress:\(dict["address"]!)"
        content.hashtag = Hashtag.init("#PopTasker")
        let dialog = ShareDialog.init(content: content)
        try! dialog.show()
//        dialog.presentingViewController = AppDel.window?.rootViewController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
