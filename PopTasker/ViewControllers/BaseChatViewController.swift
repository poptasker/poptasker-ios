//
//  BaseViewController.swift
//  FirebaseExtension
//
//  Created by User on 3/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class BaseChatViewController: BaseVC
{
    var chatUsersList = [ChatUser]()
    var Chatdelegate : ChatUserDetailsProtocol?
    var conversationRef : DatabaseReference!
    var userTableRef : DatabaseReference!
    var messagesRef : DatabaseReference!
    var isHeaderHidden:Bool = true
    var isMessageDetailsScreen:Bool!
    
    override func viewDidLoad() {
        if isHeaderHidden == true{
            self.setNavigationMenu(menuButtons: .Search, type: .MenuAndSearch)
        }
        super.viewDidLoad()
        Chatdelegate = self
        self.conversationRef = Database.database().reference().child("conversations")
        self.userTableRef = Database.database().reference().child("users")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BaseChatViewController : ChatUserDetailsProtocol
{
    func getUserNameForUserID(_ userID: String?, completion: @escaping(_ userObj: ChatUser) -> Void)
    {
        if let id = userID
        {
            //this will check if user already exist in our local array i.e chatUsersList
            let arr = chatUsersList.filter({ (obj) -> Bool in
                if obj.user_id == userID!
                {
                    completion(obj)
                    return true
                }else
                {
                    return false
                }
            })
            
            if arr.count > 0
            {
                return
            }
            
            self.userTableRef.child(id).observe(DataEventType.value, with: { (snapshot) in
                
                if snapshot.childrenCount > 0
                {
                    //getting all conversations 1 by 1
                    //iterating through all the values
                    let dict = snapshot.value as! Dictionary<String,Any>
                    let obj = ChatUser()
                    obj.update(data: dict)
                    
                    if let index = self.chatUsersList.index(where: {$0.user_id == obj.user_id}) {
                        self.chatUsersList.remove(at: index)
                        self.chatUsersList.insert(obj, at: index)
                    }else{
                        self.chatUsersList.append(obj)
                    }
                    completion(obj)
                }
            })
        }
    }
}
