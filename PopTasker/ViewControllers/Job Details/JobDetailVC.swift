//
//  JobDetailVC.swift
//  PopTasker
//
//  Created by Admin on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class JobDetailVC: BaseVC,showImageDelegate {
    
    var Datadict : JSONDICTIONARY!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblpostedBy : UILabel!
    @IBOutlet var lblDeveloper : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblHours : UILabel!
    @IBOutlet var lblCost : UILabel!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var collectionImg : PostAJobCollection!
    @IBOutlet var collectionHeight : NSLayoutConstraint!
    @IBOutlet var btnPostedBy : UIButton!
    @IBOutlet var lblPostedByDate : UILabel!
    @IBOutlet var btnComplete : UIButton!
    @IBOutlet var btnCancel : UIButton!
    var status : String!
    var arrImg = [JSONDictionary]()
    var needApply : Bool! = false
    
    var strComeFrom:String = ""
    var taskId:Int!
    var JobTitle:String!
    
    override func viewDidLoad(){
        super.viewDidLoad()

        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        if strComeFrom == "messagedetailvc"{
            getJobDetail(id: "\(taskId!)")
            self.navigationItem.title = JobTitle
        }else if strComeFrom == "frompushnotification"{
            getJobDetail(id: "\(taskId!)")
            self.navigationItem.title = "Job Details"
        }else{
            getJobDetail(id: "\(Datadict["task_id"]!)")
            self.navigationItem.title = "\(Datadict["title"]!)"
        }
        collectionImg.blockCollectionViewDidSelectAtIndexPath = {(indexpah,str) -> Void in
            
            let ratingVC = ShowImageViewController(nibName: "ShowImageViewController", bundle: nil)
            ratingVC.delegate = self
            ratingVC.arrImg = self.arrImg
            ratingVC.currentIndex = indexpah.row
            self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
    }
    func btnCloseClick(_ sender: ShowImageViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    
    func getJobDetail(id:String)
    {
        AppDel.showHUD()
        let view = UIView.init(frame: self.view.frame)
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        ApiManager.shared.postDataWithHeader(url: kActivitieDetails, params: ["task_id":id]) { (dict) in
            AppDel.hideHUD()
            view.removeFromSuperview()
            print(dict)
            let data = dict["data"] as! JSONDICTIONARY
            self.showHideButtons(dict: data)
            self.Datadict = data
            self.lblTitle.text = "\(data["title"]!)"
            self.lblAddress.text = "\(data["address"]!)"
            self.lblDeveloper.text = "\(data["description"]!)"
            self.lblCost.text = "\(data["wage"]!) budget"
            self.lblHours.text = "\(data["working_hours"]!) Hours"
            self.lblpostedBy.text = "Posted by"
            self.btnPostedBy.setTitle("\(data["employer"]!)", for: .normal)
            self.lblPostedByDate.text = "on \(data["updated_at"]!)"
            self.navigationItem.title = "\(data["title"]!)"
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd"
            
            self.lblDate.text = "\(UtilityClass.getFormattedDate(string: "\(data["start_date"]!)", formatter1: "yyyy-MM-dd", formatter2: "MMM dd")) - \(UtilityClass.getFormattedDate(string: "\(data["end_date"]!)", formatter1: "yyyy-MM-dd", formatter2: "MMM dd"))"
            
            self.lblTime.text = "\(UtilityClass.getFormattedTime(string: "\(data["start_time"]!)", formatter1: "HH:mm:ss", formatter2: "hh:mm a")) - \(UtilityClass.getFormattedTime(string: "\(data["end_time"]!)", formatter1: "HH:mm:ss", formatter2: "hh:mm a"))"
            self.arrImg = data["pictures"] as! [JSONDictionary]
            self.imgView.cornerRadius = self.imgView.frame.width/2
            self.imgView.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
            self.imgView.layer.borderWidth = 2.0
            let img  = LetterImageGenerator.imageWith(name: "\(data["title"]!)")
            if self.arrImg.count > 0
            {
                self.imgView.sd_setImage(with: URL.init(string: "\(self.arrImg[0]["thumb"]!)"), placeholderImage: UIImage.init(named: "dummy_placeholder"), options: [], completed: nil)
            }
            else
            {
                self.imgView.image = img
                self.collectionHeight.constant = 0
            }
            self.collectionImg.createUserArray(arr: self.arrImg)
        }
    }
    func showHideButtons(dict:JSONDICTIONARY)
    {
        //        if (getIntent().getBooleanExtra(NEED_APPLY, true))
        //        {
        //            if (jobModel.isApplied() == 1)
        //            {
        //                tvApplyJob.setVisibility(View.GONE);
        //            }
        //            else
        //            {
        //                tvApplyJob.setVisibility(View.VISIBLE);
        //            }
        //        }
        //        else
        //        {
        //            tvApplyJob.setVisibility(View.GONE);
        //        }
        if needApply == true
        {
            if Int("\(dict["is_applied"]!)")  == 0
            {
                btnComplete.setTitle("APPLY", for: .normal)
                btnCancel.isHidden = true
            }
            else
            {
                needNoApply(dict: dict)
//                btnComplete.setTitle("COMPLETE JOB", for: .normal)
//                btnCancel.isHidden = false
//                btnComplete.isHidden = false
            }
        }
        else
        {
            needNoApply(dict: dict)
//            btnComplete.setTitle("COMPLETE JOB", for: .normal)
//            btnComplete.isHidden = true
//            btnCancel.isHidden = true
        }
    }
   
    func needNoApply(dict:JSONDICTIONARY) {
        //        if (jobModel.getStatus().equalsIgnoreCase("accepted") && !jobModel.getPosted())
        //        {
        //            if (jobModel.getIs_start() == 1)
        //            {
        //                tvCmpJob.setVisibility(View.VISIBLE);
        //            }
        //            else
        //            {
        //                tvCmpJob.setVisibility(View.GONE);
        //            }
        //            tvCancelJob.setVisibility(View.VISIBLE);
        //        }
        //        else
        //        {
        //            tvCmpJob.setVisibility(View.GONE);
        //            tvCancelJob.setVisibility(View.GONE);
        //        }
        
        if status == "Accepted" && Int("\(dict["posted"]!)") == 0
        {
            if Int("\(dict["is_start"]!)") == 1
            {
                btnComplete.isHidden = false
            }
            else
            {
                btnComplete.isHidden = true
            }
            btnCancel.isHidden = false
        }
        else
        {
            btnComplete.isHidden = true
            btnCancel.isHidden = true
        }
        
        //
        //        if (jobModel.getPosted() && jobModel.getIs_start() == 1)
        //        {
        //            tvReviewOrNoShow.setVisibility(View.VISIBLE);
        //        }
        //        else
        //        {
        //            tvReviewOrNoShow.setVisibility(View.GONE);
        //        }
        
        if Int("\(dict["posted"]!)") == 1 &&  Int("\(dict["is_start"]!)") == 1
        {
            btnComplete.isHidden = false
            btnComplete.setTitle("REVIEW OR NO SHOW", for: .normal)
        }
    }
    //MARK:-
    @IBAction func btnProfilePress(sender:UIButton)
    {
        getProfileData()
    }
    
    func getProfileData()
    {
        AppDel.showHUD()
        let param = ["employee_id":"\(self.Datadict["employer_id"]!)"] as JSONDictionary
        ApiManager.shared.postDataWithHeader(url: kGetApplicant, params: param) { (dictProfile) in
            AppDel.hideHUD()
            print(dictProfile)
            if let data = dictProfile["data"] as? JSONDictionary
            {
                let profileView = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantProfileViewController") as! ApplicantProfileViewController
                profileView.dict = data
                self.pushViewController(viewcontroller: profileView)
            }
            else
            {
                UtilityClass.showAlert(str: "Profile not found.")
            }
        }
    }
    
    @IBAction func btnCompleteJobPress(sender:UIButton)
    {
        if sender.titleLabel?.text == "REVIEW OR NO SHOW"
        {
            let reviewObj = ENGINEER_STORYBOARD.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
            reviewObj.dictData = self.Datadict
            self.pushViewController(viewcontroller: reviewObj)
        }
        
        else if sender.titleLabel?.text == "APPLY"
        {
            let alert = UIAlertController.init(title: kAppName, message: "You are applying job as \(self.Datadict["title"]!).", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Confirm", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kApply, params: ["task_id":"\(self.Datadict["task_id"]!)"]) { (dict) in
                    AppDel.hideHUD()
                    print(dict)
                    self.navigationController?.popViewController(animated: true)
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                }
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController.init(title: kAppName, message: "You are completing job as \(self.Datadict["title"]!).", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "YES", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kCompleteJob, params: ["task_id":"\(self.Datadict["task_id"]!)"]) { (dict) in
                    AppDel.hideHUD()
                    print(dict)
                    let successView = CONFIRMATION_STORYBOARD.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                    successView.strMessage = "You completed a job successfully"
                    self.pushViewController(viewcontroller: successView)
                }
            }))
            alert.addAction(UIAlertAction.init(title: "NO", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func btnCancelJobPress(sender:UIButton)
    {
        let alert = UIAlertController.init(title: kAppName, message: "You are cancelling job as \(self.Datadict["title"]!).", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "YES", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            
            AppDel.showHUD()
            ApiManager.shared.postDataWithHeader(url: kCanceljob, params: ["task_id":"\(self.Datadict["task_id"]!)"]) { (dict) in
                AppDel.hideHUD()
                print(dict)
                let successView = CONFIRMATION_STORYBOARD.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                successView.strMessage = "You cancelled a job successfully"
                self.pushViewController(viewcontroller: successView)
            }
        
        }))
        alert.addAction(UIAlertAction.init(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
