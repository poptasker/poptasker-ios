//
//  ReviewViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 03/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ReviewViewController: BaseVC {

    var dictData : JSONDICTIONARY!
    
    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var lblEmployeeName : UILabel!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDescription : UILabel!
    @IBOutlet var rateView : RateView!
    @IBOutlet var btnEndorsYes : UIButton!
    @IBOutlet var btnEndorsNo : UIButton!
    @IBOutlet var btnNOShowYes : UIButton!
    @IBOutlet var btnNOShowNo : UIButton!
    @IBOutlet var txtComment : TextFieldComment!
    var isEndors : Int! = 0
    var isNOShow : Int! = 0
    var isFromHome : Bool! = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Confirmation"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        
        setLayout()
        
        print(dictData)
        if isFromHome
        {
            self.imgProfile.sd_setImage(with: URL.init(string: "\(dictData["empoyee_pic"]!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)

        }
        else
        {
            self.imgProfile.sd_setImage(with: URL.init(string: "\(dictData["profile_picture_employee"]!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)

        }
        self.lblEmployeeName.text = "\(dictData["employee"]!)"
        
        if self.lblEmployeeName.text == ""
        {
            self.lblEmployeeName.text = "--"
        }
        self.lblTitle.text = "\(dictData["title"]!)"
        self.lblDescription.text = "\(dictData["description"]!)"

        btnEndorYes(sender: UIButton())
        btnNOShowYes(sender: UIButton())
    }
    
    func setLayout()
    {
        self.rateView.fullSelectedImage = #imageLiteral(resourceName: "review_star")
        self.rateView.halfSelectedImage = self.rateView.fullSelectedImage
        self.rateView.notSelectedImage = #imageLiteral(resourceName: "review_star_empty")
        self.rateView.editable = true
        self.rateView.maxRating = 5
        self.rateView.leftMargin = 0
        self.rateView.midMargin = 1
        self.rateView.rating = 0

        btnEndorsNo.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        btnEndorsNo.layer.borderWidth = 2
        btnEndorsNo.layer.cornerRadius = btnEndorsNo.frame.height/2
        
        btnEndorsYes.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        btnEndorsYes.layer.borderWidth = 2
        btnEndorsYes.layer.cornerRadius = btnEndorsYes.frame.height/2
        
        btnNOShowNo.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        btnNOShowNo.layer.borderWidth = 2
        btnNOShowNo.layer.cornerRadius = btnNOShowNo.frame.height/2
        
        btnNOShowYes.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        btnNOShowYes.layer.borderWidth = 2
        btnNOShowYes.layer.cornerRadius = btnNOShowYes.frame.height/2
        
        txtComment.setLeftTitle(title: "Comment:")
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width/2
        self.imgProfile.layer.borderColor = UIColor.setPurpalSliderColor().cgColor
        self.imgProfile.layer.borderWidth = 2.0
    }
    
    @IBAction func btnEndorYes(sender:UIButton)
    {
        btnEndorsYes.isSelected = true
        btnEndorsNo.isSelected = false
        
        btnEndorsYes.backgroundColor = UIColor.setPurpalSliderColor()
        btnEndorsNo.backgroundColor = UIColor.white
        isEndors = 1
    }
    
    @IBAction func btnEndorNO(sender:UIButton)
    {
        btnEndorsNo.isSelected = true
        btnEndorsYes.isSelected = false
        
        btnEndorsNo.backgroundColor = UIColor.setPurpalSliderColor()
        btnEndorsYes.backgroundColor = UIColor.white
        
        isEndors = 0
    }
    
    
    @IBAction func btnNOShowYes(sender:UIButton)
    {
        btnNOShowYes.isSelected = true
        btnNOShowNo.isSelected = false
        
        btnNOShowYes.backgroundColor = UIColor.setPurpalSliderColor()
        btnNOShowNo.backgroundColor = UIColor.white
        isNOShow = 1
    }
    
    @IBAction func btnNOShowNo(sender:UIButton)
    {
        btnNOShowNo.isSelected = true
        btnNOShowYes.isSelected = false
        
        btnNOShowNo.backgroundColor = UIColor.setPurpalSliderColor()
        btnNOShowYes.backgroundColor = UIColor.white
        isNOShow = 0
    }
    
    @IBAction func btnDonePress(sender:UIButton)
    {
        if rateView.rating > 0
        {
            if txtComment.text?.count != 0
            {
                let param = ["task_id":"\(dictData["task_id"]!)","no_show":"\(isNOShow!)","endorsement":"\(isEndors!)","employee_id":"\(dictData["employee_id"]!)","comments":txtComment.text!,"rating":"\(rateView.rating)"]
                print(param)
                
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kReview, params: param) { (dict) in
                    
                    AppDel.hideHUD()
                    print(dict)
                    self.navigationController?.popViewController(animated: true)
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please provide comment.")
            }
            
        }
        else
        {
            UtilityClass.showAlert(str: "Please give rating.")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
