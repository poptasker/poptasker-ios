//
//  AddPaymentCardViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 20/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import Stripe
class AddPaymentCardViewController: BaseVC
{
    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var txtNumber : UITextField!
    @IBOutlet var txtValidity : UITextField!
    @IBOutlet var txtCVV : UITextField!
    @IBOutlet var txtName : UITextField!

    var imgView: UIImageView {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "payment")
        imgV.alpha = 0.4
        return imgV
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Payment"
        viewHeight.constant = UIScreen.main.bounds.height
        self.setNavigationMenu(menuButtons: .BackToRoot, type: .BackToRoot)
//        AppDel.window?.addSubview(imgView)
    }

    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace != -92) {
            if textField == txtValidity
            {
                if textField.text?.count == 5
                {
                    return false
                }
                if textField.text?.count == 2{
                    txtValidity.text = txtValidity.text! + "/"
                }
            }
            else if textField == txtNumber
            {
                if textField.text?.count == 20
                {
                    return false
                }
                if (textField.text?.count)! % 5 == 0
                {
                     txtNumber.text = txtNumber.text! + " "
                }
            }
            if textField == txtCVV
            {
                if textField.text?.count == 4
                {
                    return false
                }
               
            }
        }
       
         return true
    }
    @IBAction func btnSavePress(sender:UIButton)
    {
        if validate()
        {
            let arr = txtValidity.text!.components(separatedBy: "/")
            let number = txtNumber.text!.replacingOccurrences(of: " ", with: "")
            if STPCardValidator.validationState(forNumber:number , validatingCardBrand: true) == .valid {
            
                if STPCardValidator.validationState(forExpirationMonth: "\(arr[0])") == .valid
                {
                    if STPCardValidator.validationState(forExpirationYear: "\(arr[1])", inMonth: "\(arr[0])") == .valid
                    {
                        if STPCardValidator.validationState(forCVC: txtCVV.text!, cardBrand: STPCardValidator.brand(forNumber:number)) == .valid
                        {
                            AppDel.showHUD()
                            ApiManager.shared.postDataWithHeader(url: kAddCard, params: ["card_number":number,"cvv":txtCVV.text!,"exp_month":"\(arr[0])","exp_year":"\(arr[1])","name_on_card":txtName.text!,"billing_name":"","billing_address":"","billing_city":"","billing_state":"","billing_country":"","billing_zip":""]) { (dict) in
                                AppDel.hideHUD()
                                print(dict)
                            self.navigationController?.popViewController(animated: true)
                                UtilityClass.showAlert(str: "\(dict["message"]!)")
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "Please enter valid cvv.")
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please enter valid card expiration year.")
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please enter valid card expiration month.")
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter valid card number.")
            }
        }
        
    }
    func validate() -> Bool
    {
        if txtNumber.text!.count != 0
        {
            if txtValidity.text!.count >= 5
            {
                if txtCVV.text!.count != 0
                {
                    if txtName.text!.count != 0
                    {
                        return true
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please enter card holder name.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please enter cvv.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter card validity.")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter card number.")
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
