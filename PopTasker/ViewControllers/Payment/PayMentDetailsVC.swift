//
//  PayMentDetailsVCViewController.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class PayMentDetailsVC: BaseVC {

    @IBOutlet var btnValidity : UIButton!
    @IBOutlet var btnCardDetail : UIButton!
    @IBOutlet var viewSegment : UIView!
    @IBOutlet var tbl_Payment : PaymentTableview!
    @IBOutlet var btnSave: NeueComonButtonwithGradiantImg!
    var cardId : String! = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "Payment Details"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        getPayoutDetails()

        self.viewSegment.layer.borderColor = UIColor.setGrayLignColor().cgColor
        self.viewSegment.layer.borderWidth = 0.7
        self.btnValidity.layer.borderColor = UIColor.setGrayLignColor().cgColor
        self.btnValidity.layer.borderWidth = 0.7
        self.btnValidity.isSelected = true
        self.btnCardDetail.isSelected = false
        btnValidity.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        btnCardDetail.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        
    }
    
    func getPayoutDetails()
    {
        AppDel.showHUD()
        ApiManager.shared.getData(url: kGetPayout) { (dict) in
            print(dict)
            if let dataDict = dict["data"] as? JSONDICTIONARY
            {
                self.tbl_Payment.paymentArrayForValidity[0].txtValue = "\(dataDict["param1"]!)"
                self.tbl_Payment.paymentArrayForValidity[1].txtValue = "\(dataDict["bank_name"]!)"
                self.tbl_Payment.paymentArrayForValidity[2].txtValue = "\(dataDict["param2"]!)"
                self.tbl_Payment.paymentArrayForValidity[3].txtValue = "\(dataDict["account_no"]!)"
                
            }
            self.tbl_Payment.reloadData()
            AppDel.hideHUD()
        }
    }
    
    func getSavedCards()
    {
        AppDel.showHUD()
        ApiManager.shared.getData(url: kGetCards) { (dict) in
            print(dict)
            if let dataDict = dict["data"] as? [JSONDICTIONARY]
            {
                if dataDict.count > 0
                {
                    self.cardId = "\(dataDict[0]["card_id"]!)"
                    
                    self.tbl_Payment.paymentArrayForCardDetail[0].txtValue = "XXXX-XXXX-XXXX-\(dataDict[0]["last_four_digit"]!)"
                    self.tbl_Payment.paymentArrayForCardDetail[1].txtValue = "\(dataDict[0]["card_holder_name"]!)"
                    self.tbl_Payment.paymentArrayForCardDetail[2].txtValue = "\(dataDict[0]["card_type"]!)"
                }
                
                self.tbl_Payment.reloadData()
            }
            
            AppDel.hideHUD()
        }
    }
    
    @IBAction func btnTabTapped(_ sender: UIButton)
    {
        btnValidity.isSelected = false
        btnCardDetail.isSelected = false
        sender.isSelected = !sender.isSelected
        if(sender.tag == 0){
            tbl_Payment.boolCheckTab = false
            btnSave.setTitle("Save", for: .normal)
            btnSave.setBackgroundImage(GradientImage.gradientTheamBackgroundFrame(view: btnSave), for: .normal)
            getPayoutDetails()
        }else{
            tbl_Payment.boolCheckTab = true
            btnSave.setTitle("Delete", for: .normal)
            btnSave.setBackgroundImage(GradientImage.getRedColor(view: btnSave), for: .normal)
            getSavedCards()
        }
    }
    
    @IBAction func btnSavePress(sender:UIButton)
    {
        if btnSave.titleLabel?.text == "Save"
        {
            if validatePayoutDetails()
            {
                let param = ["param1":"\(self.tbl_Payment.paymentArrayForValidity[0].txtValue!)","bank_name":"\(self.tbl_Payment.paymentArrayForValidity[1].txtValue!)","param2":"\(self.tbl_Payment.paymentArrayForValidity[2].txtValue!)","account_no":"\(self.tbl_Payment.paymentArrayForValidity[3].txtValue!)","param3":"","param4":""] as JSONDICTIONARY
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kAddpayout, params: param) { (dict) in
                    print(dict)
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                    AppDel.hideHUD()
                }
            }
        }
        else
        {
            if self.cardId != ""
            {
                AppDel.showHUD()
                ApiManager.shared.postDataWithHeader(url: kDeleteCard, params: ["card_id":self.cardId!]) { (dict) in
                    print(dict)
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                    self.cardId = ""
                    self.tbl_Payment.paymentArrayForCardDetail[0].txtValue = ""
                    self.tbl_Payment.paymentArrayForCardDetail[1].txtValue = ""
                    self.tbl_Payment.paymentArrayForCardDetail[2].txtValue = ""
                    self.getSavedCards()
                }
            }
            else
            {
                UtilityClass.showAlert(str: "No card added.")
            }
        }
    }
    
    func validatePayoutDetails() -> Bool
    {
        if self.tbl_Payment.paymentArrayForValidity[0].txtValue.count != 0
        {
            if self.tbl_Payment.paymentArrayForValidity[1].txtValue.count != 0
            {
                if self.tbl_Payment.paymentArrayForValidity[2].txtValue.count != 0
                {
                    if self.tbl_Payment.paymentArrayForValidity[1].txtValue.count != 0
                    {
                        return true
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please enter account number.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please enter bank routing number.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter bank name.")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter name.")
            return false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
