//
//  KeywordVC.swift
//  PopTasker
//
//  Created by Admin on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol keywordDelegate {
    func selectedTags(arrIndex:[Int],arrTags:[JSONDICTIONARY])
}
class KeywordVC: BaseVC, HTagViewDelegate, HTagViewDataSource {

//    @IBOutlet var tagView1: TagView!
    @IBOutlet var tagView : HTagView!
    var arrTag = [JSONDICTIONARY]()
    var arrSelectedTag = [JSONDICTIONARY]()
    var deleg : keywordDelegate?
    var isSelected : Bool! = false
    var arrSelectedStr = [String]()
    var selectedIndex = [Int]()

    var imgView: UIImageView
    {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "keywords")
        imgV.alpha = 0.4
        return imgV
    }
    override func viewDidLoad() {

        super.viewDidLoad()
        self.navigationItem.title = "Keywords"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
//        AppDel.window?.addSubview(imgView)
        initTagView()
        tagView.delegate = self
        tagView.dataSource = self
        tagView.multiselect = true
        tagView.marg = 3
        tagView.btwTags = 16
        tagView.btwLines = 20
        tagView.tagFont = UIFont.init(name: "HelveticaNeueLTPro-Lt", size: 16.7)!
        tagView.tagSecondBackColor = UIColor.white
        tagView.tagSecondTextColor = UIColor.black
        tagView.tagMainBackColor = UIColor.init(patternImage: GradientImage.gradientTheamBackgroundFrame(view: self.view))
        tagView.tagMainTextColor = UIColor.white
        tagView.tagBorderColor = UIColor(red: 61.0/255.0, green: 196.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
    
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func initTagView()
    {
        AppDel.showHUD()
        let v = UIView.init(frame: self.view.frame)
        v.backgroundColor = UIColor.white
        self.view.addSubview(v)
        ApiManager.shared.getData(url: kKeywords) { (dict) in
            AppDel.hideHUD()
            print(dict)
            v.removeFromSuperview()
            self.arrTag = dict["data"] as! [JSONDICTIONARY]
           
            self.tagView.reloadData()
            
            if self.isSelected
            {
                for tag in self.selectedIndex
                {
                    self.tagView.selectTagAtIndex(tag)
                    self.arrSelectedTag.append(self.arrTag[tag])
                }
            }
        }
    }
    
    @IBAction func btnConfirmPress(sender:UIButton)
    {
        print(arrSelectedTag)
        deleg?.selectedTags(arrIndex: tagView.selectedIndices, arrTags: arrSelectedTag)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - HTagViewDataSource
    func numberOfTags(_ tagView: HTagView) -> Int {
        
        return arrTag.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String{
        return "\(arrTag[index]["keyword"]!)"
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType{
        return .select
    }

    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    // MARK: - HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
        arrSelectedTag = Array()
        for index in selectedIndices
        {
            arrSelectedTag.append(arrTag[index])
        }
    }
   
}
