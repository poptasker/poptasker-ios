//
//  MapViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 04/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import GooglePlacePicker

class MapViewController: BaseVC,GMSPlacePickerViewControllerDelegate {

    var latitude : CLLocationDegrees!
    var longitude : CLLocationDegrees!
    var currentAddress : String!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate  = self
        present(placePicker, animated: true, completion: nil)
        
    }
    override func loadView() {

    
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("Place name \(place.name)")
        print("Place address \(place.formattedAddress)")
        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
