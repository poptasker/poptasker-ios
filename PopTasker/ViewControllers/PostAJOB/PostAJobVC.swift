
//
//  PostAJobVC.swift
//  PopTasker
//
//  Created by Admin on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlacePicker
import BSImagePicker
import Photos

class PostAJobVC: BaseVC,PostAJobDatePickerDelegate,PostAJobTimePickerDelegate,keywordDelegate,CLLocationManagerDelegate,GMSPlacePickerViewControllerDelegate {
    
    @IBOutlet weak var tablePostaJob: PostAjobTableview!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    var selectedIndex = [Int]()
    var selectedTags = [JSONDICTIONARY]()
    var strStartDate : String! = ""
    var strEndDate : String! = ""
    var strStartTime : String! = ""
    var strEndTime : String! = ""
    var imgs = [UIImage]()
    let vc = BSImagePickerViewController()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Post a Task"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        checkPAymentAdded()
        setCurrentLocation()
        tablePostaJob.blockTableViewDidSelecLocation = { (index, str) -> Void in
            
            if index == 3
            {
                if CLLocationManager.locationServicesEnabled()
                {
                    switch(CLLocationManager.authorizationStatus())
                    {
                    case .authorizedAlways, .authorizedWhenInUse:
                        AppDel.showHUD()
                        
                        self.getAddressFromLatLon(pdblLatitude: "\(String(describing: AppDel.locationManager.location!.coordinate.latitude))", withLongitude: "\(String(describing: AppDel.locationManager.location!.coordinate.longitude))")
                        
                        break
                        
                    case .notDetermined,.restricted,.denied:
                        
                        print("Not determined.")
                        let alertController = UIAlertController(title: kAppName, message: "GPS permission allows you to access location data .Please allow it to App settings for aditional functionality", preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl)
                            {
                                UIApplication.shared.open(settingsUrl, completionHandler: nil)
                            }
                        }
                        alertController.addAction(settingsAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        break
                        
                    }
                }
            }
            if index == 6
            {
                 self.openImagePicker()
            }
            
        }
        tablePostaJob.blockTableViewDidSelecth = { (index, str) -> Void in
            print(index)
           
            if(index == 2)
            {
                let keywordVC = KEYWORD_STORYBOARD.instantiateViewController(withIdentifier: "KeywordVC") as! KeywordVC
                keywordVC.deleg = self
                if self.tablePostaJob.userFieldArray[2].value != "Select Keywords"
                {
                    keywordVC.isSelected = true
                    keywordVC.selectedIndex = self.selectedIndex
                }
                self.pushViewController(viewcontroller: keywordVC)

            }
            if(index == 3)
            {
                if CLLocationManager.locationServicesEnabled() {
                    
                    switch(CLLocationManager.authorizationStatus())
                    {
                        
                    case .authorizedAlways, .authorizedWhenInUse:
                        let center = CLLocationCoordinate2D(latitude: (AppDel.locationManager.location?.coordinate.latitude)!, longitude: (AppDel.locationManager.location?.coordinate.longitude)!)
                        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
                        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
                        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                        let config = GMSPlacePickerConfig(viewport: nil)
                        let placePicker = GMSPlacePickerViewController(config: config)
                        
                        placePicker.delegate  = self
                        self.present(placePicker, animated: true, completion: nil)

                        break
                        
                    case .notDetermined,.restricted,.denied:
                        
                        print("Not determined.")
                        let alertController = UIAlertController(title: kAppName, message: "GPS permission allows you to access location data .Please allow it to App settings for aditional functionality", preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl)
                            {
                                UIApplication.shared.open(settingsUrl, completionHandler: nil)
                            }
                        }
                        alertController.addAction(settingsAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        break
                        
                    }
                }
            }
            else if(index == 4)
            {

                let ratingVC = PostAJobDatePickerViewController(nibName: "PostAJobDatePickerViewController", bundle: nil)
                ratingVC.delegate = self
//                if self.strStartDate != ""
//                {
//                    let dateFormatter = DateFormatter()
//                    dateFormatter.dateFormat = "dd-MM-yyyy"
//                    dateFormatter.locale = NSLocale.current
////                    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
//                    let dataDate = dateFormatter.date(from:self.strStartDate!)
//
//                    ratingVC.datePickerFrom.date = UtilityClass.getFormattedDate(string: self.strStartDate, formatter1: "dd-MM-yyyy", formatter2: "yyyy-MM-dd HH:mm:ss +zzzz")
//                }
                
                self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
            }
            else if(index == 5)
            {
                let ratingVC = PostAJobTimePickerViewController(nibName: "PostAJobTimePickerViewController", bundle: nil)
                ratingVC.delegate = self
                self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
            }
            else if(index == 6)
            {
                self.openImagePicker()
            }
            else if(index == 8)
            {
                if self.validateJob()
                {
                    var strId = [String]()
                    for tag in self.selectedTags
                    {
                        strId.append("\(tag["id"]!)")
                    }
                    let param = ["title" : "\(self.tablePostaJob.userFieldArray[0].value!)","description":"\(self.tablePostaJob.userFieldArray[1].value!)","keywords":"\(strId.joined(separator: ","))","address":"\(self.tablePostaJob.userFieldArray[3].value!)","task_lat":"\(String(describing: AppDel.locationManager.location!.coordinate.latitude))","task_long":"\(String(describing: AppDel.locationManager.location!.coordinate.longitude))","start_date":self.strStartDate!,"end_date":self.strEndDate!,"start_time":self.strStartTime!,"end_time":self.strEndTime!,"wage":"\(self.tablePostaJob.userFieldArray[7].value!)"]
                    print(param)
                    var arrData = [Data]()
                    if self.tablePostaJob.arrImgs.count > 0
                    {
                        for i in 0 ... self.tablePostaJob.arrImgs.count - 1
                        {
                            let img = self.tablePostaJob.arrImgs[i]
                            arrData.append(img.pngData()!)
                        }
                    }
                    
                    AppDel.showHUD()
                    ApiManager.shared.PostWithImage(url: kCreatejob, imageData: arrData, parameters: param, onCompletion: { (dict) in
                        print(dict)
                        let keywordVC =   CONFIRMATION_STORYBOARD.instantiateViewController(withIdentifier: "ConfirmationVC") as! ConfirmationVC
                        keywordVC.isComplete = true
                        keywordVC.strMessage = "You posted a job successfully"
                        keywordVC.dict = param
                        self.pushViewController(viewcontroller: keywordVC)
                        AppDel.hideHUD()
                    })
                }
            }
        }
        
    }
    func setCurrentLocation() {
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                AppDel.showHUD()
                
                self.getAddressFromLatLon(pdblLatitude: "\(String(describing: AppDel.locationManager.location!.coordinate.latitude))", withLongitude: "\(String(describing: AppDel.locationManager.location!.coordinate.longitude))")
                self.tablePostaJob.reloadData()
                break
                
            case .notDetermined,.restricted,.denied:
                
                print("Not determined.")
                let alertController = UIAlertController(title: kAppName, message: "GPS permission allows you to access location data .Please allow it to App settings for aditional functionality", preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl)
                    {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                }
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
                
                break
                
            }
            
            //            self.locationManager.startUpdatingLocation()
        }
    }
    func checkPAymentAdded()  {
        AppDel.showHUD()
        ApiManager.shared.postDataWithErrorHandle(url: kCheckPayment, params: [:]) { (dict,err) in
            AppDel.hideHUD()
            if dict == nil
            {
                let alert = UIAlertController.init(title: kAppName, message: "No Card Added Please Add Payment Method!!", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "Next", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                    let pamentObj = PAYMENT_STORYBOARD.instantiateViewController(withIdentifier: "AddPaymentCardViewController") as! AddPaymentCardViewController
                    self.pushViewController(viewcontroller: pamentObj)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
        AppDel.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        AppDel.locationManager.requestWhenInUseAuthorization()
        AppDel.locationManager.delegate = self;
        AppDel.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        AppDel.locationManager.distanceFilter = 10
        AppDel.locationManager.allowsBackgroundLocationUpdates = true
        AppDel.locationManager.startUpdatingLocation()
        
    }

    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("Place name \(place.name)")
        print("Place address \(place.formattedAddress)")
        if place.formattedAddress == nil{
            getAddressFromLatLon(pdblLatitude: "\(place.coordinate.latitude)", withLongitude: "\(place.coordinate.longitude)")
        }
        else
        {
            self.tablePostaJob.userFieldArray[3].value = "\(place.name),\(place.formattedAddress!)"
            self.tablePostaJob.reloadData()
        }
        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        AppDel.locationManager = manager
//        self.locationManager.stopUpdatingLocation()
    }
    
    func getAddressFromLatLon(pdblLatitude: String!, withLongitude pdblLongitude: String!) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude!)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude!)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0
                {
                    let pm = placemarks![0]

                    var addressString : String = ""
                    
                    if pm.name != nil {
                        addressString = addressString + pm.name! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print(addressString)
                    self.tablePostaJob.userFieldArray[3].value = addressString
                     self.tablePostaJob.reloadData()
                    AppDel.hideHUD()
                }
        })
    }
    func DoneTimePickerClicked(_ secondDetailViewController: PostAJobTimePickerViewController)
    {
        strStartTime = UtilityClass.getFormattedTime(string: "\(secondDetailViewController.datePickerFrom.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "HHmm")
        strEndTime = UtilityClass.getFormattedTime(string: "\(secondDetailViewController.datePickerTo.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "HHmm")

        tablePostaJob.userFieldArray[5].value = "\(UtilityClass.getFormattedTime(string: "\(secondDetailViewController.datePickerFrom.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "hh:mm a")) To \(UtilityClass.getFormattedDate(string: "\(secondDetailViewController.datePickerTo.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "hh:mm a"))"
        
        tablePostaJob.reloadData()
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    
    func DoneDatePickerClicked(_ secondDetailViewController: PostAJobDatePickerViewController)
    {
        strStartDate = UtilityClass.getFormattedTime(string: "\(secondDetailViewController.datePickerFrom.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "dd-MM-yyyy")
        strEndDate = UtilityClass.getFormattedTime(string: "\(secondDetailViewController.datePickerTo.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "dd-MM-yyyy")
        
        tablePostaJob.userFieldArray[4].value = "\(UtilityClass.getFormattedDate(string: "\(secondDetailViewController.datePickerFrom.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "M/dd")) - \(UtilityClass.getFormattedDate(string: "\(secondDetailViewController.datePickerTo.date)", formatter1: "yyyy-MM-dd HH:mm:ss +zzzz", formatter2: "M/dd"))"
        tablePostaJob.reloadData()
        
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    
    //MARK:-
    
    func selectedTags(arrIndex: [Int], arrTags: [JSONDICTIONARY])
    {
        print(arrTags)
        var str = [String]()
        for tag in arrTags
        {
            str.append("\(tag["keyword"]!)")
        }
       
        self.selectedIndex = arrIndex
        self.selectedTags = arrTags

        tablePostaJob.userFieldArray[2].value = str.joined(separator: ",")
        tablePostaJob.reloadData()
    }
    
    //MARK:- Open image picker
    func openImagePicker()
    {
        self.imgs = Array()
        vc.maxNumberOfSelections = 4
        vc.takePhotoIcon = UIImage(named: "camera")
        vc.albumButton.tintColor = UIColor.green
        vc.cancelButton.tintColor = UIColor.red
        vc.doneButton.tintColor = UIColor.purple
        vc.selectionCharacter = "✓"
        vc.selectionFillColor = UIColor.gray
        vc.selectionStrokeColor = UIColor.yellow
        vc.selectionShadowColor = UIColor.red
        vc.takePhotos = true
        vc.selectionTextAttributes[NSAttributedString.Key.foregroundColor] = UIColor.lightGray
        vc.cellsPerRow = {(verticalSize: UIUserInterfaceSizeClass, horizontalSize: UIUserInterfaceSizeClass) -> Int in
            switch (verticalSize, horizontalSize) {
            case (.compact, .regular): // iPhone5-6 portrait
                return 2
            case (.compact, .compact): // iPhone5-6 landscape
                return 2
            case (.regular, .regular): // iPad portrait/landscape
                return 3
            default:
                return 2
            }
        }
        
        bs_presentImagePickerController(vc, animated: true,select: { (asset: PHAsset) -> Void in
            
            print("Selected: \(asset)")
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            
            for asset in assets
            {
                self.imgs.append(self.getAssetThumbnail(asset: asset))
            }
            self.tablePostaJob.createUserArray(arrImg: self.imgs)
            
        }, completion: nil)
    }

    func getAssetThumbnail(asset: PHAsset) -> UIImage
    {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 300, height: 300), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    func validateJob() -> Bool
    {
        if self.tablePostaJob.userFieldArray[0].value != "Type in"
        {
            if self.tablePostaJob.userFieldArray[1].value != "Type in"
            {
                if selectedTags.count != 0
                {
                    if self.tablePostaJob.userFieldArray[3].value != "Type in"
                    {
                        if strStartDate != "" && strEndDate != ""
                        {
                            if strStartTime != "" && strEndTime != ""
                            {
                                if self.tablePostaJob.userFieldArray[7].value != "Type in"
                                {
                                    return true
                                }
                                else
                                {
                                    UtilityClass.showAlert(str: "Please enter job wages.")
                                    return false
                                }
                            }
                            else
                            {
                                UtilityClass.showAlert(str: "Please select job time(From and To).")
                                return false
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "Please select From and To date.")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please select address.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please select job keywords.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter job description.")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter job title.")
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
