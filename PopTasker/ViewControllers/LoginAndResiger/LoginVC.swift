//
//  LoginVC.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class LoginVC: BaseVC,SWRevealViewControllerDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    
    //MARK: Variable Declaration
    
    @IBOutlet var txtEmail: TextFieldTitle!
    @IBOutlet var txtPassword: TextFieldTitle!
    @IBOutlet var btnForgotPassword: NeueLightButtonGray!
    @IBOutlet var btnSignIn: NeueLightButton!
    @IBOutlet var btnFbIn: NeueLightButton!
    @IBOutlet var btnGoogleIn: NeueLightButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Setup View
        self.setupView()
    }
    
    func setupView()
    {
        txtEmail.validateTextField(type: .Email, minLength: 5, maxLength: 100, alignment: .center, placeHolder: "Type In")
        txtPassword.validateTextField(type: .Password, minLength: 6, maxLength: 100, alignment: .center, placeHolder: "Type In")
        btnForgotPassword.setTitle("Forgot Your Password?", for: .normal)
        
        btnSignIn.setTitle("SIGN IN", for: .normal)
        btnSignIn.setBackgroundColor(type: .signInPurple)
        btnSignIn.addTarget(self, action: #selector(self.btnSignPress), for: .touchUpInside)
        
        btnFbIn.setBackgroundColor(type: .signInFb)
        btnGoogleIn.setBackgroundColor(type: .signInGoogle)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        StatusBarStyle.setStatusBar(style: .default)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        txtEmail.setLeftTitle(title: "Email")
        txtPassword.setLeftTitle(title: "Password")
        
//        txtEmail.text = "jatin@solulab.com"
//        txtPassword.text = "123456"
    }
    
    //MARK: IBActions
    
    @IBAction func btnSignPress()
    {
        if txtEmail.validateTextFiled(validationMesage: .invalidEmail)
        {
            if txtPassword.validateTextFiled(validationMesage: .invalidPassLength)
            {
                AppDel.showHUD()
                print("\(UIDevice.current.identifierForVendor!.uuidString)")
                let param = ["email":txtEmail.text!,"password":txtPassword.text!,"fcm_token":"\(UserDefaults.standard.value(forKey: "fcmToken")!)","device_id":"\(UIDevice.current.identifierForVendor!.uuidString)","device_type":"ios"] as JSONDICTIONARY
                
                print(param)
                ApiManager.shared.postData(url: kLogin, params: param) { (dict) in
                    print(dict)
                    AppDel.hideHUD()
                    
                    setModelDataInUserDefaults(key: "userData", value: dict)
                    setUserDefaultsFor(object: true as AnyObject, with: "isLogin")
                    
                    AppDel.tabbarVC = STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                    AppDel.sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
                    AppDel.window?.rootViewController = SideMenuController(contentViewController: AppDel.tabbarVC,menuViewController: AppDel.sideMenuviewObj)
                    AppDel.window?.makeKeyAndVisible()
                }
            }
        }
    }
    
    @IBAction func btnForgotPassPressed(_ sender: UIButton)
    {
        let registerVc = FORGOTPASS_STORYBOARD.instantiateViewController(withIdentifier: "ForgotPassVC")
        self.navigationController?.pushViewController(registerVc, animated: true)
    }
    @IBAction func btnSignUpPress()
    {
        let registerVc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        let userFieldArray = [
            RegisterUser.init(placeHolder: "First Name", txtValue: "", imgBgName: "Register_First_Name", cellType: CellType.FirstName),
            RegisterUser.init(placeHolder: "Last Name", txtValue: "", imgBgName: "Register_Last_Name", cellType: CellType.LastName),
            RegisterUser.init(placeHolder: "Email", txtValue: "", imgBgName: "Register_Email", cellType: CellType.Email),
            RegisterUser.init(placeHolder: "Password", txtValue: "", imgBgName: "Register_Password", cellType: CellType.Password),
            RegisterUser.init(placeHolder: "Confirm Password", txtValue: "", imgBgName: "Register_Confirm_Password", cellType: CellType.ConfirmPassword),
            ]
        registerVc.arr = userFieldArray
        registerVc.isFromFB = false
        registerVc.fb_token = ""
        self.navigationController?.pushViewController(registerVc, animated: true)
    }
    @IBAction func btnGooglePress(sender:UIButton)
    {
        AppDel.showHUD()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- Google Delegate Method
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {

    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
            AppDel.hideHUD()
        } else {
            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let fullNameArr : [String] = fullName!.components(separatedBy: " ")
            
            // And then to access the individual words:
            
            let firstName : String! = fullNameArr[0]
            let lastName : String! = fullNameArr[1]
           
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
            let email = user.profile.email
            
            let param = ["email":email!,"fcm_token":"\(getUserDefaultsForKey(key: "fcmToken")!)","device_id":"\(UIDevice.current.identifierForVendor!.uuidString)","google_token":idToken!,"firstname":"\(firstName!)","lastname":"\(lastName!)","device_type":"ios"] as JSONDICTIONARY
            
            print(param)
            ApiManager.shared.postData(url: kLoginwithgoogle, params: param, completion: { (dict) in
                print(dict)
                AppDel.hideHUD()
                setModelDataInUserDefaults(key: "userData", value:dict )
                setUserDefaultsFor(object: true as AnyObject, with: "isLogin")
                
                AppDel.tabbarVC = STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                AppDel.sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
                AppDel.window?.rootViewController = SideMenuController(contentViewController: AppDel.tabbarVC,menuViewController: AppDel.sideMenuviewObj)
                AppDel.window?.makeKeyAndVisible()
            })
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        AppDel.hideHUD()
        UtilityClass.showAlert(str: error.localizedDescription)
        
    }
    @IBAction func btnFacebookPress(sender:UIButton)
    {
        let fbLoginManagerTemp : LoginManager = LoginManager()
        fbLoginManagerTemp.logOut()
        let fbLoginManager : LoginManager = LoginManager()
        AppDel.showHUD()
        fbLoginManager.logIn(readPermissions: [ReadPermission.publicProfile,ReadPermission.email,ReadPermission.userBirthday,ReadPermission.userHometown], viewController: self) { (result) in
            AppDel.hideHUD()
            self.getFBUserData()
        }
    }
    
    func getFBUserData()
    {
        let params = ["fields":"name,email,first_name,last_name,gender,picture.type(large),age_range,hometown"]
        let graphRequest = GraphRequest(graphPath: "me", parameters: params)
        AppDel.showHUD()
        graphRequest.start{ (urlResponse, requestResult) in
            switch requestResult
            {
            case .failed(let error):
                print(error)
                AppDel.hideHUD()
                UtilityClass.showAlert(str: "\(error.localizedDescription)")
                break
            case .success(let graphResponse):
                print(requestResult)
                if let dict = graphResponse.dictionaryValue as JSONDictionary?
                {
                    print(dict)
                    let FBDict = dict
                    ApiManager.shared.postData(url: ksocialcheck, params: ["fb_token":"\(dict["id"]!)"], completion: { (dict) in
                        print(dict)
                        let dictData = dict["data"] as! JSONDICTIONARY
                        let isExist = dictData["is_exists"] as! Int
                        if isExist == 1
                        {
                            let param = ["email":"","fcm_token":"\(getUserDefaultsForKey(key: "fcmToken")!)","device_id":"\(UIDevice.current.identifierForVendor!.uuidString)","fb_token":"\(FBDict["id"]!)","firstname":"\(FBDict["first_name"]!)","lastname":"\(FBDict["last_name"]!)","device_type":"ios"] as JSONDICTIONARY
                            
                            print(param)
                            ApiManager.shared.postData(url: kLoginwithfb, params: param, completion: { (dict) in
                                print(dict)
                                AppDel.hideHUD()
                                setModelDataInUserDefaults(key: "userData", value:dict )
                                setUserDefaultsFor(object: true as AnyObject, with: "isLogin")
                                
                                AppDel.tabbarVC = STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                                AppDel.sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
                                AppDel.window?.rootViewController = SideMenuController(contentViewController: AppDel.tabbarVC,menuViewController: AppDel.sideMenuviewObj)
                                AppDel.window?.makeKeyAndVisible()
                            })
                        }
                        else
                        {
                            if let email = FBDict["email"] as? String
                            {
                                let param = ["email":email,"fcm_token":"\(getUserDefaultsForKey(key: "fcmToken")!)","device_id":"\(UIDevice.current.identifierForVendor!.uuidString)","fb_token":"\(FBDict["id"]!)","firstname":"\(FBDict["first_name"]!)","lastname":"\(FBDict["last_name"]!)","device_type":"ios"] as JSONDICTIONARY
                                
                                
                                print(param)
                                ApiManager.shared.postData(url: kLoginwithfb, params: param, completion: { (dict) in
                                    print(dict)
                                    AppDel.hideHUD()
                                    setModelDataInUserDefaults(key: "userData", value:dict )
                                    setUserDefaultsFor(object: true as AnyObject, with: "isLogin")
                                    
                                    AppDel.tabbarVC = STORYBOARD_TAB.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                                    AppDel.sideMenuviewObj = ( MENU_STORYBOARD.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController)!
                                    AppDel.window?.rootViewController = SideMenuController(contentViewController: AppDel.tabbarVC,menuViewController: AppDel.sideMenuviewObj)
                                    AppDel.window?.makeKeyAndVisible()
                                })
                            }
                            else
                            {
                                AppDel.hideHUD()
                                let viewObj = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                                let arr = [
                                    RegisterUser.init(placeHolder: "First Name", txtValue: "\(FBDict["first_name"]!)", imgBgName: "Register_First_Name", cellType: CellType.FirstName),
                                    RegisterUser.init(placeHolder: "Last Name", txtValue: "\(FBDict["last_name"]!)", imgBgName: "Register_Last_Name", cellType: CellType.LastName),
                                    RegisterUser.init(placeHolder: "Email", txtValue: "", imgBgName: "Register_Email", cellType: CellType.Email)
                                    ]
                               
                                viewObj.arr = arr
                                viewObj.isFromFB = true
                                viewObj.fb_token = "\(FBDict["id"]!)"
                                self.navigationController?.pushViewController(viewObj, animated: true)

                            }

                        }
                    })

                }
                break
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
