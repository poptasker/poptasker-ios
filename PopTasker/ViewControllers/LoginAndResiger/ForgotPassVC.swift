//
//  ForgotPassVC.swift
//  PopTasker
//
//  Created by Devendra on 18/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {
    @IBOutlet var txtEmail : TextFieldGray119!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Reset Password"
        self.navigationController?.navigationBar.isHidden = false
        StatusBarStyle.setStatusBar(style: .lightContent)
        txtEmail.textType = TextField.TextFieldType.Email
//        txtEmail.text = "urjasheth91@gmail.com"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        let menu = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 44, height: 44))
        menu.setImage(#imageLiteral(resourceName: "Nvc_Back"), for: .normal)
        let barButtonItem = UIBarButtonItem.init(customView: menu)
        self.navigationItem.leftBarButtonItem = barButtonItem
        menu.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 10)
        menu.addTarget(self, action: #selector(self.buttonBackPress), for: .touchUpInside)

    }
    
    @objc func buttonBackPress()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResetPresss(sender:UIButton)
    {
        if txtEmail.validateTextFiled(validationMesage: .invalidEmail)
        {
            AppDel.showHUD()
            ApiManager.shared.postData(url: kResetPassword, params: ["email":txtEmail.text!]) { (dict) in
                print(dict)
                AppDel.hideHUD()
                self.navigationController?.popViewController(animated: true)
                UtilityClass.showAlert(str: "\(dict["message"]!)")

            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
}
