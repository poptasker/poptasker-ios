//
//  RegisterVC.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class RegisterVC: BaseVC {
    
    //Variable Declaration
    @IBOutlet var tblRegister : RegisterTableView!
    @IBOutlet var btnSignIn: UIButton!
    var isFromFB : Bool! = false
    var fb_token = ""
    var arr : [RegisterUser]!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblRegister.isFromFB = isFromFB
        tblRegister.fb_token = fb_token
        tblRegister.createUserArray(arr: arr)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
