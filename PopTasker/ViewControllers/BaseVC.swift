//
//  BaseVC.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
enum searchType
{
    case Activity
    case Message
    case Applicant
    case ApplicantFromJob
    case Favorite
    case JobSearchList
}

protocol searchTypeDelegate {
    func search(text:String,type:searchType)
    func close(text:String,type:searchType)
}
class BaseVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - Variable Declaration
    //ScrollView Height Constraint
    @IBOutlet var constraintHeight: NSLayoutConstraint!
    var delegate : searchTypeDelegate?
    var searchType : searchType!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setSearchType(type : searchType)
    {
        self.searchType = type
    }
    //MARK: Create SideMenu
    
    func setNavigationMenu(menuButtons: NavigationButtons, type: MenuType) {
        
        switch type {
        case .Menu:
            setButton(type: menuButtons, isLeft: true)
            break
        case .Back:
            setButton(type: menuButtons, isLeft: true)
            break
        case .Serch:
            setButton(type: menuButtons, isLeft: false)
            break
        case .Close:
            setButton(type: menuButtons, isLeft: false)
            break
        case .MenuAndSearch:
            setButton(type: .Menu, isLeft: true)
            setButton(type: .Search, isLeft: false)
            break
        case .BackAndSearch:
            setButton(type: .Back, isLeft: true)
            setButton(type: .Search, isLeft: false)
            break
        case .BackAndAdd:
            setButton(type: .Back, isLeft: true)
            setButton(type: .Add, isLeft: false)
            break
        case .MapSearch:
            setButton(type: .Menu, isLeft: true)
            setMapSearchRightView()
            break
        case .Reset:
            setButton(type: .Back, isLeft: true)
            setButton(type: .Reset, isLeft: false)
            break
        case .BackToRoot:
            setButton(type: .BackToRoot, isLeft: true)
        }
    }
    func setMapSearchRightView()
    {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 97, height: 30))
        let btn1 = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
        btn1.setImage(#imageLiteral(resourceName: "Nav_filter"), for: .normal)
        btn1.addTarget(self, action: #selector(self.FitlerPress), for: .touchUpInside)
        view.addSubview(btn1)
        
        let btn2 = UIButton.init(frame: CGRect.init(x: 30, y: 0, width: 30, height: 30))
        btn2.setImage(#imageLiteral(resourceName: "Nvc_map"), for: .normal)
        btn2.addTarget(self, action: #selector(self.GotoMapPress), for: .touchUpInside)
        view.addSubview(btn2)
        
        let btn3 = UIButton.init(frame: CGRect.init(x: 62, y: 0, width: 35, height: 30))
        btn3.setImage(#imageLiteral(resourceName: "Nvc_Search"), for: .normal)
        btn3.addTarget(self, action: #selector(self.MapsearchButtonPress), for: .touchUpInside)
        view.addSubview(btn3)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: view)
    }
    func setButton(type: NavigationButtons, isLeft: Bool)
    {
        let menu = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 44, height: 44))
        if type == .Reset
        {
            menu.setTitle("Reset", for: .normal)
        }
        else
        {
            menu.setImage(type.image, for: .normal)
        }
        let barButtonItem = UIBarButtonItem.init(customView: menu)
        if isLeft
        {
            self.navigationItem.leftBarButtonItem = barButtonItem
            menu.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 10)
        }
        else
        {
            self.navigationItem.rightBarButtonItem = barButtonItem
            menu.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: -10)
        }
        
        switch type
        {
        case .Menu:
            menu.addTarget(self, action: #selector(self.buttonMenuPress), for: .touchUpInside)
            break
        case .Back:

            menu.addTarget(self, action: #selector(self.buttonBackPress), for: .touchUpInside)
            break
        case .BackToRoot:

            menu.addTarget(self, action: #selector(self.buttonBackToRootPress), for: .touchUpInside)
            break
        case .Add:
            menu.addTarget(self, action: #selector(self.addButtonPress), for: .touchUpInside)
            break
        case .Search:
            menu.addTarget(self, action: #selector(self.searchButtonPress), for: .touchUpInside)
            break
        case .Close:
            menu.addTarget(self, action: #selector(self.closeButtonPress), for: .touchUpInside)
            break
        case .CloseMap:
            menu.addTarget(self, action: #selector(self.closeMapButtonPress), for: .touchUpInside)
            break
//        case .Reset:
//            menu.addTarget(self, action: #selector(self.closeMapButtonPress), for: .touchUpInside)
//            break
        default:
            break
        }
    }
    
    @objc func buttonMenuPress()
    {
//        SJSwiftSideMenuController.toggleLeftSideMenu()
        sideMenuController?.revealMenu()
    
    }
    @objc func buttonBackPress()
    {
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            AppDel.tabbarVC.tabBar.isHidden = false
            nvc.popViewController(animated: true)
        }
    }
    @objc func buttonBackToRootPress()
    {
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            AppDel.tabbarVC.tabBar.isHidden = false
            nvc.popToRootViewController(animated: true)
        }
    }
    @objc func addButtonPress()
    {
        let PayMentDetailsVC = PAYMENT_STORYBOARD.instantiateViewController(withIdentifier: "PayMentDetailsVC") as! PayMentDetailsVC
        self.pushViewController(viewcontroller: PayMentDetailsVC)
    }
    @objc func searchButtonPress()
    {
        let textfield = TextFieldNavigation.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width - 60, height: 40))
        textfield.attributedPlaceholder = NSAttributedString.init(string: "Search", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray,NSAttributedString.Key.font:Font.setFont(name: "HelveticaNeueLTPro-Lt", size: 24)!])
        textfield.delegate = self
        textfield.textColor = UIColor.white
        textfield.font = Font.setHelvaticaNeueLTPro(font: FontType.Light, size: 24)
        self.navigationItem.titleView = textfield
        textfield.becomeFirstResponder()
        self.setButton(type: .Close, isLeft: false)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            delegate?.search(text: updatedText, type: self.searchType)
        }
        
//        let txt = textField.text!

        return true
    }
//    func textFieldDidEndEditing(_ textField: UITextField)
//    {
//        let txt = textField.text!
//        delegate?.search(text: txt, type: self.searchType)
////        self.closeButtonPress()
//    }
    @objc func GotoMapPress()
    {
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            nvc.popViewController(animated: true)
        }
    }
    @objc func FitlerPress()
    {
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            let filterObj = FILTER_STORYBOARD.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
            
            nvc.pushViewController(filterObj, animated: true)
        }
    }
    @objc func MapsearchButtonPress()
    {
        let textfield = TextFieldNavigation.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width - 60, height: 40))
        textfield.attributedPlaceholder = NSAttributedString.init(string: "Search", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray,NSAttributedString.Key.font:Font.setFont(name: "HelveticaNeueLTPro-Lt", size: 24)!])
        textfield.textColor = UIColor.white
        textfield.delegate = self

        textfield.font = Font.setHelvaticaNeueLTPro(font: FontType.Light, size: 24)
        self.navigationItem.titleView = textfield
        self.setButton(type: .CloseMap, isLeft: false)
    }
    @objc func closeButtonPress()
    {
        self.navigationItem.titleView = nil
        self.setButton(type: .Search, isLeft: false)
        delegate?.close(text: "", type: self.searchType)
    }
    @objc func closeMapButtonPress()
    {
        self.navigationItem.titleView = nil
        delegate?.close(text: "", type: self.searchType)
        setMapSearchRightView()
    }
    func pushViewController(viewcontroller: UIViewController)
    {
        if let nvc = AppDel.tabbarVC.selectedViewController as? UINavigationController
        {
            AppDel.tabbarVC.tabBar.isHidden = true
            nvc.pushViewController(viewcontroller, animated: true)
        }
    }
}
import UIKit

// Provides access to the side menu controller
public extension UIViewController {
    
    /// Access the nearest ancestor view controller  hierarchy that is a side menu controller.
    public var sideMenuController: SideMenuController? {
        return findSideMenuController(from: self)
    }
    
    fileprivate func findSideMenuController(from viewController: UIViewController) -> SideMenuController? {
        var sourceViewController: UIViewController? = viewController
        repeat {
            sourceViewController = sourceViewController?.parent
            if let sideMenuController = sourceViewController as? SideMenuController {
                return sideMenuController
            }
        } while (sourceViewController != nil)
        return nil
    }
}
