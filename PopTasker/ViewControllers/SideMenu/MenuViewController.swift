//
//  MenuViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 06/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class MenuViewController: BaseVC
{
    @IBOutlet var tblMenu : MenuTableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tblMenu.blockTableViewDidSelectHeader = {(str)
            -> Void in
            self.sideMenuController?.hideMenu()

            let MyProfileVC = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
            self.pushViewController(viewcontroller: MyProfileVC)
        }
        tblMenu.blockTableViewDidSelectAtIndexPath = { (indexpath, str) -> Void in
        
            self.sideMenuController?.hideMenu()

            switch indexpath.row
            {
            case 0:
                let TransactionsVC = TRANSACTION_STORYBOARD.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
                self.pushViewController(viewcontroller: TransactionsVC)
                break
            case 1:
                let FavoriteListVC = FAVORITE_STORYBOARD.instantiateViewController(withIdentifier: "FavoriteListVC")
                self.pushViewController(viewcontroller: FavoriteListVC)
                break
            case 2:
                let ApplicantVC = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantVC")
                self.pushViewController(viewcontroller: ApplicantVC)
                break
            case 3:
                let ApplicantVC = SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "SettingsViewController")
                self.pushViewController(viewcontroller: ApplicantVC)
                break
            case 4:
                let alertController = UIAlertController.init(title: kAppName, message: "Are you sure you want to logout?", preferredStyle: .alert)
                let alertActionOK = UIAlertAction.init(title: "YES", style: .default) { (action) in
                    
                    setUserDefaultsFor(object: false as AnyObject, with: "isLogin")
//                    removeUserDefaultsFor(key: "userData")
//                    AppDel.setRootView()
                   
                    print("\(UIDevice.current.identifierForVendor!.uuidString)")
                    AppDel.showHUD()
                    ApiManager.shared.postDataWithHeader(url: kLogout, params: ["device_id":"\(UIDevice.current.identifierForVendor!.uuidString)"], completion: { (dict) in
                        print(dict)
                        AppDel.hideHUD()
                        setUserDefaultsFor(object: false as AnyObject, with: "isLogin")
                        removeUserDefaultsFor(key: "userData")
                        removeUserDefaultsFor(key: "userProfile")

                        AppDel.setRootView()
                    })
                }
                
                alertController.addAction(alertActionOK)
                let alertActionNO = UIAlertAction.init(title: "NO", style: .default, handler: nil)
                alertController.addAction(alertActionNO)
                AppDel.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                
                break
            default:
                break
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblMenu.reloadData()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
