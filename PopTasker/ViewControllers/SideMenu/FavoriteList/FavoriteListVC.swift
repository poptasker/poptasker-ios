//
//  FavoriteListVC.swift
//  PopTasker
//
//  Created by Rohan on 31/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class FavoriteListVC: BaseVC,searchTypeDelegate
{
    @IBOutlet var tblFav : FavoriteListTable!
    var arr = [JSONDICTIONARY]()
    var arrFilter = [JSONDICTIONARY]()
    var imgView: UIImageView
    {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "Favorites")
        imgV.alpha = 0.4
        return imgV
    }
    
    override func viewDidLoad() {
        self.setNavigationMenu(menuButtons: .Back, type: .BackAndSearch)
        super.viewDidLoad()
        self.title = "Favorites"
        self.setSearchType(type: .Favorite)
        self.delegate = self
//        AppDel.window?.addSubview(imgView)
        getFavoriteList()
        tblFav.blockTableViewFavorite = {(index) -> Void in
            self.arr = Array()
            
            self.getFavoriteList()
        }
        tblFav.blockTableViewDidSelectAtIndexPath = { (indexpath, str) -> Void in
            let viewObj = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantProfileViewController") as! ApplicantProfileViewController
            viewObj.dict = self.tblFav.arrFav[indexpath.row] as JSONDictionary
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
    }
    
    //MARK:- search delegate
    func search(text: String, type: searchType)
    {
        if type == .Favorite
        {
            self.arrFilter = self.arr
            
            if text != ""
            {
                let filter = arrFilter.filter { (dict) -> Bool in
                    if let name = dict["name"] as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                print(filter) // ["b": 42]
                arrFilter = filter
            }
            self.tblFav.arrFav = self.arrFilter
            self.tblFav.reloadData()
        }
    }
    
    func close(text: String, type: searchType)
    {
        self.arrFilter = self.arr
        self.tblFav.arrFav = self.arrFilter
        self.tblFav.reloadData()
    }
    func getFavoriteList()
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kGetFavoriteList, params: [:]) { (dict) in
            print(dict)
            let data = dict["data"] as! JSONDICTIONARY
            let listing = data["listing"] as! [JSONDICTIONARY]
            if listing.count == 0
            {
                UtilityClass.showToast(view: self.view, message: "No record found.")
            }
            for var dict in listing
            {
                dict["name"] = "\(dict["firstname"]!) \(dict["lastname"]!)"
                self.arr.append(dict)
            }
            self.tblFav.arrFav = self.arr
            self.tblFav.reloadData()
            AppDel.hideHUD()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
