//
//  PrivacyPolicyViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: BaseVC, WKUIDelegate {

    var webView: WKWebView!
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Privacy"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        let url = URL(string:"\(BASE_URL)privacy_policy")
        let requestObj = URLRequest(url: url! as URL)
        webView.load(requestObj)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
