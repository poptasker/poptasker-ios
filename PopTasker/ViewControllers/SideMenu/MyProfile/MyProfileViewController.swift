//
//  MyProfileViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import MBProgressHUD
import GrowingTextView
class MyProfileViewController: BaseVC,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GrowingTextViewDelegate
{
    @IBOutlet var txtSummary: GrowingTextView!
    @IBOutlet var txtEducation: GrowingTextView!
    @IBOutlet var txtPssions: GrowingTextView!
    @IBOutlet var txtSkills: GrowingTextView!
    @IBOutlet var txtPhoneNumber: GrowingTextView!
    @IBOutlet var txtHomeAddress: GrowingTextView!
    @IBOutlet var txtLinkedin: GrowingTextView!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var tblProfile : UITableView!
    var imagePicker = UIImagePickerController()
    var arrReviews = [JSONDictionary]()
    @IBOutlet var cellHeader : UITableViewCell!
    @IBOutlet var cellSave : UITableViewCell!
    var isPicSelected : Bool! = false
    var finalCount : Int! = 0
    private var lastContentOffset: CGFloat = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Profile"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        self.imgView.cornerRadius = self.imgView.frame.width/2
        self.imgView.setBorder(width: 2.0)
        self.imgView.layer.borderColor = UIColor.setPurpalColor().cgColor
        imagePicker.delegate = self
        getProfile()
    }

    func getProfile()
    {
        AppDel.showHUD()
        
        ApiManager.shared.postDataWithHeader(url: kUserProfile, params: [:]){ (dict) in
            
            print(dict)
            let data = dict["data"] as! JSONDictionary
            setModelDataInUserDefaults(key: "userProfile", value: data)
            self.lblName.text = "\(data["firstname"]!) \(data["lastname"]!)"

            self.txtSummary.text = "\(data["summary"]!)"
            self.txtEducation.text = "\(data["education"]!)"
            self.txtPssions.text = "\(data["passions"]!)"
            self.txtSkills.text = "\(data["skills"]!)"
            self.txtPhoneNumber.text = "\(data["phone"]!)"
            self.txtHomeAddress.text = "\(data["address"]!)"
            self.txtLinkedin.text = "\(data["linkedin"]!)"
            
            if data["profilepic"] as? String != ""
            {
                self.imgView.sd_setImage(with: URL.init(string: "\(data["profilepic"]!)"), placeholderImage: #imageLiteral(resourceName: "AppLogo"), options: [], completed: nil)
            }
            else
            {
                let first = self.lblName.text?.first
                self.imgView.image = LetterImageGenerator.imageWith(name: "\(first!)")
            }
           
            self.getUserReview(Profiledict: data)
        }
    }

    func getUserReview(Profiledict : JSONDictionary)
    {
//        let view = UIView.init(frame: self.view.frame)
//        view.backgroundColor = UIColor.white
//        self.view.addSubview(view)
        let param = ["user_id":"\(Profiledict["user_id"]!)"] as JSONDictionary
        ApiManager.shared.postDataWithHeader(url: kGetReviews, params: param) { (dictReview) in
            AppDel.hideHUD()
//            view.removeFromSuperview()
            
            print(dictReview)
            let data = dictReview["data"] as! JSONDictionary
            self.arrReviews = data["listing"] as! [JSONDictionary]
            var count = 0
            for view in self.cellHeader.contentView.subviews
            {
                if (view is UITextView)
                {
                    count = count + self.lineCount(textView: view as! UITextView)
                }
            }
            print(count)
            self.finalCount = count - 8
            if self.finalCount < 0
            {
                self.finalCount = 0
            }
           
            self.tblProfile.reloadData()
//            self.tblProfile.createUserArray(dict: Profiledict , arr: data["listing"] as! [JSONDictionary])
        }
    }
    
    //MARK:- DATASOURCE AND DELEGATES
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return arrReviews.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
                cellHeader.selectionStyle = .none
            return cellHeader
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileReviewsTableViewCell") as! MyProfileReviewsTableViewCell
            cell.selectionStyle = .none
            let dict = arrReviews[indexPath.row] as JSONDictionary
            cell.lblName.text = "\(dict["firstname"]!) \(dict["lastname"]!)"
            cell.lblDate.text = "\(dict["review_date"]!)"
            cell.lblComment.text = "\(dict["comments"]!)"
            cell.rateView.rating = Float("\(dict["rating"]!)")!
            let first = "\(dict["firstname"]!)".first
            let img = LetterImageGenerator.imageWith(name: "\(first!)")
            if "\(dict["profilepic"]!)" != ""
            {
                cell.imgView.sd_setImage(with: URL.init(string: "\(dict["profilepic"]!)"), completed: nil)
            }
            else
            {
                cell.imgView.image = img
            }
            return cell
        }
        else
        {
            cellSave.selectionStyle = .none
            return cellSave
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            print(CGFloat(750 + (15 * finalCount)))
            return CGFloat(750 + (15 * finalCount))
        }
        else if indexPath.section == 1
        {
            return getProportionalHeight(height: 133.7)
        }
        else
        {
            return 112.7
        }
    }
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return getProportionalHeight(height: 0)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return getProportionalHeight(height: 0)
    }
   
 
    //MARK:- DID SELECT
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    //MARK:- Textview delegate
    func textViewDidEndEditing(_ textView: UITextView)
    {
        var count = 0
        for view in cellHeader.contentView.subviews {
            if (view is UITextView) {
                count = count + lineCount(textView: view as! UITextView)            }
        }
        print(count)
        finalCount = count - 8
        if finalCount < 0
        {
            finalCount = 0
        }
        DispatchQueue.main.async
        {
            self.tblProfile.reloadSections(IndexSet.init(integer: 0), with: .none)
        }
    }
    //MARK:- Scrollview delegate

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        if UIApplication.shared.isKeyboardPresented
        {
            self.view.endEditing(true)
            var count = 0
            for view in cellHeader.contentView.subviews
            {
                if (view is UITextView)
                {
                    count = count + lineCount(textView: view as! UITextView)
                }
            }
            print(count)
            finalCount = count - 8
            if finalCount < 0
            {
                finalCount = 0
            }
            DispatchQueue.main.async
            {
                self.tblProfile.reloadSections(IndexSet.init(integer: 0), with: .none)
            }
        }
    }
    
    func lineCount(textView : UITextView) -> Int {
        let numLines = (textView.contentSize.height / (textView.font?.lineHeight)!)
        
        print(Int(numLines))
        var cnt = Int(numLines) - 1
        if cnt == 0
        {
            cnt = 1
        }
        return cnt
    }
    
    @IBAction func selectImage(sender:UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom
        {
        case .pad:
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }

        AppDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnSavePress(sender:UIButton)
    {
        if validation()
        {
            let str = "\(self.lblName.text!)"
            let arr = str.components(separatedBy: " ")

            let param = ["firstname":"\(arr[0])",
                "lastname":"\(arr[1])",
                "summary":"\(self.txtSummary.text!)",
                "education":"\(self.txtEducation.text!)",
                "skills":"\(self.txtPssions.text!)",
                "passions":"\(self.txtSkills.text!)",
                "phone":"\(self.txtPhoneNumber.text!)",
                "address":"\(self.txtHomeAddress.text!)",
                "linkedin":"\(self.txtLinkedin.text!)"]
            
            print(param)
            AppDel.showHUD()
            
            if isPicSelected
            {
                ApiManager.shared.PostWithSingleImage(url: kUserEdit, imageData: self.imgView.image!.pngData()!, parameters: param)
                { (dict) in
                    print(dict)
                    setModelDataInUserDefaults(key: "userProfile", value: dict["data"] as! JSONDICTIONARY)

                    AppDel.hideHUD()
                    self.navigationController?.popViewController(animated: true)
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                }
            }
            else
            {
                ApiManager.shared.postDataWithHeader(url: kUserEdit, params: param) { (dict) in
                    print(dict)
                    setModelDataInUserDefaults(key: "userProfile", value: dict["data"] as! JSONDICTIONARY)

                    AppDel.hideHUD()
                    self.navigationController?.popViewController(animated: true)

                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                }

            }
        }
    }
    
    func openCamera()
    {
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            AppDel.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            AppDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        AppDel.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if (info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage) != nil {
            // imageViewPic.contentMode = .scaleToFill
            self.imgView.image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
            isPicSelected = true
//            self.tblProfile.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func validation() -> Bool {
        if self.txtSummary.text?.count != 0
        {
            if self.txtEducation.text?.count != 0
            {
                if self.txtPssions.text?.count != 0
                {
                    if self.txtSkills.text?.count != 0
                    {
                        if self.txtPhoneNumber.text?.count != 0
                        {
                            if self.txtHomeAddress.text?.count != 0
                            {
                                if self.txtLinkedin.text?.count != 0
                                {
                                    return true
                                }
                                else
                                {
                                    UtilityClass.showAlert(str: "Please provide linkedIn profile url.")
                                    return false
                                }
                            }
                            else
                            {
                                UtilityClass.showAlert(str: "Please provide home address.")
                                return false
                            }
                        }
                        else
                        {
                            UtilityClass.showAlert(str: "Please provide phone number.")
                            return false
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: "Please provide skills.")
                        return false
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: "Please provide passion.")
                    return false
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please provide education.")
                return false
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please provide personal summary.")
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
extension UIApplication {
    /// Checks if view hierarchy of application contains `UIRemoteKeyboardWindow` if it does, keyboard is presented
    var isKeyboardPresented: Bool {
        if let keyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow"), self.windows.contains(where: { $0.isKind(of: keyboardWindowClass) }) {
            return true
        } else {
            return false
        }
    }
}
