//
//  ContactUsViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import MessageUI
class ContactUsViewController: BaseVC,MFMailComposeViewControllerDelegate
{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contact Us"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
    }

    @IBAction func btnCallPress(sender:UIButton)
    {
        if let url = URL(string: "tel://+19252396838"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *)
            {
                UIApplication.shared.open(url)
            }
            else
            {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnMailPress(sender:UIButton)
    {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail()
    }
    func sendEmail()
    {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["poptasker@gmail.com"])
//        composeVC.setSubject("Hello!")
//        composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
