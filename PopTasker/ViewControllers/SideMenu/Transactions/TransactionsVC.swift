//
//  TransactionsVC.swift
//  PopTasker
//
//  Created by Urja_Macbook on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
protocol transactionProtocol
{
    func checkEarn(isEarn: Bool)
}
class TransactionsVC: BaseVC
{
    @IBOutlet var btnEarn : UIButton!
    @IBOutlet var btnSpend : UIButton!
    @IBOutlet var viewSegment : UIView!
    @IBOutlet var tbl_Transaction : TransactionsTableView!
    var spentArray = [TransactionModel]()
    var EarnArray = [TransactionModel]()
    var isNextPage : Bool! = false
    var page : Int! = 1
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Transactions"
        self.setNavigationMenu(menuButtons: .Add, type: .BackAndAdd)
        
        self.viewSegment.layer.borderColor = UIColor.setGrayLignColor().cgColor
        self.viewSegment.layer.borderWidth = 0.7
        self.btnEarn.layer.borderColor = UIColor.setGrayLignColor().cgColor
        self.btnEarn.layer.borderWidth = 0.7
        self.btnEarn.isSelected = true
        self.btnSpend.isSelected = false
        btnEarn.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        btnSpend.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        tbl_Transaction.createUserArray(transaction: EarnArray)
        btnEarnTapped(sender: UIButton())
        self.tbl_Transaction.addInfiniteScrolling {
            if self.isNextPage
            {
                self.page = self.page + 1
                if self.btnEarn.isSelected
                {
                    self.btnEarnTapped(sender: UIButton())
                }
                else
                {
                    self.btnSpendTapped(sender: UIButton())
                }
//                self.getActivity(status: "\(self.arrFilter[(index.row)].lowercased())")
            }
            else
            {
                self.hideInfiniteScroll()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
    }
    func hideInfiniteScroll() {
        self.tbl_Transaction.infiniteScrollingView.stopAnimating()
    }
    @IBAction func btnEarnTapped(sender:UIButton)
    {
        let selected = self.btnEarn.isSelected
        if selected == false
        {
            self.btnEarn.isSelected = !selected
            self.btnSpend.isSelected = selected
        }
        self.page = 1
        self.EarnArray = Array()
        getTransactionHistory(type: "earned")
    }
    @IBAction func btnSpendTapped(sender:UIButton)
    {
        let selected = self.btnSpend.isSelected
        if selected == false
        {
            self.btnEarn.isSelected = selected
            self.btnSpend.isSelected = !selected
        }
        self.page = 1
        self.spentArray = Array()
        getTransactionHistory(type: "spend")

    }
    func getTransactionHistory(type:String!)
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kTransactions, params: ["type":type!,"page":self.page]) { (dict) in
            print(dict)
            let data = dict["data"] as! JSONDICTIONARY
            if Int("\(data["has_page"]!)") == 1
            {
                self.isNextPage = true
            }
            else
                
            {
                self.isNextPage = false
            }
            let listing = data["listing"] as! [JSONDICTIONARY]
            if listing.count == 0
            {
                UtilityClass.showToast(view: self.view, message: "No transaction found.")
            }
            AppDel.hideHUD()
            if type == "earned"
            {
                for userData in listing
                {
                    let transaction = TransactionModel.init(cellType: .Earn, titleLable: "\(userData["title"]!)", date: "\(userData["start_date"]!) \(userData["start_time"]!)", cost: "$\(userData["wage"]!)", address: "\(userData["address"]!)", timeAgo: "\(userData["working_hours"]!) hours", description: "\(userData["description"]!)")
                    self.EarnArray.append(transaction)
                }
                self.tbl_Transaction.createUserArray(transaction: self.EarnArray)
            }
            else
            {
                for userData in listing
                {
                    let transaction = TransactionModel.init(cellType: .Spent, titleLable: "\(userData["title"]!)", date: "\(userData["start_date"]!) \(userData["start_time"]!)", cost: "$\(userData["wage"]!)", address: "\(userData["address"]!)", timeAgo: "\(userData["working_hours"]!) hours", description: "\(userData["description"]!)")
                    self.spentArray.append(transaction)
                }
                self.tbl_Transaction.createUserArray(transaction: self.spentArray)
            }
            self.hideInfiniteScroll()
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
