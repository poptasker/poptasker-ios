//
//  SettingsViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class SettingsViewController: BaseVC {

    @IBOutlet var tblSettings : SettingsTableView!
    var imgView: UIImageView {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "settings")
        imgV.alpha = 0.4
        return imgV
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Settings"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
//        AppDel.window?.addSubview(imgView)
        tblSettings.blockTableViewDidSelectAtIndexPath = { (indexpath, str) -> Void in
                        
            switch indexpath.row
            {
            case 0:
                let HelpVC = SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
                self.pushViewController(viewcontroller: HelpVC)
                break
            case 1:
                let ContactUsVC = SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "ContactUsViewController")
                self.pushViewController(viewcontroller: ContactUsVC)
                break
            case 2:
                let AboutUsVC = SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "AboutUsViewController")
                self.pushViewController(viewcontroller: AboutUsVC)
                break
            case 3:
                let PrivacyPolicyVC = SETTINGS_STORYBOARD.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")
                self.pushViewController(viewcontroller: PrivacyPolicyVC)
                break
            default:
                break
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
