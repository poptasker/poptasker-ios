//
//  ApplicantsListVC.swift
//  PopTasker
//
//  Created by Rohan on 31/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantsListVC: BaseVC,searchTypeDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var tblObj : ApplicantListTable!
    var isNextPage : Bool! = false
    var page : Int! = 1
    var taskId : String! = ""
    var arr = [JSONDICTIONARY]()
    var arrFilter = [JSONDICTIONARY]()
    @IBOutlet var lblTaskName : UILabel!
    var arrCollectionFilter = [String]()
    @IBOutlet var collectionView : UICollectionView!
    var imgView: UIImageView
    {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = UIImage.init(named: "ApplicantsList")
        imgV.alpha = 0.4
        return imgV
    }
    
    override func viewDidLoad()
    {
        self.setNavigationMenu(menuButtons: .Back, type: .BackAndSearch)
        self.setSearchType(type: .ApplicantFromJob)
        super.viewDidLoad()
        self.title = "Applicants"
        self.delegate = self
        print(taskId!)
        
        collectionView.register(UINib(nibName: "ApplicantListCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ApplicantListCollectionCell")
        
        arrCollectionFilter = ["Date","Rating","# of endorsements","# of jobs completed"]
        tblObj.blockTableViewDidSelectAtIndexPath = { (indexpath, dict,mainDict) -> Void in
            let viewObj = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantProfileViewController") as! ApplicantProfileViewController
            viewObj.dict = dict as JSONDictionary
            viewObj.isFromApplicant = true
            viewObj.isAwarded = Int("\(mainDict["is_awarded"]!)")
            viewObj.isFromApplicant = true
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        tblObj.blockTableViewFavorite = {(index) -> Void in
            self.page = 1
            self.arr = Array()
            self.getapplicantsfromjob(id: self.taskId!)
        }
        self.tblObj.addInfiniteScrolling {
            if self.isNextPage
            {
                self.page = self.page + 1
                
                self.getapplicantsfromjob(id: self.taskId!)
            }
            else
            {
                self.hideInfiniteScroll()
            }
        }
//        AppDel.window?.addSubview(imgView)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
        self.arr = Array()
        getapplicantsfromjob(id: taskId!)
    }
 
    func getapplicantsfromjob(id:String!)
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kgetapplicantsfromjob, params: ["task_id":id!,"page":1]) { (dict) in
            print(dict)
            
            let data = dict["data"] as! JSONDICTIONARY
            if let taskName = data["task"] as? String
            {
                self.lblTaskName.text = "Task : \(taskName)"
            }
            if Int("\(data["has_page"]!)") == 1
            {
                self.isNextPage = true
            }
            else
            {
                self.isNextPage = false
            }
            let arrListing = data["listing"] as! [JSONDICTIONARY]
            if arrListing.count == 0
            {
                UtilityClass.showToast(view: self.view, message: "Applicants not found.")
            }
            for var dict in arrListing
            {
                dict["name"] = "\(dict["firstname"]!) \(dict["lastname"]!)"
                self.arr.append(dict)
            }
            self.tblObj.mainDict = data
            self.tblObj.arrApplicantList = self.arr
            self.tblObj.createUserArray()
            AppDel.hideHUD()
        }
    }
    //MARK:- search delegate
    func search(text: String, type: searchType) {
        if type == .ApplicantFromJob
        {
            self.arrFilter = self.arr
            
            if text != ""
            {
                let filter = arrFilter.filter { (dict) -> Bool in
                    if let name = dict["name"] as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                print(filter) // ["b": 42]
                arrFilter = filter
            }
            self.tblObj.arrApplicantList = self.arrFilter
            self.tblObj.createUserArray()
        }
    }
    func close(text: String, type: searchType)
    {
        self.arrFilter = self.arr
        self.tblObj.arrApplicantList = self.arrFilter
        self.tblObj.reloadData()
    }
    func hideInfiniteScroll()
    {
        self.tblObj.infiniteScrollingView.stopAnimating()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let str = self.arrCollectionFilter[indexPath.row]
        let width = str.width(withConstrainedHeight: 40, font: Font.setFont(name: "HelveticaNeueLTPro-Lt", size: 16)!)
        return CGSize.init(width: width + 10, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrCollectionFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ApplicantListCollectionCell", for: indexPath as IndexPath) as! ApplicantListCollectionCell
        cell.isSelected = false
        
        cell.lblName.text = self.arrCollectionFilter[indexPath.item]
        
        if indexPath.row == 0
        {
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right) //Add this line
            cell.isSelected = true
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! ApplicantListCollectionCell
        cell.imgView.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ApplicantListCollectionCell
        var order = ""
        cell.imgView.isHidden = false
        if cell.isDes
        {
            order = "dec"
            cell.isDes = false
            cell.imgView.image = UIImage.init(named: "upline")
        }
        else
        {
            order = "asc"
            cell.isDes = true
            cell.imgView.image = UIImage.init(named: "downline")
        }
        print(cell.lblName.text!)
        if cell.lblName.text! == "Date"
        {
            var array2 = [JSONDICTIONARY]()
            if cell.isDes
            {
                array2 = arr.sorted(by:{let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm"
                    return formatter.date(from:$0["applicationdate"] as! String)! > formatter.date(from:$1["applicationdate"] as! String)!})
            }
           else
            {
                array2 = arr.sorted(by:{let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm"
                    return formatter.date(from:$0["applicationdate"] as! String)! < formatter.date(from:$1["applicationdate"] as! String)!})
            }
            
            print(array2)
            arr = array2
            arrFilter = array2
            self.tblObj.arrApplicantList = arrFilter
            self.tblObj.createUserArray()
        }
        else if cell.lblName.text! == "Rating"
        {
            var array2 = [JSONDICTIONARY]()
            if cell.isDes
            {
                array2 = arr.sorted{($1["ratings"] as? Double ?? 0) > ($0["ratings"] as? Double ?? 0)}
            }
            else
            {
             
                array2 = arr.sorted{($1["ratings"] as? Double ?? 0) < ($0["ratings"] as? Double ?? 0)}
            }
            
            print(array2)
            arr = array2
            arrFilter = array2
            self.tblObj.arrApplicantList = arrFilter
            self.tblObj.createUserArray()
        }
        else if cell.lblName.text! == "# of endorsements"
        {
            var array2 = [JSONDICTIONARY]()
            if cell.isDes
            {
                array2 = arr.sorted{($1["endorsements"] as? Int ?? 0) > ($0["endorsements"] as? Int ?? 0)}
            }
            else
            {
                
                array2 = arr.sorted{($1["endorsements"] as? Int ?? 0) < ($0["endorsements"] as? Int ?? 0)}
            }
            
            print(array2)
            arr = array2
            arrFilter = array2
            self.tblObj.arrApplicantList = arrFilter
            self.tblObj.createUserArray()
        }
        else if cell.lblName.text! == "# of jobs completed"
        {
            var array2 = [JSONDICTIONARY]()
            if cell.isDes
            {
                array2 = arr.sorted{($1["completedtasks"] as? Int ?? 0) > ($0["completedtasks"] as? Int ?? 0)}
            }
            else
            {
                
                array2 = arr.sorted{($1["completedtasks"] as? Int ?? 0) < ($0["completedtasks"] as? Int ?? 0)}
            }
            
            print(array2)
            arr = array2
            arrFilter = array2
            self.tblObj.arrApplicantList = arrFilter
            self.tblObj.createUserArray()
        }
        print(order)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
