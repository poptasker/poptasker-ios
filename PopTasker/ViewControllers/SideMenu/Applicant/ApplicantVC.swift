//
//  ApplicantVC.swift
//  PopTasker
//
//  Created by Urja_Macbook on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantVC: BaseVC,searchTypeDelegate
{
    @IBOutlet var tblApplicant : ApplicantTableView!
    var isNextPage : Bool! = false
    var page : Int! = 1
    var arr = [JSONDICTIONARY]()
    var arrFilter = [JSONDICTIONARY]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Applicants"
        self.setNavigationMenu(menuButtons: .Back, type: .BackAndSearch)
        self.setSearchType(type: .Applicant)
        self.delegate = self
        getApplicantsList()
        tblApplicant.blockTableViewDidSelectAtIndexPath = { (indexpath, dict) -> Void in
            let applicantsListVC = APPLICANT_STORYBOARD.instantiateViewController(withIdentifier: "ApplicantsListVC") as! ApplicantsListVC
            applicantsListVC.taskId = "\(dict["task_id"]!)"
            self.navigationController?.pushViewController(applicantsListVC, animated: true)
        }
        self.tblApplicant.addInfiniteScrolling {
            if self.isNextPage
            {
                self.page = self.page + 1
              
                self.getApplicantsList()
            }
            else
            {
                self.hideInfiniteScroll()
            }
        }
    }
    func hideInfiniteScroll() {
        self.tblApplicant.infiniteScrollingView.stopAnimating()
    }
    func getApplicantsList()
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kGetapplications, params: ["page":self.page])
        { (dict) in
            print(dict)
            let dataDict = dict["data"] as! JSONDICTIONARY

            if Int("\(dataDict["has_page"]!)") == 1
            {
                self.isNextPage = true
            }
            else
                
            {
                self.isNextPage = false
            }
            AppDel.hideHUD()
            let listingArr = dataDict["listing"] as! [JSONDICTIONARY]
//            let arr = [JSONDICTIONARY]()
            for dict in listingArr
            {
                self.arr.append(dict)
            }
            self.tblApplicant.arrApplicant = self.arr
            self.hideInfiniteScroll()
            self.tblApplicant.reloadTable()
        }
    }
    
    //MARK:- search delegate
    func search(text: String, type: searchType)
    {
        if type == .Applicant
        {
            self.arrFilter = self.arr
            
            if text != ""
            {
                let filter = arrFilter.filter { (dict) -> Bool in
                    if let name = dict["title"] as? String
                    {
                        return name.lowercased().range(of:text.lowercased()) != nil
                    }
                    return false
                }
                print(filter) // ["b": 42]
                arrFilter = filter
                
            }
            self.tblApplicant.arrApplicant = self.arrFilter
            self.tblApplicant.reloadTable()
        }
    }
    func close(text: String, type: searchType) {
        self.arrFilter = self.arr
        self.tblApplicant.arrApplicant = self.arrFilter
        self.tblApplicant.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppDel.tabbarVC.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
