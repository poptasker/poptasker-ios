//
//  ApplicantProfileViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 12/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class ApplicantProfileViewController: BaseVC
{
    @IBOutlet var tblObj : ApplicantProfileTableView!
    var dict = JSONDictionary()
    var isFromApplicant : Bool! = false
    var isAwarded : Int! = 0
    var imgView: UIImageView {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "applicantProfile")
        imgV.alpha = 0.4
        return imgV
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        self.title = "Profile"
//        AppDel.window?.addSubview(imgView)
        getUserReview(Profiledict: dict)
        tblObj.blockTableViewAwardJob = {(index,dict) -> Void in
            
            print(dict)

            let alert = UIAlertController.init(title: kAppName, message: "You selected \(dict["firstname"]!) \(dict["lastname"]!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Confirm", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                self.awardJob(dict: dict)

            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func getUserReview(Profiledict : JSONDictionary)
    {
        let view = UIView.init(frame: self.view.frame)
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        var userIdParam = ""
        if isFromApplicant
        {
            userIdParam = "userid"
        }
        else
        {
            userIdParam = "user_id"
        }
        let param = ["user_id":"\(Profiledict[userIdParam]!)"] as JSONDictionary
        ApiManager.shared.postDataWithHeader(url: kGetReviews, params: param) { (dictReview) in
            AppDel.hideHUD()
            view.removeFromSuperview()
            
            print(dictReview)
            let data = dictReview["data"] as! JSONDictionary
            self.tblObj.isAwarded = self.isAwarded
            self.tblObj.isFromApplicant = self.isFromApplicant
            self.tblObj.createUserArray(dict: Profiledict , arr: data["listing"] as! [JSONDictionary])
        }
    }
    
    func awardJob(dict:JSONDICTIONARY)
    {
        AppDel.showHUD()
        ApiManager.shared.postDataWithHeader(url: kAwardjob, params: ["task_id":"\(dict["task_id"]!)","employee_id":"\(dict["userid"]!)"]) { (dict) in
            AppDel.hideHUD()
            self.navigationController?.popViewController(animated: true)
            UtilityClass.showAlert(str: "\(dict["message"]!)")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }
}
