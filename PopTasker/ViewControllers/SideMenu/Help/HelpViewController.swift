//
//  HelpViewController.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit
import GrowingTextView
class HelpViewController: BaseVC {

    @IBOutlet var txtDescription : GrowingTextView!
    @IBOutlet var txtEmail : TextField!
    @IBOutlet var viewHeight : NSLayoutConstraint!
    var imgView: UIImageView {
        let imgV = UIImageView.init(frame: AppDel.window!.bounds)
        imgV.image = #imageLiteral(resourceName: "help")
        imgV.alpha = 0.4
        return imgV
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Help"
        self.setNavigationMenu(menuButtons: .Back, type: .Back)
        viewHeight.constant = self.view.frame.height - 64
//        AppDel.window?.addSubview(imgView)
    }

    @IBAction func btnSendPress(sender:UIButton)
    {
        if txtDescription.text.count > 0
        {
            if txtEmail.text!.count > 0
            {
                ApiManager.shared.postDataWithHeader(url: kHelp, params: ["email":txtEmail.text!,"query":txtDescription.text!]) { (dict) in
                    print(dict)
                    self.navigationController?.popViewController(animated: true)
                    UtilityClass.showAlert(str: "\(dict["message"]!)")
                }
            }
            else
            {
                UtilityClass.showAlert(str: "Please enter your email id.")
            }
        }
        else
        {
            UtilityClass.showAlert(str: "Please enter your query.")
        }
       
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
