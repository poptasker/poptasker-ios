//
//  TextFieldBlue.swift
//  PopTasker
//
//  Created by Admin on 17/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

//
//  TextFieldGrat.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

class TextFieldBlue: TextField
{
    override func draw(_ rect: CGRect)
    {
        
        self.clipsToBounds = true
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,attributes: [NSAttributedString.Key.foregroundColor: UIColor.setBlueTitlecolor()])
        
        self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))
        
        self.textColor = UIColor.setBlueTitlecolor()
        //  self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
    }
}
