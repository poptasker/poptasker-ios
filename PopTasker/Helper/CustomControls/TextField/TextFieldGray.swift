
//
//  TextFieldGrat.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

class TextFieldGray: TextField
{
    @IBInspectable
    public var fontSize: CGFloat = 10.0 {
        didSet {
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: fontSize))
        }
    }

    override func draw(_ rect: CGRect)
    {

        self.clipsToBounds = true
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,attributes: [NSAttributedString.Key.foregroundColor: UIColor.customGrayColor()])

        if(fontSize != 0){
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: fontSize))
            
        }else{
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))

        }

        
        self.textColor = UIColor.customGrayColor()
      //  self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
    }
}
class TextFieldGray119: TextField
{
    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    @IBInspectable
    public var fontSize: CGFloat = 10.0 {
        didSet {
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: fontSize))
        }
    }

    override func draw(_ rect: CGRect)
    {
        self.addBottomLign()
        if(fontSize != 0){
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: fontSize))

        }else{
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))

        }
        
        self.textColor = UIColor.setgray_119_119_119()
        //  self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
