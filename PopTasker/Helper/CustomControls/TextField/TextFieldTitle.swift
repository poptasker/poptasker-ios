//
//  TextFieldTitle.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class TextFieldTitle: TextField
{
    var leftLbl: LabelNeueBold!
    let padding = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)

    override func draw(_ rect: CGRect)
    {
        self.addBottomLign()
        self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
        self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
    }
    
    func setLeftTitle(title: String) {
        if leftLbl == nil {
            leftLbl = LabelNeueBold.init(frame: CGRect.init(x: 0, y: 2, width: 100, height: self.frame.size.height))
            self.leftViewMode = .always
            self.leftView = leftLbl
        }
        leftLbl.text = title   
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
}
class TextFieldProfileTitle: TextField
{
    var leftLbl: LabelNeueBold!
    let padding = UIEdgeInsets(top: 0, left: 200, bottom: 0, right: 0)

    override func draw(_ rect: CGRect)
    {
//        self.addBottomLign()
        super.draw(rect)

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
        self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))
        
        let border = CALayer()
        let width = CGFloat(0.7)
        border.borderColor = UIColor.setGrayLignColor().cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func setLeftTitle(title: String) {
        if leftLbl == nil {
            leftLbl = LabelNeueBold.init(frame: CGRect.init(x: 0, y: 2, width: 170, height: self.frame.size.height))
            self.leftViewMode = .always
            self.leftView = leftLbl
        }
        leftLbl.text = title
    }
    
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, padding)
//    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, padding)
//    }
}
class TextFieldComment: TextField
{
    var leftLbl: LabelNeueBold!
    let padding = UIEdgeInsets(top: 0, left: 100, bottom: 0, right: 0)
    
    override func draw(_ rect: CGRect) {
        self.addBottomLign()
    
        self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
        self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
    }
    
    
    func setLeftTitle(title: String) {
        if leftLbl == nil {
            leftLbl = LabelNeueBold.init(frame: CGRect.init(x: 0, y: 2, width: 100, height: self.frame.size.height))
            self.leftViewMode = .always
            self.leftView = leftLbl
        }
        leftLbl.text = title
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
}
class TextFieldNavigation: TextField
{
    var leftLbl: LabelNeueBold!
    
    override func draw(_ rect: CGRect) {
        self.addBottomLign()
        self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
        self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))
    }
}
class BottomLineTextField: TextField
{
    
    override func draw(_ rect: CGRect) {
        self.addBottomLign()
        self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
    }
    @IBInspectable
    public var fontSize: CGFloat = 10.0 {
        didSet {
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: fontSize))
        }
    }
    
    @IBInspectable
    public var color: UIColor = UIColor.customGrayColor() {
        didSet {
            self.textColor = color
        }
    }
}
