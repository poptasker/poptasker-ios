//
//  TextView.swift
//  PopTasker
//
//  Created by Solulab_Vicky on 23/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
class TextView: UITextView{
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        self.textContainerInset = UIEdgeInsets(top: 12,left: 8,bottom: 5,right: 8)
        self.clipsToBounds = true
        self.tintColor = UIColor.setButtonColor()
//        self.textColor = GREY_COLOR_THEME_TEXT
    }
}
