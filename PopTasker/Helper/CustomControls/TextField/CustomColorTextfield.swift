//
//  CustomColorTextfield.swift
//  PopTasker
//
//  Created by Admin on 17/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

class CustomColorTextfield: TextField
{
    
    @IBInspectable var boolBlueColor: Bool = false

    override func draw(_ rect: CGRect)
    {
        
        self.clipsToBounds = true
        
        if(boolBlueColor){
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,attributes: [NSAttributedString.Key.foregroundColor: UIColor.setBlueTitlecolor()])
            
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))
            
            self.textColor = UIColor.setBlueTitlecolor()

        }else{
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,attributes: [NSAttributedString.Key.foregroundColor: UIColor.setgray_119_119_119()])
            
            self.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 20))
            
            self.textColor = UIColor.setgray_119_119_119()

        }
        //  self.setValue(UIColor.customGrayColor(), forKeyPath: "_placeholderLabel.textColor")
    }
}
