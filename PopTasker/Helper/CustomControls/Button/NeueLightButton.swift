//
//  NeueLightButton.swift
//  PopTasker
//
//  Created by Rohan on 20/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class NeueBoldButton: UIButton {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.setFont()
        super.draw(rect)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    
    func setFont() {
        self.setCornerRadius()
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueBold, size: getProportionalFont(size: 17))
        self.titleEdgeInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }

}

class NeueLightButton: UIButton {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        self.setFont()
        super.draw(rect)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    
    func setFont() {
        self.setCornerRadius()
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 18.7))
        self.titleEdgeInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func setBackgroundColor(type: ColorType = ColorType.signInPurple) {
        self.backgroundColor = type.color
    }
}

class NeueLightButtonGray: UIButton {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        self.setFont()
        super.draw(rect)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    
    func setFont() {
        self.setCornerRadius()
        self.setTitleColor(UIColor.customGrayColor(), for: .normal)
        self.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        self.titleEdgeInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func setBackgroundColor(type: ColorType = ColorType.signInPurple) {
        self.backgroundColor = type.color
    }
}
class NeueLightButtonGray119: UIButton {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        self.setFont()
        super.draw(rect)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    
    func setFont() {
        self.setCornerRadius()
        self.setTitleColor(UIColor.setgray_119_119_119(), for: .normal)
        self.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 16))
        self.titleEdgeInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func setBackgroundColor(type: ColorType = ColorType.signInPurple) {
        self.backgroundColor = type.color
    }
}
class NeueComonButtonwithGradiantImg: UIButton {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        self.setFont()
        super.draw(rect)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
     self.setBackgroundImage(GradientImage.gradientTheamBackgroundFrame(view: self), for: .normal)
    self.setBackgroundImage(GradientImage.getRedColor(view: self), for: .selected)
    }
    
    func setFont() {
        self.setCornerRadius()
        
        self.setTitleColor(UIColor.customWhiteColor(), for: .normal)
        self.titleLabel!.font = Font.setHelvaticaNeueLTPro(font: .NeueLight, size: getProportionalFont(size: 18.7))
        self.titleEdgeInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func setBackgroundColor(type: ColorType = ColorType.signInPurple) {
        //self.backgroundColor = type.color
    }
}
