//
//  UtilityClass.swift
//  comon
//
//  Created by Ankit on 27/01/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
import UIKit


class UtilityClass {
    
    public static func printAllFont() {
        for family in UIFont.familyNames {
            
            let sName: String = family as String
            print("family: \(sName)")
            
            for name in UIFont.fontNames(forFamilyName: sName) {
                print("name: \(name as String)")
            }
        }
    }
    
    public static func setStatusbarContent(style: UIStatusBarStyle) {
        // Set Status bar style
        UIApplication.shared.statusBarStyle = style
    }
    
    public static func setNavigationTitleColor(alpha:CGFloat = 1.0) -> UIColor {
        return UIColor.setColor(red: 95, green: 86, blue: 142, alpha: alpha)
    }
    
    public static func setCornerRadius(view: UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = view.frame.size.width / 2
    }
    public static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    public static func isValidPassword(testStr:String) -> Bool {
        let passwordRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
    }
    public static func showAlert(str : String)
    {
        let alert = UIAlertController.init(title: kAppName, message: str, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
        AppDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    public static func getFormattedDate(string: String ,formatter1 : String, formatter2:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatter1
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = formatter2
        
        let date: Date? = dateFormatterGet.date(from: string)
        print("Date",dateFormatterPrint.string(from: date!)) // Feb 01,2018
        return dateFormatterPrint.string(from: date!);
    }
    public static func getFormattedTime(string: String ,formatter1 : String, formatter2:String) -> String{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = formatter1
        
        let fullDate = dateFormatter.date(from: string)
        
        dateFormatter.dateFormat = formatter2
        dateFormatter.locale = NSLocale.current
        let time2 = dateFormatter.string(from: fullDate!)
        return time2
    }
    public static func showToast(view:UIView,message : String)
    {
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "OpenSans", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    public static func getFormattedDateTime(secondss : Double,isChatDetailScreen : Bool) -> String
    {
        //secondss is in miliseconds so divided by 1000
        let seconds = secondss / 1000
        
        let isToday = Calendar.current.isDateInToday(Date(timeIntervalSince1970: seconds))
        
        let isYesterday = Calendar.current.isDateInYesterday(Date(timeIntervalSince1970: seconds))
        
        let tDate = Date(timeIntervalSince1970: seconds)
        
        var str = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        if isToday
        {
            if isChatDetailScreen{
                str = "Today \(dateFormatter.string(from: tDate))"
            }else{
             str = dateFormatter.string(from: tDate)
            }
            
        }else if isYesterday
        {
            if isChatDetailScreen{
                str = "Yesterday \(dateFormatter.string(from: tDate))"
            }
            else{
                str = "Yesterday"
            }
        }else
        {
            var dateFormat = ""
            if isChatDetailScreen{
                dateFormat = CHAT_DATE_FORMAT
            }else{
                dateFormat = CHAT_LIST_DATE_FORMAT
            }
            dateFormatter.dateFormat = dateFormat
            str = "\(dateFormatter.string(from: tDate))"
        }
        return str
    }
    public static func getDateFromTimeStamp(timeStamp : String) -> String {
        
        if timeStamp.count > 0 {
            let date = Date(timeIntervalSince1970: Double(timeStamp)!/1000)
            let dtFormatter = DateFormatter()
            dtFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
            return dtFormatter.string(from: date)
        }
        return ""
    }
    /*
    public static func setPushAnimation(vc: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        AppDel.getNavigationController().view.layer.add(transition, forKey: kCATransition)
        AppDel.getNavigationController().pushViewController(vc, animated: false)
    }
    
    public static func setPopAnimation() {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        AppDel.getNavigationController().view.layer.add(transition, forKey: kCATransition)
        AppDel.getNavigationController().popViewController(animated: false)
    }
    
    public static func disableIQKeyBoardForVC() {
        IQKeyboardManager.shared().disabledToolbarClasses.addObjects(from: [CreateEventViewController.self,EventDetailsViewController.self,GroupActivityDetailsViewController.self,ThreadDetailViewController.self,NewGroupViewController.self,EditRuleAndDescriptionViewController.self,ShareImageWithCaptionViewController.self,OnBoardingViewController.self])
    }
    
   
 */
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0
        {
            return "\(years(from: date)) year(s) ago"
        }
        if months(from: date)  != 0
        {
            return "\(months(from: date)) months(s) ago"
        }
        if weeks(from: date)   != 0
        {
            return "\(weeks(from: date)) week(s) ago"
        }
        if days(from: date)    != 0
        {
            return "\(days(from: date)) day(s) ago"
        }
        if hours(from: date)   != 0
        {
            return "\(hours(from: date)) hour(s) ago"
        }
        if minutes(from: date) != 0
        {
            return "\(minutes(from: date)) minute(s) ago"
        }
        if seconds(from: date) != 0
        {
            return "few moment(s) ago"
        }
        
        return ""
    }
}
