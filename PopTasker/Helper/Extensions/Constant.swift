//
//  Constant.swift
//  Comon
//
//  Created by Ankit on 27/01/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
import UIKit

public let kAppName      = "PopTasker"
public let kGoogleClientID      = "197333913323-jr8joi9c783i5srs3ir7kgbjpcgckcn3.apps.googleusercontent.com"
public let kGoogleAPIKey      = "AIzaSyCWtXtZVYqclDLBJQHiOxNU3sj-BbW1ZCw"
let AppDel               = UIApplication.shared.delegate as! AppDelegate
let kPresentedViewHeight = UIScreen.main.bounds.size.height
let kPresentedViewWidth  = UIScreen.main.bounds.size.width
let kDeviceFrame         = AppDel.window!.bounds
let CHAT_DATE_FORMAT = "EEEE, MMM d - hh:mm a"//"dd/MM/yy hh:mm a"
let CHAT_LIST_DATE_FORMAT = "dd/MM/yy"
let GREY_COLOR_THEME_TEXT : UIColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
let VALIDATION_BLANK_TEXT_CHAT = "Please Enter Text"
//-------------------------------------------------//
//------------#    Device Check   #----------------//
//-------------------------------------------------//

let IS_iPHONE_4         = UIScreen.main.bounds.size.height == 480 ? true : false as Bool
let IS_iPHONE_5         = UIScreen.main.bounds.size.height == 568 ? true : false as Bool
let IS_iPHONE_6         = UIScreen.main.bounds.size.height == 667 ? true : false as Bool
let IS_iPHONE_6_plus    = UIScreen.main.bounds.size.height == 736 ? true : false as Bool
let IS_iPad             = UIScreen.main.bounds.size.height == 1024 ? true : false as Bool
let IS_iPHONE_X         = UIScreen.main.nativeBounds.height == 2436 ? true : false as Bool

// Get device UDID
let device_id           = UIDevice.current.identifierForVendor!.uuidString

//-------------------------------------------------//
//------------#   General Const.  #----------------//
//-------------------------------------------------//

let kYES                                = "YES"
let kNO                                 = "NO"
var k_Language                          = "en"

//-------------------------------------------------//
//-------------#   Storyboards   #-----------------//
//-------------------------------------------------//

let MAIN_STORYBOARD         = UIStoryboard.init(name: "Main", bundle: nil)
let FORGOTPASS_STORYBOARD         = UIStoryboard.init(name: "ForgotPassword", bundle: nil)

let SIDE_MENU_STORYBOARD    = UIStoryboard.init(name: "SideMenu", bundle: nil)
let STORYBOARD_TAB          = UIStoryboard.init(name: "TabsVC", bundle: nil)
let MESSAGE_STORYBOARD      = UIStoryboard.init(name: "Message", bundle: nil)
let MESSAGEDETAIL_STORYBOARD = UIStoryboard.init(name: "MessageDetail", bundle: nil)
let KEYWORD_STORYBOARD = UIStoryboard.init(name: "Keywords", bundle: nil)
let CONFIRMATION_STORYBOARD = UIStoryboard.init(name: "ConfirmationVC", bundle: nil)

let ENGINEER_STORYBOARD = UIStoryboard.init(name: "Engineer", bundle: nil)
let MENU_STORYBOARD = UIStoryboard.init(name: "Menu", bundle: nil)
let TRANSACTION_STORYBOARD = UIStoryboard.init(name: "Transactions", bundle: nil)
let APPLICANT_STORYBOARD = UIStoryboard.init(name: "Applicant", bundle: nil)
let FAVORITE_STORYBOARD = UIStoryboard.init(name: "Favorite", bundle: nil)
let SETTINGS_STORYBOARD = UIStoryboard.init(name: "Settings", bundle: nil)
let PROFILE_STORYBOARD = UIStoryboard.init(name: "MyProfile", bundle: nil)
let MAPSEARCH_STORYBOARD = UIStoryboard.init(name: "MapSearch", bundle: nil)
let SEARCHDETAIL_STORYBOARD = UIStoryboard.init(name: "SearchDetail", bundle: nil)
let POSTTASK_STORYBOARD = UIStoryboard.init(name: "PostJob", bundle: nil)
let PAYMENT_STORYBOARD = UIStoryboard.init(name: "Payment", bundle: nil)
let FILTER_STORYBOARD = UIStoryboard.init(name: "Filter", bundle: nil)



//-------------------------------------------------//
//-------#   Navigation And bottom Button   #------//
//-------------------------------------------------//

// Navigation top buttons type

enum MenuType {
    case Menu
    case Back
    case Serch
    case BackAndSearch
    case MenuAndSearch
    case BackAndAdd
    case Close
    case MapSearch
    case Reset
    case BackToRoot
}

enum NavigationButtons {
    case Menu
    case Back
    case Search
    case Add
    case Close
    case CloseMap
    case MapSearch
    case Reset
    case BackToRoot
    var image: UIImage {
        switch self {
        case .Menu: return #imageLiteral(resourceName: "Nvc_Menu")
        case .Back: return #imageLiteral(resourceName: "Nvc_Back")
        case .Search: return #imageLiteral(resourceName: "Nvc_Search")
        case .Add: return #imageLiteral(resourceName: "Nvc_Add")
        case .Close: return #imageLiteral(resourceName: "Nvc_Close")
        case .CloseMap: return #imageLiteral(resourceName: "Nvc_Close")
        case .MapSearch:
            return UIImage()
        case .Reset:
            return UIImage()
        case .BackToRoot:
            return #imageLiteral(resourceName: "Nvc_Back")
        }
    }
    
}

//-------------------------------------------------//
//--------#   ViewController identifires   #-------//
//-------------------------------------------------//

let kHomeViewController             = "HomeViewController"
let kChatViewController             = "ChatViewController"
let kProfileViewController          = "ProfileViewController"


/*---------------------------------------------------------*/
/*--------------- # Type alias # ----------------*/
/*---------------------------------------------------------*/

typealias INT_JSON = Int32
typealias JSONDICTIONARY = [String: Any]

enum FeedType: String {
    case Event = "Event"
    case Group = "Group"
    case Post = "Post"
}

let kPageSize = 10

/*---------------------------------------------------------*/
/*-------------------- # Api Constant # -------------------*/
/*---------------------------------------------------------*/
//MARK: Api Constant

struct LoginKey {
    static let email                    = "email"
    static let password                 = "password"
    static let userId                   = "userId"
    static let userName                 = "userName"
    static let insertDate               = "insertDate"
    static let userImage                = "userImage"
    static let type                     = "type"
    static let isActive                 = "isActive"
    static let city                     = "city"
    static let country                  = "country"
    static let phoneNumber              = "phoneNumber"
    static let token                    = "token"
}

struct NotificatonKey {
    //only notify
    static let  NOTI_NAME_TASK_REMINDER     = "task-reminder"
    static let  NOTI_NAME_AWARDED_TASK      = "awarded-task"
    //redirect
    static let  NOTI_NAME_TASK_APPLICATION  = "task-application"
    static let  NOTI_NAME_TASK_COMPLETED    = "task-completed"
    static let  NOTI_NAME_AWARD_TASK        = "award-task"
    static let  NOTI_NAME_TASK_CANCELED     = "task-canceled"
    
    static let  NOTI_NAME_CHAT_MESSAGE      = "chat_message"
    static let  NOTI_NAME_NEW_APPLICATION   = "new-application";
}
