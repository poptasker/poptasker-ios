//
//  GradientImage.swift
//  PopTasker
//
//  Created by Rohan on 29/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

class GradientImage {
    
    class func gradientTheamBackgroundFrame(view: UIView, isMessageDetail:Bool = false) -> UIImage{
        
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        var sizeLength:CGFloat!
        if isMessageDetail {
            
        }else{
            
        }
        sizeLength = UIScreen.main.bounds.size.width
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height+10.0)
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor(red: 0/255, green: 22/255, blue: 59/255, alpha: 1).cgColor, UIColor(red: 65/255, green: 183/255, blue: 200/255, alpha: 1).cgColor]
        return self.image(fromLayer: gradient)
        
    }
    class func gradientTheamBackgroundFrameforPurple(view: UIView,isMessageDetail:Bool = false, height:CGFloat = 0.0) -> UIImage{
        
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.width
        var defaultNavigationBarFrame:CGRect!
        if isMessageDetail{
            gradient.startPoint = CGPoint(x: 1, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 1)
            defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: height)
        }else{
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 1)
            defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height)
        }
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor(red: 0/255, green: 22/255, blue: 59/255, alpha: 1).cgColor, UIColor(red: 74/255, green: 32/255, blue: 91/255, alpha: 1).cgColor]
        return self.image(fromLayer: gradient)
        
    }

    class func getPlainColor(view: UIView) -> UIImage {
        
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        let sizeLength = UIScreen.main.bounds.size.width
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height)
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor(red: 65/255, green: 183/255, blue: 200/255, alpha: 1).cgColor, UIColor(red: 65/255, green: 183/255, blue: 200/255, alpha: 1).cgColor]
        return self.image(fromLayer: gradient)
        
    }
    class func getRedColor(view: UIView) -> UIImage {
        
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        let sizeLength = UIScreen.main.bounds.size.width
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height)
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor(red: 205/255, green: 64/255, blue: 52/255, alpha: 1).cgColor, UIColor(red: 205/255, green: 64/255, blue: 52/255, alpha: 1).cgColor]

       return self.image(fromLayer: gradient)
        
    }

    
    class func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
    
    class func gradientTheamBackgroundFrameMessage(view: UIView) -> UIImage{
        
        
        
        
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red: 0/255, green: 22/255, blue: 59/255, alpha: 1).cgColor, UIColor(red: 65/255, green: 183/255, blue: 200/255, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        let sizeLength = UIScreen.main.bounds.size.width
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height)
        gradient.frame = defaultNavigationBarFrame
        
        return self.image(fromLayer: gradient)
        
    }
    class func gradientTheamBackgroundFrameforPurpleMessage(view: UIView) -> UIImage{
        
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        let sizeLength = UIScreen.main.bounds.size.width
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: view.frame.size.height)
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor(red: 0/255, green: 22/255, blue: 59/255, alpha: 1).cgColor, UIColor(red: 74/255, green: 32/255, blue: 91/255, alpha: 1).cgColor]
        return self.image(fromLayer: gradient)
        
    }
    
}
