//
//  FilterModel.swift
//  PopTasker
//
//  Created by Admin on 11/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit
class FilterModel {
    var cellType: CellFilterType!
    var sliderMinValue: String! = ""
    var sliderMaxValue: String! = ""
    var sliderValue: String! = ""
    var sliderValueType: String! = ""
    var sliderTintColor: String! = ""
    var titleLabel: String! = ""
    var isSliderValUpldated : Bool! = false
    init(cellType : CellFilterType!,sliderMinValue: String,sliderMaxValue: String, sliderValue: String,sliderValueType : String,sliderTintColor : String, titleLabel:String,isSliderValUpldated : Bool!)
    {
        self.cellType = cellType
        self.sliderMinValue = sliderMinValue
        self.sliderMaxValue = sliderMaxValue
        self.sliderValue = sliderValue
        self.sliderValueType = sliderValueType
        self.sliderTintColor = sliderTintColor
        self.titleLabel = titleLabel
        self.isSliderValUpldated = isSliderValUpldated
    }
//    var filterArray = [FilterDetail]()
//    init(cellType: CellFilterType!,filterArray: [FilterDetail]){
//        self.cellType = cellType
//        self.filterArray = filterArray
//    }
}
class FilterDetail {
    var sliderMinValue: String! = ""
    var sliderMaxValue: String! = ""
    var sliderValue: String! = ""
    var sliderValueType: String! = ""
    var sliderTintColor: String! = ""
    var titleLabel: String! = ""
    
    init(sliderMinValue: String,sliderMaxValue: String, sliderValue: String,sliderValueType : String,sliderTintColor : String, titleLabel:String) {
        self.sliderMinValue = sliderMinValue
        self.sliderMaxValue = sliderMaxValue
        self.sliderValue = sliderValue
        self.sliderValueType = sliderValueType
        self.sliderTintColor = sliderTintColor
        self.titleLabel = titleLabel
    }

}
