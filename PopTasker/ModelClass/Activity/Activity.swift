//
//  Activity.swift
//  PopTasker
//
//  Created by Urja_Macbook on 06/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import UIKit

class Activity
{
    var imgUrl : String! = ""
    var coachName: String! = ""
    var name: String! = ""
    var description: String! = ""
    var developer: String! = ""
    var dollar: String! = ""
    var time: String! = ""
    var dict: JSONDICTIONARY! = [:]
    var activityFilter : String! = ""
    var cellType: ActivityCellType!
    
    init(imgUrl : String,coachName: String,name: String, description: String,developer: String,dollar: String,time: String, cellType: ActivityCellType,dict:JSONDICTIONARY,filter : String) {
        self.imgUrl = imgUrl
        self.coachName = coachName
        self.name = name
        self.description = description
        self.developer = developer
        self.dollar = dollar
        self.time = time
        self.cellType = cellType
        self.activityFilter = filter
        self.dict = dict
    }
}
