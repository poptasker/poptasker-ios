//
//  SearchDetail.swift
//  PopTasker
//
//  Created by Admin on 07/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

class SearchDetail
{
    var img : UIImage!
    var jobName: String! = ""
    var date: String! = ""
    var city: String! = ""
    var description: String! = ""
    var dollar: String! = ""
    var time: String! = ""
    var isApplied: Int! = 0

    var cellType: SearchDetailCellType!
    
    init(img : UIImage,jobName: String,date: String,city: String, description: String,dollar: String,time: String,isApplied:Int, cellType: SearchDetailCellType) {
        self.img = img
        self.jobName = jobName
        self.date = date
        self.city = city
        self.description = description
        self.dollar = dollar
        self.time = time
        self.isApplied = isApplied
        self.cellType = cellType
    }
}
