//
//  RegisterUser.swift
//  PopTasker
//
//  Created by Rohan on 22/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

class RegisterUser {
    
    var placeHolder: String! = ""
    var txtValue: String! = ""
    var imgBgName: String! = ""
    var cellType: CellType!
    
    init(placeHolder: String, txtValue: String, imgBgName:String, cellType: CellType!) {
        self.placeHolder = placeHolder
        self.txtValue = txtValue
        self.imgBgName = imgBgName
        self.cellType = cellType
    }
}
