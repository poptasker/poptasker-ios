//
//  PostAjob.swift
//  PopTasker
//
//  Created by Admin on 10/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit
class PostAjob {
    
    var title: String! = ""
    var value: String! = ""
    var valueColor: String! = ""
    var righButtonimgName: String! = ""
    var cellType: PostAjobCellType!
    var arr : [UIImage]! = [UIImage]()
    init(title: String, value: String,valueColor : String, righButtonimgName:String, cellType: PostAjobCellType!,arr : [UIImage]) {
        self.title = title
        self.value = value
        self.valueColor = valueColor
        self.righButtonimgName = righButtonimgName
        self.cellType = cellType
        self.arr = arr
    }
}
