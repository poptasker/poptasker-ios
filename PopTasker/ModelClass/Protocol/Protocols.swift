//
//  Protocols.swift
//  FirebaseExtension
//
//  Created by User on 3/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

protocol ChatUserDetailsProtocol
{
    func getUserNameForUserID(_ userID: String?, completion: @escaping(_ userObj: ChatUser) -> Void)
}
