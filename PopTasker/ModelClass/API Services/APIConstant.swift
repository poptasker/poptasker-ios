//
//  APIConstant.swift
//  PopTasker
//
//  Created by Urja_Macbook on 24/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//
import UIKit
import Foundation

let BASE_URL = "http://18.219.55.216/"
//let BASE_URL = "http://admin.poptasker.com"

let kLogin = "\(BASE_URL)api/v1/login"
let kLoginwithfb = "\(BASE_URL)api/v1/loginwithfb"
let ksocialcheck = "\(BASE_URL)api/v1/socialcheck"
let kLoginwithgoogle = "\(BASE_URL)api/v1/loginwithgoogle"
let kRegister = "\(BASE_URL)api/v1/register"
let kResetPassword = "\(BASE_URL)api/v1/resetpassword"
let kLogout = "\(BASE_URL)api/v1/user/signout"
let kActivities = "\(BASE_URL)api/v1/activities"
let kActivitieDetails = "\(BASE_URL)api/v1/task/details"
let kGetApplicant = "\(BASE_URL)api/v1/employee/profile"
let kGetReviews = "\(BASE_URL)api/v1/get/reviews"
let kCompleteJob = "\(BASE_URL)api/v1/completejob"
let kApply = "\(BASE_URL)api/v1/apply"
let kCanceljob = "\(BASE_URL)api/v1/canceljob"
let kReview = "\(BASE_URL)api/v1/review"
let kKeywords = "\(BASE_URL)api/v1/keywords"
let kCreatejob = "\(BASE_URL)api/v1/createjob"
let kCheckPayment = "\(BASE_URL)api/v1/check/payment"
let kAddCard = "\(BASE_URL)api/v1/add/card"
let kGetPayout = "\(BASE_URL)api/v1/getpayout"
let kGetCards = "\(BASE_URL)api/v1/getcards"
let kAddpayout = "\(BASE_URL)api/v1/addpayout"
let kDeleteCard = "\(BASE_URL)api/v1/delete/card"
let kUserProfile = "\(BASE_URL)api/v1/user/profile"
let kUserEdit = "\(BASE_URL)api/v1/user/edit"
let kGetapplications = "\(BASE_URL)api/v1/getapplications"
let kgetapplicantsfromjob = "\(BASE_URL)api/v1/getapplicantsfromjob"
let kFavorite = "\(BASE_URL)api/v1/user/favorite"
let kMessagelist = "\(BASE_URL)api/v1/messages"
let kAwardjob = "\(BASE_URL)api/v1/awardjob"
let kGetFavoriteList = "\(BASE_URL)api/v1/user/getfavoritelist"
let kNearbyJobs = "\(BASE_URL)api/v1/nearbyjobs"
let kGetFilters = "\(BASE_URL)api/v1/getfilters"
let kSaveSearch = "\(BASE_URL)api/v1/savesearch"
let kTransactions = "\(BASE_URL)api/v1/transactions"
let kNotreview = "\(BASE_URL)api/v1/task/notreview"
let kHelp = "\(BASE_URL)api/v1/user/help"



