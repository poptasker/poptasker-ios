//
//  ApiManager.swift
//  OyeDeals
//
//  Created by Ankit on 9/15/17.
//  Copyright © 2017 Solulab. All rights reserved.
//
import UIKit
import Foundation
import Alamofire
//import MBProgressHUD
typealias JSONDictionary = [String:AnyObject]

public class ApiManager
{
   static let shared: ApiManager = ApiManager()
   
    //MARK:- Post method
    func postData(url:String!,params:Parameters!,completion:@escaping (_ dict: JSONDictionary) -> ())
    {
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response:DataResponse<Any>) in
            print(response.result)
            if let statusCode = response.response?.statusCode
            {
                if Int("\(statusCode)") == 500
                {
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                }
                else if Int("\(statusCode)") == 405
                {
                    AppDel.hideHUD()
//                    AppDel.sessionExpire()
                }
                else
                {
                    if let json = response.result.value as? JSONDictionary
                    {
                        print(json)
                        if Int("\(statusCode)") != 200
                        {
                            AppDel.hideHUD()
                            UtilityClass.showAlert(str: "\(json["message"]!)")
                        }
                        else
                        {
                            if "\(json["status"]!)"  == "error"
                            {
                                UtilityClass.showAlert(str: "\(json["message"]!)")
                            }
                            else
                            {
                                completion(json)
                            }
                        }
                    }
                    else
                    {
                        AppDel.hideHUD()
                        UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                    }
                }
            }
            else
            {
                UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                AppDel.hideHUD()
            }
            
        }
    }
//    print(params)
//    let request = NSMutableURLRequest(URL: URL.init(string: url)!)
//    //        request.setValue(authorizationToken, forHTTPHeaderField:"Authorization")
//    Alamofire.request(request)
//    .responseJSON { (_, _, JSON, error) in
//
//
//    }
    //MARK:- Post method with header
    func postDataWithHeader(url:String!,params:Parameters!,completion:@escaping (_ dict: JSONDictionary) -> ())
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY

        let header = ["Authorization":"Bearer \(dataDict["api_token"]!)"]
        print(header)
        
        Alamofire.request(url, method: .post, parameters: params,headers:header).responseJSON { (response:DataResponse<Any>) in
            print(response.result)
            if let statusCode = response.response?.statusCode
            {
                if Int("\(statusCode)") == 500
                {
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                }
                else if Int("\(statusCode)") == 405
                {
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                    //                    AppDel.sessionExpire()
                }
                else
                {
                    if let json = response.result.value as? JSONDictionary
                    {
                        print(json)
                        if Int("\(statusCode)") != 200
                        {
                            AppDel.hideHUD()
                            UtilityClass.showAlert(str: "\(json["message"]!)")
                        }
                        else
                        {
                            if "\(json["status"]!)"  == "error"
                            {
                                UtilityClass.showAlert(str: "\(json["message"]!)")
                            }
                            else
                            {
                                completion(json)
                            }
                        }
                    }
                    else
                    {
                        AppDel.hideHUD()
                        UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                    }
                }
            }
            else
            {
                UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                AppDel.hideHUD()
            }
        }
    }
    //MARK:- Multiple image - Post method with image
    func PostWithImage(url: String, imageData: [Data], parameters: [String : Any], onCompletion: @escaping (_ dict: JSONDictionary) -> ())
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        
        let header = ["Authorization":"Bearer \(dataDict["api_token"]!)","enctype": "multipart/form-data"]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData.count > 0
            {
                for i in 0...imageData.count - 1
                {
                    let data = imageData[i] as Data
                    multipartFormData.append(data, withName: "pictures[]", fileName: "image\(i).jpg", mimeType: "image/jpg")
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
            upload.responseJSON { response in
                print("Succesfully uploaded")
                if let statusCode = response.response?.statusCode
                {
                    if Int("\(statusCode)") == 500
                    {
                        AppDel.hideHUD()
                        UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                    }
                    else if Int("\(statusCode)") == 405
                    {
                        AppDel.hideHUD()
                        //                    AppDel.sessionExpire()
                    }
                    else
                    {
                        if let json = response.result.value as? JSONDictionary
                        {
                            print(json)
                            if Int("\(statusCode)") != 200
                            {
                                AppDel.hideHUD()
                                UtilityClass.showAlert(str: "\(json["message"]!)")
                            }
                            else
                            {
                                if "\(json["status"]!)"  == "error"
                                {
                                    UtilityClass.showAlert(str: "\(json["message"]!)")
                                }
                                else
                                {
                                    onCompletion(json)
                                }
                            }
                        }
                        else
                        {
                            AppDel.hideHUD()
                            UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                        }
                    }
                }
                else
                {
                    UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                    AppDel.hideHUD()
                }
            }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                UtilityClass.showAlert(str: error.localizedDescription)
                AppDel.hideHUD()
            }
            
        }
    }
    //MARK:- single image - Post method with image
    func PostWithSingleImage(url: String, imageData: Data, parameters: [String : Any], onCompletion: @escaping (_ dict: JSONDictionary) -> ())
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        
        let header = ["Authorization":"Bearer \(dataDict["api_token"]!)","enctype": "multipart/form-data"]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
           multipartFormData.append(imageData, withName: "profilepic", fileName: "image.png", mimeType: "image/png")
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let statusCode = response.response?.statusCode
                    {
                        if Int("\(statusCode)") == 500
                        {
                            AppDel.hideHUD()
                            UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                        }
                        else if Int("\(statusCode)") == 405
                        {
                            AppDel.hideHUD()
                            //                    AppDel.sessionExpire()
                        }
                        else
                        {
                            if let json = response.result.value as? JSONDictionary
                            {
                                print(json)
                                if Int("\(statusCode)") != 200
                                {
                                    AppDel.hideHUD()
                                    UtilityClass.showAlert(str: "\(json["message"]!)")
                                }
                                else
                                {
                                    if "\(json["status"]!)"  == "error"
                                    {
                                        UtilityClass.showAlert(str: "\(json["message"]!)")
                                    }
                                    else
                                    {
                                        onCompletion(json)
                                    }
                                }
                            }
                            else
                            {
                                AppDel.hideHUD()
                                UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                            }
                        }
                    }
                    else
                    {
                        UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                        AppDel.hideHUD()
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                UtilityClass.showAlert(str: error.localizedDescription)
                AppDel.hideHUD()
            }
            
        }
    }
    //MARK:- Post method with error handle
    func postDataWithErrorHandle(url:String!,params:Parameters!,completion:@escaping (_ dict: JSONDictionary?,_ err:JSONDICTIONARY?) -> ())
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        
        let header = ["Authorization":"Bearer \(dataDict["api_token"]!)"]
        print(header)
        
        Alamofire.request(url, method: .post, parameters: params,headers:header).responseJSON { (response:DataResponse<Any>) in
            print(response.result)
            if let statusCode = response.response?.statusCode
            {
                if Int("\(statusCode)") == 500
                {
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                }
                else if Int("\(statusCode)") == 405
                {
                    AppDel.hideHUD()
                    //                    AppDel.sessionExpire()
                }
                else
                {
                    if let json = response.result.value as? JSONDictionary
                    {
                        print(json)
                        if Int("\(statusCode)") != 200
                        {
                            completion(nil, json)
                        }
                        else
                        {
                            if "\(json["status"]!)"  == "error"
                            {
                                 completion(nil, json)
                            }
                            else
                            {
                                completion(json, nil)
                            }
                        }
                    }
                    else
                    {
                        AppDel.hideHUD()
                        UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                    }
                }
            }
            else
            {
                
                UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                AppDel.hideHUD()
            }
        }
    }
    //MARK:- get method
    func getData(url:String!,completion:@escaping (_ dict: JSONDictionary) -> ())
    {
        let userdata = getModelDataFromUserDefaults(key: "userData") as! JSONDICTIONARY
        let dataDict = userdata["data"] as! JSONDICTIONARY
        
        let header = ["Authorization":"Bearer \(dataDict["api_token"]!)"]
        print(header)
        Alamofire.request(url, method: .get, parameters: nil, headers: header).responseJSON { (response:DataResponse<Any>) in
    
            print(response.result)
            if let statusCode = response.response?.statusCode
            {
                if Int("\(statusCode)") == 500
                {
                    AppDel.hideHUD()
                    UtilityClass.showAlert(str: "Server Not Responding, Please try again later...!!")
                }
                else if Int("\(statusCode)") == 405
                {
                    AppDel.hideHUD()
                    //                    AppDel.sessionExpire()
                }
                else
                {
                    if let json = response.result.value as? JSONDictionary
                    {
                        print(json)
                        if Int("\(statusCode)") != 200
                        {
                            AppDel.hideHUD()
                            UtilityClass.showAlert(str: "\(json["message"]!)")
                        }
                        else
                        {
                            if "\(json["status"]!)"  == "error"
                            {
                                UtilityClass.showAlert(str: "\(json["message"]!)")
                            }
                            else
                            {
                                completion(json)
                            }
                        }
                    }
                    else
                    {
                        AppDel.hideHUD()
                        UtilityClass.showAlert(str: "Something went wrong. Please try again later.")
                    }
                }
            }
            else
            {
                UtilityClass.showAlert(str: response.result.error!.localizedDescription)
                AppDel.hideHUD()
            }
        }
    }
}

import UIKit
class GlobalFunction: NSObject
{
    
    static var instance: GlobalFunction!
    class func sharedInstance() -> GlobalFunction
    {
        self.instance = (self.instance ?? GlobalFunction())
        return self.instance
    }
    class func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                UIAlertController.showAlert(withTitle: kAppName, alertMessage:"Something went wrong", buttonArray: ["OK"], completion: { (index:Int) in
                    
                })
            }
        }
        return nil
    }
}

extension UIAlertController
{
    class func showAlert(withTitle alertTitle: String, alertMessage: String, buttonArray: [String], completion: @escaping(_ buttonIndex : Int) -> Void)
    {
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        for i in 0..<buttonArray.count {
            let alertButton = UIAlertAction(title: (buttonArray[i]), style: .default, handler: { UIAlertAction in
                
                completion(i)
                
                alertController.dismiss(animated: true, completion: {
                    
                })
            })
            
            alertController.addAction(alertButton)
        }
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController
        {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // present the view controller
            topController.present(alertController, animated: true, completion: nil)
        }
    }
    
}
