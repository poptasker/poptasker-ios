//
//  MyProfile.swift
//  PopTasker
//
//  Created by Urja_Macbook on 10/10/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
class MyProfile {
    
    var placeHolder: String! = ""
    var txtValue: String! = ""
    var cellType: CellProfileType!
    var img : UIImage! = UIImage()
    init(placeHolder: String, txtValue: String, cellType: CellProfileType!,img:UIImage = UIImage()) {
        self.placeHolder = placeHolder
        self.txtValue = txtValue
        self.cellType = cellType
        self.img = img
    }
}
