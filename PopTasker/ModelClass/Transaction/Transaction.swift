//
//  Transaction.swift
//  PopTasker
//
//  Created by Urja_Macbook on 18/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit
class TransactionModel {
    var cellType: CellTransactionType!
    var titleLable: String! = ""
    var date: String! = ""
    var cost: String! = ""
    var address: String! = ""
    var timeAgo: String! = ""
    var description: String! = ""

    init(cellType: CellTransactionType!,titleLable: String,date: String, cost: String,address : String,timeAgo : String,description:String)
    {
        self.titleLable = titleLable
        self.date = date
        self.cost = cost
        self.address = address
        self.timeAgo = timeAgo
        self.cellType = cellType
        self.description = description

    }
//    var filterArray = [Transaction]()
//    init(cellType: CellTransactionType!,filterArray: [Transaction]){
//        self.filterArray = filterArray
//    }
}
//class Transaction {
//    
//}
