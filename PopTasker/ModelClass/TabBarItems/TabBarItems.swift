//
//  TabBarItems.swift
//  PopTasker
//
//  Created by Rohan on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation
import UIKit

struct TabBarItems {

    var title: String!
    var viewController: String!
    var unSelectedImg: UIImage!
    var selectedImg: UIImage!
    
    init(title: String, viewController: String, unSelectedImg: UIImage, selectedImg: UIImage  ) {
        self.title = title
        self.viewController = viewController
        self.unSelectedImg = unSelectedImg
        self.selectedImg = selectedImg
    }
    
}
