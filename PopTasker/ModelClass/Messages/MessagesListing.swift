//
//  Listing.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 17, 2018

import Foundation


class MessagesListing{

    var address : String!
    var conversationId : String!
    var fromId : Int!
    var picture : String!
    var taskId : Int!
    var title : String!
    var toId : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        conversationId = dictionary["conversation_id"] as? String
        fromId = dictionary["from_id"] as? Int
        picture = dictionary["picture"] as? String
        taskId = dictionary["task_id"] as? Int
        title = dictionary["title"] as? String
        toId = dictionary["to_id"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if conversationId != nil{
            dictionary["conversation_id"] = conversationId
        }
        if fromId != nil{
            dictionary["from_id"] = fromId
        }
        if picture != nil{
            dictionary["picture"] = picture
        }
        if taskId != nil{
            dictionary["task_id"] = taskId
        }
        if title != nil{
            dictionary["title"] = title
        }
        if toId != nil{
            dictionary["to_id"] = toId
        }
        return dictionary
    }
}
