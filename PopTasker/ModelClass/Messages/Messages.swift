//
//  Messages.swift
//  PopTasker
//
//  Created by Admin on 30/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

class Messages {
    
    var imageProfile: URL!
    var name: String! = ""
    var date: String! = ""
    var dateFilter: String! = ""
    var messageTypr: String! = ""

    init(imageProfile: URL, name: String, date:String, dateFilter: String!) {
        self.imageProfile = imageProfile
        self.name = name
        self.date = date
        self.dateFilter = dateFilter
    }
}
