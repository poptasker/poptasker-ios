//
//  Conversations.swift
//  FirebaseExtension
//
//  Created by User on 3/1/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

enum MessageType : String
{
    case TextMessage = "text"
    case MediaMessagePhoto = "mediaPhoto"
    case MediaMessageVideo = "mediaVideo"
}

class Conversation
{
    var conversationID = ""
    var typingDictionary : NSMutableDictionary?
    var chatHistoryDictionary : NSMutableDictionary?
    var isGroup = false
    let lastMessage = LastMessage()
    var userDictionary : NSMutableDictionary?
    var isValueChanged = false
    var unReadMessageCountDict: NSMutableDictionary!
    //for offline support
    var username = ""
    var userImage = ""

    
    //UserData
    var conversationId:String!
    var FromId:Int!
    var ProfilePic:String!
    var title:String!
    var toId:Int!
    var address:String!
    var taskId:Int!
    
    func update(data : Dictionary<String,Any>)
    {
        if let dict = data["chat_history"]
        {
            chatHistoryDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let dict = data["users"]
        {
            userDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let dict = data["last_message"]
        {
            lastMessage.update(data: dict as! NSDictionary)
        }
        
        if let dict = data["unread"]
        {
            unReadMessageCountDict = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        //unread
    }
    
    func updateUserData(data: JSONDICTIONARY){
        
        if let dict = data["from_id"]
        {
            FromId = dict as? Int
        }
        if let dict = data["picture"]
        {
            ProfilePic = dict as? String
        }
        if let dict = data["title"]
        {
            title = dict as? String
        }
        if let dict = data["to_id"]
        {
            toId = dict as? Int
        }
        if let dict = data["conversation_id"]
        {
            conversationId = dict as? String
        }
        if let dict = data["address"]
        {
            address = dict as? String
        }
        if let dict = data["task_id"]
        {
            taskId = dict as? Int
        }
        
    }
}

class LastMessage : Message{
    var messageId = ""
    
    func update(data : NSDictionary)
    {
        if data.value(forKey: "text") != nil{
            messageText = data.value(forKey: "text") as! String
        }
        if let data = data["timestamp"]{
            timestamp = data as! String
        }
        
        if let data = data["msgID"]{
            messageId = data as! String
        }
    }
}

class Message{
    var id = ""
    var messageText = ""
    var timestamp = ""
    var status = ""
    var messageType = "text"
    var senderName = ""
    var user = ""
    var mediaURL = ""
    var videoThumbnail = ""
    var messageStatus = ""
    var senderProfile = ""
    
    func update(data : Dictionary<String,Any>)
    {
        if let data = data["senderName"]
        {
            senderName = data as! String
        }
        
        if let data = data["time"]
        {
            timestamp = data as! String
        }
        
        if let data = data["message"]
        {
            messageText = data as! String
        }
        
        if let data = data["user"]
        {
            user = data as! String
        }
        
        if let data = data["message_status"]
        {
            messageStatus = data as! String
        }

        if let data = data["type"]
        {
            messageType = data as! String
        }
        if let data = data["senderProfile"]
        {
            senderProfile = data as! String
        }
    }
}

class ChatUser
{
    var user_id = ""
    var name = ""
    var imageURL = ""

    func update(data : Dictionary<String,Any>)
    {
        if let data = data["id"]
        {
            user_id = (data as! NSNumber).stringValue
        }

        if let data = data["picture"]
        {
            imageURL = data as! String
        }

        if let data = data["name"]
        {
            name = data as! String
        }
    }
}

//class ChatGroup : ChatUser{
//    var conversationID = ""
//
//    override func update(data : Dictionary<String,Any>)
//    {
//        super.update(data: data)
//
//        if let data = data["groupID"]
//        {
//            conversationID = data as! String
//        }
//    }
//}

class Sections
{
    var sectionTitle = ""
    var sectionMessages = [Message]()
}
