//
//  MessageDetail.swift
//  PopTasker
//
//  Created by Admin on 31/08/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation


class MessageDetails {
    var fromimageProfile:URL!
    var fromName : String = ""
    var fromDateFilterString : String = ""
    var fromAddress : String = ""
    var messages = [MessageList]()
    init(fromimageProfile: URL, fromName: String, fromAddress:String, messages: [MessageList]) {
        self.fromimageProfile = fromimageProfile
        self.fromName = fromName
        self.fromAddress = fromAddress
        self.messages = messages
    }

}
class MessageList {
    var datelable : String = ""
    var messageLists  = [MessageDetailsList]()
    init(datelabel:String,messageLists:[MessageDetailsList]) {
        self.datelable = datelabel
        self.messageLists = messageLists
    }
}
class MessageDetailsList {
    var fromimageProfile: URL!
    var fromtext: String! = ""
    var totext: String! = ""
    var messageType:CellMessageType
    init(fromimageProfile: URL, fromtext: String, totext:String,typeMessage:CellMessageType) {
        self.fromimageProfile = fromimageProfile
        self.fromtext = fromtext
        self.totext = totext
        self.messageType = typeMessage
    }

}
