//
//  RootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 17, 2018

import Foundation


class MessagesRootClass{

    var currentPage : Int!
    var hasPage : Bool!
    var listing : [MessagesListing]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    func ParseDict(fromDictionary dictionary: [String:Any]){
        currentPage = dictionary["current_page"] as? Int
        hasPage = dictionary["has_page"] as? Bool
        listing = [MessagesListing]()
        if let listingArray = dictionary["listing"] as? [[String:Any]]{
            for dict in listingArray{
                let value = MessagesListing(fromDictionary: dict)
                listing.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if currentPage != nil{
            dictionary["current_page"] = currentPage
        }
        if hasPage != nil{
            dictionary["has_page"] = hasPage
        }
        if listing != nil{
            var dictionaryElements = [[String:Any]]()
            for listingElement in listing {
                dictionaryElements.append(listingElement.toDictionary())
            }
            dictionary["listing"] = dictionaryElements
        }
        return dictionary
    }
}
