//
//  Payment.swift
//  PopTasker
//
//  Created by Admin on 05/09/18.
//  Copyright © 2018 Ankit Kargathra. All rights reserved.
//

import Foundation

class Payment {
    
    var FullName: String! = ""
    var placeHolder: String! = ""
    var txtValue: String! = ""
    var cellType: CellPaymentType!
    
    init(placeHolder: String, txtValue: String, cellType: CellPaymentType!) {
        self.placeHolder = placeHolder
        self.txtValue = txtValue
        self.cellType = cellType
    }
}
